<?php

/*
|--------------------------------------------------------------------------
| Get Routes
|--------------------------------------------------------------------------
|
|
|
*/

Route::get('/account', 'PageController@accountPage');

Route::group(array('before' => 'auth.required-fields'), function(){
    Route::get('/', 'PageController@homePage');
    Route::get('/mail', 'AdvertentieController@sendingEmail');

    Route::get('/hoe-het-werkt', 'PageController@hoehetwerktPage');
    Route::get('/over-ons', 'PageController@overonsPage');
    Route::get('/algemene-voorwaarden', 'PageController@algemenevoorwaardenPage');
    Route::get('/veelgestelde-vragen', 'PageController@veelgesteldevragenPage');
    Route::get('/contact', 'PageController@contactPage');
    Route::get('/categorieen', 'PageController@categorieenPage');
    Route::get('/acties', 'PageController@actiesPage');
    Route::get('/advertentie/plaatsen', 'PageController@advertentiePlaatsenPage');
    Route::get('/advertentie/{id}', 'PageController@advertentiePage');
    Route::get('/advertentie/{id}/check-betaling', 'TransactieController@checkTransaction');
    Route::get('/advertentie/{id}/betaald', 'PageController@advertentieBetaald');
    Route::get('/advertentie/{id}/bewerken', 'PageController@advertentieBewerken');
    Route::get('/download/{id}', 'AdvertentieController@download');
    Route::get('/categorie/{cat}', 'PageController@categoriePage');

    Route::get('/zoeken', 'PageController@zoekenPage');
    Route::get('/inloggen', 'PageController@inlogPage');
    Route::get('/facebook/login', 'UserController@registerFacebook');
    Route::get('/actie/deelname/{id}', 'ActieController@doeMee');
    Route::get('/getpdf',  'PageController@getPdf');
    Route::get('/privacy-policy', 'PageController@privacypolicyPage');
});

Route::get('/uitloggen', function() {
	if (Auth::check())
	{
		Auth::logout();
		return Redirect::to('/');
	}
	else
	{
		return Redirect::to('/');
	}
});
    
/* Forgot password (GET) */
Route::get('/forgot-password', 
    array('as' => 'account-forgot-password',
        'uses' => 'UserController@getForgotPassword'
));

// Change password (GET) 
Route::get('/change-password', 
    array('as' => 'account-change-password',
        'uses' => 'UserController@getChangePassword'
));

Route::get('/canvas', 'PageController@canvasImg');
Route::get('/upload', function() {
  return View::make('upload');
});
Route::post('/upload', 'PageController@uploadImg');


Route::get('/advertentie/upload', function() {
  return View::make('upload');
});
Route::post('/advertentie/upload', 'AdvertentieController@uploadImg');


Route::get('/advertentie/imgick', function() {
  return View::make('imgick');
});
Route::post('/advertentie/imgick', 'AdvertentieController@imageMagick');

Route::get('/advertentie/multi_pdfUpload', function() {
  return View::make('multi_pdfUpload');
});
Route::post('/advertentie/multi_pdfUpload', 'AdvertentieController@multi_pdfUpload');

Route::get('/advertentie/viewCoupon', function() {
  return View::make('viewCoupon');
});
Route::post('/advertentie/viewCoupon', 'AdvertentieController@viewImgCoupon');

Route::get('/advertentie/check', function() {
  return View::make('check');
});
Route::post('/advertentie/check', 'AdvertentieController@check');


/*Route::get('/mailPdf', function() {
  return View::make('mailPdf');
});
Route::post('/mailPdf', 'PageController@mailPdf');*/


Route::post('/uploadPdf', 'PageController@uploadPdf');


/*
|--------------------------------------------------------------------------
| Post Routes
|--------------------------------------------------------------------------
|
|
|
*/
Route::post('/searchPage', 'PageController@searchPage');
Route::post('/registreren', 'UserController@registerEmail');
Route::post('/inloggen', 'UserController@loginEmail');
Route::post('/pdfcheck', 'AdvertentieController@pdfcheck');
Route::post('/advertentie/plaatsen', 'AdvertentieController@plaatsen');
Route::post('/account/wijzig/foto', 'UserController@accountWijzigFoto');
Route::post('/account/wijzig/gegevens', 'UserController@accountWijzigGegevens');
Route::post('/zoeken', 'AdvertentieController@zoeken');
Route::post('/advertentie/{id}/betalen', 'TransactieController@betalen');
Route::post('/advertentie/{id}/betalen/transactie', 'TransactieController@executeTransaction');
Route::post('/advertentie/{id}/bewerken', 'AdvertentieController@bewerken');
Route::post('/advertentie/{id}/verwijderen', 'AdvertentieController@advertentieVerwijderen');
Route::post('/advertentie/{id}/comment', 'CommentController@postComment');
Route::post('/subscribe', 'UserController@subscribe');

/* Forgot password (POST) */
Route::post('/forgot-password', 
	array('as' => 'account-forgot-password-post',
	'uses' => 'UserController@postForgotPassword'
));

/* Change password (POST) */
Route::post('/change-password', 
	array('as' => 'account-change-password-post',
		'uses' => 'UserController@postChangePassword'
));
Route::get('/upload', 'PageController@uploadImg');


/* MISSINGGGG */
App::missing(function($exception)
{
    return View::make('pages.nietGevonden')->with('whoops', 'Die pagina bestaat niet.');
});

// App::fatal(function($exception)
// {
// 	Log::error($exception);
//     return View::make('pages.nietGevonden')->with('whoops', 'We konden geen verbinding maken met de server, probeer het opnieuw.');
// });


function checkWaardenummer($w)
{
	return Waardenummer::where('waardeNummer', '=', $w)->count();
}