<?php

class CommentController extends BaseController {

	public function postComment($id)
	{
		if (!Auth::check())
		{
			Session::flash('whoops', 'Log je in om een vraag te stellen.');
			return Redirect::to('/inloggen');
		}
		else
		{
			$from    = Auth::user()->userId;
			$content = trim(Input::get('comment'));
			
			if ($content == '')
			{
				Session::flash('custom-errors', ['Je kunt geen lege vraag stellen.']);
				return Redirect::back()->withInput();
			}
			else
			{
				$comment                = new Comment;
				$comment->advertentieId = $id;
				$comment->fromId        = Auth::user()->userId;
				$comment->comment       = $content;
				$comment->toId 	 		= Input::get('toId');
				$comment->createdAt     = new DateTime;

				if ($comment->save())
				{
					// Mail naar toId

					Session::flash('custom-success', ['Je vraag is succesvol gesteld; je ontvangt een e-mail zodra er is gereageerd.']);
					return Redirect::back();
				}
				else
				{
					Session::flash('custom-errors', ['Er ging wat fout bij het verbinden met de server, probeer het opnieuw.']);
					return Redirect::back()->withInput();
				}
			}
		}
	}

}