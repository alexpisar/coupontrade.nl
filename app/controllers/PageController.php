<?php

class PageController extends BaseController {
	
	public function homePage()
	{
		//$countries = DB::table('Search')->orderBy("weight")->lists('country');
		$search = Search::orderBy("weight")->get();
		if($search){
			$countries = $search->lists('country');
			$price = $search->lists('price');
		}

		$countries = isset($countries) ? $countries : '';
		$price = isset($price) ? $price : '';
		$ads = Advertentie::where('verkocht', '=', '0')->where('verwijderd', '=', '0', 'AND')->orderBy('advertentieId', 'DESC')->take(12)->get();
		
		if (count($ads) > 0)
		{
			foreach ($ads as $a) {
				$a->increment('weergaven');
			}
		}

		$gem = Gemist::orderByRaw("RAND()")->take(3)->get();
		return View::make('pages.home')->with('ads', $ads)->withGemist($gem)->with('countries' , $countries)->with('price' , $price);
	}

	public function hoehetwerktPage()
	{
		return View::make('pages.hoe-het-werkt');
	}

	public function overonsPage()
	{
		return View::make('pages.over-ons');
	}

	public function algemenevoorwaardenPage()
	{
		return View::make('pages.algemene-voorwaarden');
	}

	public function veelgesteldevragenPage()
	{
		return View::make('pages.veelgestelde-vragen');
	}

	public function actiesPage()
	{
		$a = Actie::where('actief', '=', '1')->orderBy('actieId', 'DESC')->get();
		return View::make('pages.acties')->withActies($a);
	}

	public function contactPage()
	{
		return View::make('pages.contact');
	}

	public function categorieenPage()
	{
		$ads = Advertentie::where('verkocht', '=', '-')->where('verwijderd', '=', '0', 'AND')->orderBy('advertentieId', 'DESC')->take(72)->get();
		$cats = ['vakanties','evenementen','restaurants','hotels','avondje-uit','dagje-weg','sport','overig'];
		return View::make('pages.categorieen')->withAds($ads)->withCats($cats);
	}

	public function inlogPage()
	{
		if (Auth::check())
		{
			return Redirect::to('/');
		}
		return View::make('pages.inloggen');
	}

	public function accountPage()
	{
		if (Auth::check())
		{
			$ads = Auth::user()->advertenties()->where('verwijderd', '=', '0')->get();
			$aan = Auth::user()->aankopen()->get();
			$aanExists = [];
			foreach ($aan as $pdf) {
			  try{
				if($pdf->pdfExists())
					$aanExists[] = $pdf;
			  }catch(Exception $e){}
			}

			return View::make('pages.account')->withAds($ads)->withAankopen($aanExists);
			// return $aan;
		}
		else
		{
			return Redirect::to('/inloggen')->with('whoops', 'Je bent niet ingelogd.');
		}
	}

	public function advertentiePlaatsenPage()
	{
		if (Auth::check())
		{
			return View::make('pages.advertentiePlaatsen')
			->with('notification', 
		      'Loop je tegen problemen aan bij het plaatsen van je advertentie? Stuur dan een e-mail naar <a href="mailto:info@coupontrade.nl" class="blue">info@coupontrade.nl</a>');
		}
		else
		{
			return Redirect::to('/inloggen')->with('whoops', 'Log in om een advertentie te plaatsen.');
		}
	}

	public function advertentiePage($id)
	{
		$c = strlen($id);
		$adId = substr($id, 0, strpos($id, '-'));
		$adUrl 	= substr($id, strlen($adId) + 1, strpos($id, '-') + $c);
		
		$q = Advertentie::where('advertentieId', '=', $adId)->where('titelUrl', '=', $adUrl, 'AND')->where('verwijderd', '=', '0', 'AND')->get();

		if (count($q) != 0)
		{
			$couponImgs = [];

			if ($q[0]->type == 5)
			{
				$m = Advertentie::where('verkoperId', '=', $q[0]->verkoperId)->where('verkocht', '=', '0', 'AND')->get();
			}
			else
			{
				$m = Advertentie::where('categorie', '=', $q[0]->categorie)->where('verkocht', '=', '0', 'AND')->take(5)->get();
			}
			if($q[0]->pdfImg){
				foreach (explode(',', $q[0]->pdfImg) as $img){
					if(trim($img) && file_exists(public_path().$img))
						$couponImgs[] = $img;

				}
				$couponImgs = array_unique($couponImgs);
			}

			return View::make('pages.advertentiePage')->withAd($q[0])->withMeer($m)->withCouponImgs($couponImgs);
		}
		else
		{
			return View::make('pages.nietGevonden')->with('whoops', 'De advertentie die je zoekt bestaat niet (meer)!');
		}
	}

	public function actiePage($id)
	{
		$c     = strlen($id);
		$adId  = substr($id, 0, strpos($id, '-'));
		$adUrl = substr($id, strlen($adId) + 1, strpos($id, '-') + $c);
		$actie = Actie::where('actieId', '=', $adId)->where('titelUrl', '=', $adUrl, 'AND')->first();

		if ($actie)
		{
			return View::make('pages.actie')->withActie($actie);
		}
		else
		{
			return View::make('pages.nietGevonden')->with('whoops', 'De actie die je zoekt bestaat niet.');
		}
	}

	public function categoriePage($cat)
	{
		$ads = Advertentie::where('categorie', '=', $cat)->where('verkocht', '=', '0', 'AND')->where('verwijderd', '=', '0', 'AND')->get();
		$cats = ['vakanties','evenementen','restaurants','hotels','avondje-uit','dagje-weg','sport','overig'];
		return View::make('pages.categoriePage')->withAds($ads)->withCat($cat)->withCats($cats);
	}

	public function zoekenPage()
	{
		
		return View::make('pages.zoekPage');
	}
	
	public function search()
	{
		return View::make('pages.filter');
	}
	
	
	public function searchPage()
	{	
		
		$countries = '';
		$facilities = '';
		$arrangements = '';
		$searchItmes = Search::orderBy("weight")->get();
		if($searchItmes){
			$countries = $searchItmes->lists('country');
			$facilities = $searchItmes->lists('facilities');
			$arrangements = $searchItmes->lists('arrangement');
		}
		
		if(isset($_POST)){
			
			$data = Input::all();
			/*$val = $data['accomodation'];*/
			$reg = $data['country'];
			$price = $data['price'];
			$checkBox = isset($data['arrangement']) ? $data['arrangement'] : '';
			if(!empty($checkBox)){
			$values = implode(',',$checkBox);
			//echo $values;
			}
			 $values = isset($values)?$values:"";
			$split = explode('-',$price);
			//print_r($split);
			$sql = "SELECT advertentieId,verkoperId,titel,titelUrl,categorie,rating,facilities,duration,country,arrangement,accomodatie,prijs,twv,
			geldigTot FROM Advertenties WHERE titel like '%%'"; 
			if(!empty($reg))
			$sql.= " AND country = '$reg'";
			if(!empty($values))
			$sql.= " AND arrangement = '$values'";
			if($split[0]== 0 && !empty($split[1]))
			$sql.=" AND (prijs BETWEEN $split[0] AND $split[1])";
			if(!empty($split[0]) && !empty($split[1]))
			$sql.=" AND (prijs BETWEEN $split[0] AND $split[1])";
			if(!empty($split[0]) && empty($split[1]))
			$sql.=" AND prijs >= '$split[0]'";
			$sql.=" AND verkocht = '0' AND verwijderd = '0' ";
			$ads[] = DB::select( DB::raw($sql));
			
			//print_r($ads);die();
			foreach($ads[0] as $ad){
				$arrayList['advertentieId'] = $ad->advertentieId;
				$arrayList['verkoperId'] = $ad->verkoperId;
				$arrayList['titel'] = $ad->titel;
				$arrayList['titelUrl'] = $ad->titelUrl;
				$arrayList['categorie'] = $ad->categorie;
				$arrayList['rating'] = $ad->rating;
				$arrayList['facilities'] = explode(',',$ad->facilities);
				$arrayList['duration'] = $ad->duration;
				$arrayList['country'] = $ad->country;
				$arrayList['arrangement'] = explode(',',$ad->arrangement);
				$arrayList['accomodation'] = $ad->accomodatie;
				$arrayList['prijs'] = $ad->prijs;
				$arrayList['twv'] = $ad->twv;
				$arrayList['geldigTot'] = $ad->geldigTot;
				$arrNew[] = $arrayList;
				}
				$arrNew = isset($arrNew)? $arrNew:'';
			 $json = json_encode($arrNew,JSON_NUMERIC_CHECK);
			return View::make('pages.filter')->with('data', $json)
				->with('countries' , $countries)->with('facilities' , $facilities)
				->with('arrangements' , $arrangements)->with('callfrom' , 'searchpagefilter');	
			
			}
			
		
	}
	
	/*public function searchBoxPage()
	{
		$country = DB::table('Search')->lists('country');
		$accomodation = DB::table('Search')->lists('accomodation');
		$stay = DB::table('Search')->lists('length_stay');
		$facilities = DB::table('Search')->lists('facilities');
		$arrangement = DB::table('Search')->lists('arrangement');
		
		return View::make('pages.searchPage')
		->with('country' , $country)
		->with('accomodation' , $accomodation)
		->with('stay' , $stay)
		->with('facilities' , $facilities)
		->with('arrangement' , $arrangement);

	}*/
	
	

	public function advertentieBetaald($id)
	{
		if (Auth::check())
		{
			$c = strlen($id);
			$adId = substr($id, 0, strpos($id, '-'));
			$adUrl 	= substr($id, strlen($adId) + 1, strpos($id, '-') + $c);
			
			$q = Advertentie::where('advertentieId', '=', $adId)->where('titelUrl', '=', $adUrl, 'AND')->get();

			if(count($q) != 0)
			{
				$verkoop = Verkoop::where('advertentieId', '=', $adId)->orderBy('verkoopId', 'DESC')->get();
				if (count($verkoop) != 0 && $verkoop[0]->koperId == Auth::user()->userId)
				{
					return View::make('pages.advertentieBetaald')->withAd($q[0]);
				}
				else
				{
					return View::make('pages.nietGevonden')->with('whoops', 'De pagina die je zoekt bestaat niet.');		
				}
			}
			else
			{
				return View::make('pages.nietGevonden')->with('whoops', 'De pagina die je zoekt bestaat niet.');
			}
		}
		else
		{
			return View::make('pages.nietGevonden')->with('whoops', 'De pagina die je zoekt bestaat niet.');
		}
	}

	public function advertentieBewerken($id)
	{
		if (Auth::check())
		{
			$c = strlen($id);
			$adId = substr($id, 0, strpos($id, '-'));
			$adUrl 	= substr($id, strlen($adId) + 1, strpos($id, '-') + $c);
			$q = Advertentie::where('advertentieId', '=', $adId)->where('titelUrl', '=', $adUrl, 'AND')->first();

			if($q)
			{
				if ($q->verkoperId === Auth::user()->userId)
				{
					return View::make('pages.advertentieBewerken')->withAd($q);
				}
				else
				{
					return View::make('pages.nietGevonden')->with('whoops', 'Deze advertentie is niet door jou geplaatst. Je kunt hem daarom niet bewerken.');
				}
			}
			else
			{
				return View::make('pages.nietGevonden')->with('whoops', 'De advertentie die je wilt bewerken bestaat niet (meer).');
			}
		}
		else
		{
			return Redirect::to('/inloggen')->with('whoops', 'Je bent niet ingelogd.');
		}
	}
	
	public function canvasImg()
	{
		return View::make('pages.canvas');
	}
	
	public function popUp()
	{
		return View::make('pages.pop');
	}
	
	public function uploadImg()
	{
		 if(Request::ajax()) {
      		$data = Input::all();
      		// get the dataURL
			$dataURL = $data["image"];  
			
			// the dataURL has a prefix (mimetype+datatype) 
			// that we don't want, so strip that prefix off
			$parts = explode(',', $dataURL);  
			$data = $parts[1];  
			
			// Decode base64 data, resulting in an image
			$data = base64_decode($data);  
			
		
			// create a temporary unique file name
			$file = 'img/pdfImg/image/' . uniqid() . '.png';
			
			
			// write the file to the upload directory
			$success = file_put_contents($file, $data);
			
			// return the temp file name (success)
			// or return an error message just to frustrate the user (kidding!)
			echo $success ? $file : 'Unable to save this image.';
    	 }
				
	}
	
	/*public function mailPdf()
	{
		$userid = Auth::user()->userId;
		$user =  DB::select( DB::raw("SELECT * FROM Users WHERE userId = :userId"), array( 'userId' => $userid ));
		foreach($user as $res){
						$data = ['naam' => $res->naam ];
						Mail::send('emails.pdfMail', $data, function($message) use($res)
						{
						  $message->to($res->email);
							  $message->subject('Pdf for the coupon is attached');
						});						
				}
		return "success";
	}*/
	public function getPdf()
	{
		//adv id required
		$id = Auth::user()->userId;
		$pdf = DB::select( DB::raw("SELECT * FROM Pdfs WHERE koperId = :koperId"), array( 'koperId' => $id));
			if ($pdf)
			{
				if ($pdf[0]->koperId === Auth::user()->userId)
				{
					
					$headers = array('Content-Type: application/pdf',);
					return Response::download($pdf[0]->path, '', $headers);
				}
				
			}
	}
	
		
	public function uploadPdf(){
		
		function randString($length = 4)
		{
		    $str = "";
		    $characters = array_merge(range('A','W'), range('a','w'), range('0','9'));
		    $max = count($characters) - 1;
		    for ($i = 0; $i < $length; $i++)
		    {
			    $rand = mt_rand(0, $max);
			    $str .= $characters[$rand];
			}
		    return $str;
		}
		
		if (Input::hasFile('fileInput')) {
	
		// File was successfully uploaded and is now in temp storage.
		// Move it somewhere more permanent to access it later.
			$user 		= Auth::user();
			$file 		= Input::file('fileInput');
			$rand 		= randString();
			$filename 	= $file->getClientOriginalName();
			$path 		= base_path() . '/private/uploads/' . $user->email . '/' . date('d-m-Y h-i') . ' ' . $rand;

	
			if (Input::file('fileInput')->move($path . '/origineel', $filename)) {
				
				$input = $path . '/origineel/' . $filename;
				$str =  strstr($filename,".pdf",true);

				$uploadPath = 'img/pdfImg/'.$str. '.png';
				
				$imagick = new Imagick();
				$imagick->readImage($input);
				$imagick->writeImage($uploadPath);
				
				$var = "/" . $uploadPath;
				// File successfully saved to permanent storage
		
		
				} else {
					
					echo "--------";
				// Failed to save file, perhaps dir isn't writable. Give the user an error.
			}
	
		}
		//return View::make('pages.canvas');
					return Redirect::back()->with('var' , $var);

		
	}

	public function privacypolicyPage()
	{
		return View::make('pages.privacy-policy');
	}

}