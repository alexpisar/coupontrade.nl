<?php

class TransactieController extends BaseController {

	public function betalen($id)
	{
		if(Auth::check())
		{
			$c = strlen($id);
			$adId = substr($id, 0, strpos($id, '-'));
			$adUrl 	= substr($id, strlen($adId) + 1, strpos($id, '-') + $c);
			$q = Advertentie::where('advertentieId', '=', $adId)->where('titelUrl', '=', $adUrl, 'AND')->get();

			if (count($q) != 0)
			{
				if ($q[0]->verkocht == '0')
				{
					$qantani 	= Qantani::CreateInstance('958', '7stIatt', 'fnckMlQvTNSEIhbqTYmBIVtRQ');
					$banken 	= $qantani->Ideal_getBanks();
					if (Input::has('aantal'))
					{
						if (Input::get('aantal') == 0)
						{
							return Redirect::back()->withInput()->with('whoops', 'Selecteer een aantal.');
						}
						else
						{
							Session::flash('aantal', Input::get('aantal'));
							return View::make('pages.betalen')->withAd($q[0])->withBanken($banken)->withAantal(Input::get('aantal'));
						}
					}
					else
					{
						return View::make('pages.betalen')->withAd($q[0])->withBanken($banken);
					}
				}
				else
				{
					return View::make('pages.nietGevonden')->with('whoops', 'Dit aanbod is al verkocht.');
				}
			}
			else
			{
				return View::make('pages.nietGevonden')->with('whoops', 'De advertentie die je zoekt bestaat niet (meer).');
			}
		}
		else
		{
			return Redirect::to('/inloggen')->with('whoops', 'Om een betaling te doen moet je ingelogd zijn.');
		}
	}

	public function executeTransaction($id)
	{
		if(Auth::check())
		{
			Session::reflash();
			if (Input::get('bank') == '0')
			{
				return Redirect::to('/advertentie/' . $id)->with('whoops', 'U heeft geen bank geselecteerd.');
			}
			else
			{
				$c = strlen($id);
				$adId = substr($id, 0, strpos($id, '-'));
				$adUrl 	= substr($id, strlen($adId) + 1, strpos($id, '-') + $c);
				
				$q = Advertentie::where('advertentieId', '=', $adId)->where('titelUrl', '=', $adUrl, 'AND')->get();

				if (count($q) != 0)
				{
					if ($q[0]->verkocht == '0' && $q[0]->verwijderd == '0')
					{
						$qantani 	= Qantani::CreateInstance('958', '7stIatt', 'fnckMlQvTNSEIhbqTYmBIVtRQ');

						if (Session::has('aantal'))
						{
							$url = $qantani->Ideal_execute(array(
								'Amount'		=> Session::get('aantal') * $q[0]->prijs,// * 1.05,
								'Currency'		=> 'EUR',
								'Description'	=> 'Advertentie ' . $q[0]->advertentieId . ' door ' . Auth::user()->userId,
								'Bank'			=> Input::get('bank'),
								'Return'		=> URL::action("TransactieController@checkTransaction", ["id" => $id])
							));
						}
						else
						{
							$url = $qantani->Ideal_execute(array(
								'Amount'		=> $q[0]->prijs,// * 1.05,
								'Currency'		=> 'EUR',
								'Description'	=> 'Advertentie ' . $q[0]->advertentieId . ' door ' . Auth::user()->userId,
								'Bank'			=> Input::get('bank'),
								'Return'		=> URL::action("TransactieController@checkTransaction", ["id" => $id])
							));
						}
						
						if ($url)
						{
							if ($q[0]->type == 5)
							{
								$advert = $q[0];
								do {
									$waardenummer = mt_rand(1000000000, 9999999999);
								} while(checkWaardenummer($waardenummer) == 1);

								$data = [
									'waardenummer' => $waardenummer,
									'datum'        => date('d-m-Y'),
									'advertentie'  => $advert
								];

							    $html = View::make('pdf.waardebon')->withData($data)->__tostring();
							    // return PDFLIB::load($html, 'A4', 'portrait')->show();

								define('BUDGETS_DIR', base_path() . '/private/partners/' . $advert->partner->referentieId);

								if (!is_dir(BUDGETS_DIR)){
								    mkdir(BUDGETS_DIR, 0755, true);
								}

								$pdfPath = BUDGETS_DIR . '/' . $waardenummer . '.pdf';
								File::put($pdfPath, PDFLIB::load($html, 'A4', 'portrait')->output());

								$pdf = new Pdf;
								$pdf->advertentieId = $advert->advertentieId;
								$pdf->koperId = Auth::user()->userId;
								$pdf->verkocht = 1;
								$pdf->path = Pdf::trimPath( BUDGETS_DIR . '/' . $waardenummer . '.pdf'); //convert to relative path
								$pdf->updatedAt = new DateTime();
								$pdf->save();

								$wn = new Waardenummer;
								$wn->partnerRef = $advert->partner->referentieId;
								$wn->waardeNummer = $waardenummer;
								$wn->pdfId = $pdf->pdfId;
								$wn->koperId = Auth::user()->userId;
								$wn->save();

								Session::flash('wn', $waardenummer);
							}

							return Redirect::to($url);
						}
						else
						{
							return Redirect::to('/advertentie/' . $id)->with('whoops', 'Betaling mislukt. Probeer het nog een keer.');
						}
					}
					else
					{
						return View::make('pages.nietGevonden')->with('whoops', 'Dit aanbod is al verkocht of is verwijderd.');
					}
				}
				else
				{
					return View::make('pages.nietGevonden')->with('whoops', 'De advertentie die je zoekt bestaat niet (meer).');
				}
			}
		}
		else
		{
			return Redirect::to('/inloggen')->with('whoops', 'Om een betaling uit te voeren moet je ingelogd zijn.');
		}
	}

	public function checkTransaction($id)
	{
		Session::reflash();
		if (Request::get('status') == '')
		{
			return Redirect::to('/advertentie/' . $id)->with('whoops', 'Betaling mislukt. Probeer het nog een keer.');
		}
		else if (Request::get('status') != '0')
		{
			$adId = substr($id, 0, strpos($id, '-'));
			$ad = Advertentie::find($adId);
			$ad->updatedAt = new DateTime();

			if ($ad->type != 5)
			{
				if(Session::has('aantal'))
				{
					if ($ad->count == Session::get('aantal'))
					{
						$ad->verkocht = 1;
					}
					$ad->count = $ad->count - Session::get('aantal');
				}
				else
				{
					$ad->verkocht = 1;
				}
				$ad->save();

				if (Session::has('aantal'))
				{
					$pdf = Pdf::where('advertentieId', '=', $adId)->where('verkocht', '=', '0', 'AND')->take(Session::get('aantal'))->get();
					$deprijs = $ad->prijs * Session::get('aantal');
				}
				else
				{
					$pdf = Pdf::where('advertentieId', '=', $adId)->where('verkocht', '=', '0', 'AND')->take(1)->get();
					$deprijs = $ad->prijs;
				}

				foreach($pdf as $p) {
					$p->verkocht = 1;
					$p->koperId = Auth::user()->userId;
					$p->updatedAt = new DateTime();
					$p->save();
				}
			}
			else
			{
				// genereer pdf
				$pdf = Pdf::where('advertentieId', '=', $adId)->where('verkocht', '=', '0', 'AND')->take(1)->get();
				$deprijs = $ad->prijs;
			}

			$verkoop = new Verkoop;
			$verkoop->advertentieId = $adId;
			$verkoop->verkoperId = $ad->verkoperId;
			$verkoop->koperId = Auth::user()->userId;
			if (Session::has('aantal'))
			{
				$verkoop->aantal = Session::get('aantal');
			}
			$verkoop->createdAt = new DateTime();
			$verkoop->updatedAt = new DateTime();
			$verkoop->save();

			$koper = Auth::user();
			$dataKoper = ['naam' => $koper->naam, 'url' => $id, 'titel' => $ad->titel];

			if ($ad->type == 5)
			{
				$verkoper = Partner::where('referentieId', '=', $ad->verkoperId)->first();
				$dataVerkoper = ['naam' => $verkoper->naam, 'titel' => $ad->titel, 'prijs' => $deprijs, 'wn' => Session::get('wn')];
				Mail::later(10, 'emails.advertentieVerkochtPartner', $dataVerkoper, function($message) use ($verkoper)
				{
				  $message->to($verkoper->email, $verkoper->naam)
				          ->subject('U heeft een verkoop verricht!');
				});
			}
			else
			{
				$verkoper = User::find($ad->verkoperId);
				$dataVerkoper = ['naam' => $verkoper->naam, 'titel' => $ad->titel, 'prijs' => $deprijs];
				Mail::later(10, 'emails.advertentieVerkocht', $dataVerkoper, function($message) use ($verkoper)
				{
				  $message->to($verkoper->email, $verkoper->naam)
				          ->subject('Je advertentie is zojuist verkocht!');
				});
			}

			Mail::later(12, 'emails.advertentieGekocht', $dataKoper, function($message) use ($koper)
			{
			  $message->to($koper->email, $koper->naam)
			          ->subject('Je aankoop downloaden');
			});

			return Redirect::to('/advertentie/' . $id . '/betaald');
		}
		else
		{
			return Redirect::to('/advertentie/' . $id)->with('whoops', 'Betaling mislukt. Probeer het nog een keer.');
		}

	}

}