<?php

class UserController extends BaseController {

	public function registerEmail()
	{
		$file = Input::file('foto');
		
		if ($file)
		{
			$input = [
				'foto' 	=> Input::file('foto'),
				'voornaam' => str_replace(' ', '', Input::get('voornaam')),
				'achternaam' => str_replace(' ', '', Input::get('achternaam')),
				'email' => Input::get('email'),
				'wachtwoord' => Input::get('wachtwoord'),
				'wachtwoord_confirmation' => Input::get('wachtwoord_confirmation'),
				'woonplaats' => Input::get('woonplaats')
			];

			$rules = [
				'foto' => 'mimes:jpeg',
				'voornaam' => 'required|alpha',
				'achternaam' => 'required|alpha',
				'email' => 'required|email|unique:Users',
				'wachtwoord' => 'min:6|confirmed',
				'woonplaats' => 'required'
			];

			$messs = [
				'mimes' => 'Selecteer een .jpeg, of .jpg bestand als profielfoto.',
				'required' => 'Alle velden zijn verplicht om in te vullen.',
				'voornaam.alpha' => 'Het naam veld mag alleen letters bevatten.',
				'achternaam.alpha' => 'Het naam veld mag alleen letters bevatten.',
				'email.email' => 'Voer een geldig e-mail adres in.',
				'email.unique' => 'Dit e-mail adres is al in gebruik.',
				'wachtwoord.min' => 'Kies een wachtwoord met een minimale lengte van 6 karakters.',
				'wachtwoord.confirmed' => 'De wachtwoorden die je hebt ingevoerd komen niet overeen.'
			];

			$validator = Validator::make($input, $rules, $messs);

			if ($validator->fails())
			{
				return Redirect::back()->withInput()->with('fouten', $validator->messages()->all());
			}
			else
			{
				$rnd              = str_random(9);
				$rn               = str_random(11);
				$move             = $file->move(public_path() . '/img/profile-pictures/', $rnd . '-' . $rn . '.jpg');
				$user             = new User;
				$user->email      = Input::get('email');
				$user->password   = Hash::make(Input::get('wachtwoord'));
				$user->pictureUrl = '/img/profile-pictures/' . $rnd . '-' . $rn . '.jpg';
				$user->naam       = ucwords(Input::get('voornaam') . ' ' . Input::get('achternaam'));
				$user->woonplaats = Input::get('woonplaats');
				$user->createdAt  = new DateTime();
				$user->updatedAt  = new DateTime();

				$data = ['naam' => Input::get('voornaam')];

				if ($user->save())
				{
					Mail::later(10, 'emails.welkomEmail', $data, function($message) use($user)
					{
					  $message->to($user->email, $user->naam)
					          ->subject('Welkom bij CouponTrade!');
					});

					Auth::loginUsingId($user->userId);
					return Redirect::to('/inloggen')->with('result', 'Je bent succesvol geregistreerd en ingelogd.');//regok
				}
				else
				{
					return Redirect::back()->withInput()->with('whoops', 'Er ging wat fout bij het verbinden met de server. Probeer het opnieuw.');
				}

			}
		}
		else
		{
			return Redirect::back()->withInput()->with('whoops', 'Selecteer een profiel foto.');
		}
	}

	public function loginEmail()
	{
		$input = [
			'email' => Input::get('email'),
			'password' => Input::get('password')
		];

		$rules = [
			'email' => 'required|email',
			'password' => 'required'
		];

		$messs = [
			'required' => 'Vul beide velden in.',
			'email.email' => 'Je hebt een ongeldig e-mail adres ingevoerd.',
		];

		$validator = Validator::make($input, $rules, $messs);

		if ($validator->fails())
		{
			return Redirect::back()->withInput()->with('fouten', $validator->messages()->all());
		}
		else
		{
			if (Auth::attempt(['email' => Input::get('email'), 'password' => Input::get('password')]))
			{
				return Redirect::to('/account');
			}
			else
			{
				return Redirect::back()->withInput()->with('whoops', 'Je kon niet worden ingelogd. Weet je zeker dat je de juist e-mail/wachtwoord combinatie hebt ingevoerd?');
			}
		}
	}

	public function registerFacebook()
	{
	    /**
	     * Obtain an access token.
	     */
	    try
	    {
	        $token = Facebook::getTokenFromRedirect();

	        if ( ! $token)
	        {
	            return Redirect::to('/inloggen')->with('whoops', 'Kon geen access token verkrijgen.');
	        }
	    }
	    catch (FacebookQueryBuilderException $e)
	    {
	        return Redirect::to('/inloggen')->with('whoops', $e->getPrevious()->getMessage());
	    }

	    if ( ! $token->isLongLived())
	    {
	        /**
	         * Extend the access token.
	         */
	        try
	        {
	            $token = $token->extend();
	        }
	        catch (FacebookQueryBuilderException $e)
	        {
	            return Redirect::to('/inloggen')->with('whoops', $e->getPrevious()->getMessage());
	        }
	    }

	    Facebook::setAccessToken($token);

	    /**
	     * Get basic info on the user from Facebook.
	     */
	    try
	    {
	        $fbUser = Facebook::object('me')->fields('id', 'name', 'email', 'hometown')->get();
	    }
	    catch (FacebookQueryBuilderException $e)
	    {
	        return Redirect::to('/inloggen')->with('whoops', $e->getPrevious()->getMessage());
	    }

	    $checkIfExist = User::where('email', '=', $fbUser['email'])->get();
	    if (count($checkIfExist) == 0)
	    {
	    	if (isset($fbUser['hometown']))
	    	{
	    		$woonplaats = $fbUser['hometown'];
	    	}
	    	else
	    	{
	    		$woonplaats = 'Onbekend';
	    	}

	    	$randomPassword = str_random(8);

	    	$user = new User;
	    	$user->email = $fbUser['email'];
	    	$user->naam = $fbUser['name'];
	    	$user->password = Hash::make($randomPassword);
	    	$user->pictureUrl = 'https://graph.facebook.com/' . $fbUser['id'] . '/picture?type=large';
	    	$user->woonplaats = $woonplaats;
	    	$user->createdAt = new DateTime();
	    	$user->updatedAt = new DateTime();
	    	$user->facebookId = $fbUser['id'];
	    	$user->access_token = $token;

	    	$data = ['naam' => $fbUser['name'], 'email' => $fbUser['email'], 'wachtwoord' => $randomPassword];

	    	if ($user->save())
	    	{

				Mail::later(10, 'emails.welkomFacebook', $data, function($message) use ($user)
				{
				  $message->to($user->email, $user->naam)
				          ->subject('Welkom bij CouponTrade!');
				});

	    		if(Auth::loginUsingId($user->userId))
	    		{
	    			return Redirect::to('/');
	    		}
	    		else
	    		{
	    			return Redirect::to('/inloggen')->with('whoops', 'Je bent geregistreerd, maar kon niet ingelogd worden. Log opnieuw in.');
	    		}
	    	}
	    	else
	    	{
	    		return Redirect::to('/inloggen')->with('whoops', 'Je kon niet geregistreerd worden, probeer het opnieuw.');
	    	}
	    }
	    else
	    {
	    	if ($checkIfExist[0]->facebookId == NULL)
	    	{
	    		// Al geregistreerd, maar niet met Facebook
	    		$user = User::find($checkIfExist[0]->userId);
	    		$user->facebookId = $fbUser['id'];
	    		$user->access_token = $token;
	    		$user->updatedAt = new DateTime();
	    		if ($user->save())
	    		{
	    			if (Auth::loginUsingId($user->userId))
	    			{
	    				return Redirect::to('/');
	    			}
	    			else
	    			{
	    				return Redirect::to('/inloggen')->with('whoops', 'Je bent geregistreerd, maar kon niet ingelogd worden. Log opnieuw in.');
	    			}
	    		}
	    		else
	    		{
	    			return Redirect::to('/inloggen')->with('whoops', 'Er ging wat fout bij het verbinden met de server. Probeer het opnieuw.');
	    		}
	    	}
	    	else
	    	{
	    		// Al geregistreerd met Facebook, inloggen
	    		if(Auth::loginUsingId($checkIfExist[0]->userId))
	    		{
	    			return Redirect::to('/account');
	    		}
	    		else
	    		{
	    			return Redirect::to('/inloggen')->with('whoops', 'Je bent geregistreerd, maar kon niet ingelogd worden. Log opnieuw in.');
	    		}
	    	}
	    }
	}

	public function accountWijzigFoto()
	{			
		$input = [
			'foto' 	=> Input::file('foto')
		];

		$rules = [
			'foto' => 'mimes:jpeg'
		];

		$messs = [
			'mimes' => 'Selecteer een .jpeg, of .jpg bestand als profielfoto.'
		];

		$validator = Validator::make($input, $rules, $messs);

		if ($validator->fails())
		{
			return Response::json('Failed', 500);
		}
		else
		{
			$file             = Input::file('foto');
			$rnd              = str_random(9);
			$rn               = str_random(11);
			$move             = $file->move(public_path() . '/img/profile-pictures/', $rnd . '-' . $rn . '.jpg');
			$user             = Auth::user();
			$user->pictureUrl = '/img/profile-pictures/' . $rnd . '-' . $rn . '.jpg';
			$user->updatedAt  = new DateTime();
			$user->save();
			return Response::json('Success', 200);
		}

	}

	public function accountWijzigGegevens()
	{
     	$validator = Validator::make(array_map('trim', Input::all()), User::getRules(), User::getMessages());

     	if($validator->fails()){
     		$messages = $validator->messages()->all();
     		$messageArr = array_unique($messages);
           return Redirect::back()->withInput()->with('whoops', $messageArr);
        }

		// 	return Redirect::back()->withInput()->with('whoops', 'Je kunt je weergavenaam niet leeg laten.');

			$n = Input::get('naam');
			$w = Input::get('woonplaats');
			$i = Input::get('iban');
			$b = Input::get('bankhouder');
			$user = Auth::user();
			$user->naam = $n;
			$user->woonplaats = $w;
			$user->iban = $i;
			$user->bankhouder = $b;
			$user->updatedAt = new DateTime();
			if ($user->save())
			{
				return Redirect::back()->with('success', 'Je wijzigingen zijn opgeslagen!');
			}
			else
			{
				return Redirect::back()->withInput()->with('whoops', 'Er ging wat fout bij het verbinden met de server, probeer het opnieuw.');
			}
	}

	public function subscribe()
	{
		$e = Input::get('email');
		$email = new Abonnee;
		$email->email = $e;
		$email->createdAt = new DateTime();
		$email->save();
	}
	
	public function getForgotPassword()
	{
		return View::make('pages.forgotPassword');
		
	}
	public function postForgotPassword()
	{
			$validator = Validator::make(Input::all(),
			array(
				'email' => 'required|email'
			)
		);

		if($validator->fails()) {
			return Redirect::route('account-forgot-password')
				->withErrors($validator)
				->withInput();
		} 
		else {
			$user = DB::select( DB::raw("SELECT * FROM Users WHERE email = :email"), array(
			'email' => Input::get('email')));
			if(count($user) > 0) {
				 $password = str_random(6);
				 $update = DB::table('Users')
           		 ->where('email',$user[0]->email )
           		 ->update(array('password' => Hash::make($password)));
				 if(count($update)){
					 $data = ['naam' => $user[0]->naam, 'password' => $password, 'email' => $user[0]->email ];
							Mail::send('emails.forgotPasswordMail', $data, function($message) use($user)
							{
							  $message->to($user[0]->email);
							  $message->subject('Jouw wachtwoord');
							});
							Session::regenerate();
							Session::put('email', $user[0]->email);
					}
					
					return Redirect::to('/inloggen')
						->with('result', 'Je ontvangt binnen enkele minuten een e-mail met instructies om je wachtwoord te resetten.');
				}
				else
				{
					return Redirect::to('/inloggen');
				}			
		}
		/* fall back */
		return Redirect::route('account-forgot-password');
		}


	public function getChangePassword() {
	
	return View::make('pages.resetPassword');
	}
	
	
	public function postChangePassword() {
		
		$old_password = Input::get('old_password');
		$new_password = Input::get('password');
		
				
	$validator = Validator::make(Input::all(),
		array(
			'password' 		=> 'required|min:6',
			'old_password'	=> 'required|min:6',
			'password_again'=> 'required|same:password'
		)
		
	);

	if($validator->fails()) {
		return Redirect::route('account-change-password')
			->withErrors($validator);
	}
	else {
		if (Session::has('email')) {
			$session = Session::get('email');
			
			$userPassReset = DB::select( DB::raw("SELECT * FROM Users WHERE email = :email"), array(
			'email' => $session));
			if(count($userPassReset)){
				 $updatePassReset = DB::table('Users')
           		 ->where('email',$session )
           		 ->update(array('password' => Hash::make($new_password)));
				}
			}
			return Redirect::to('/inloggen')
				->with('result', 'Het wachtwoord van uw account is gewijzigd.');			
		}
	}
}