<?php

class AdvertentieController extends BaseController {

	private function randString($length = 4)
	{
	    $str = "";
	    $characters = array_merge(range('A','W'), range('a','w'), range('0','9'));
	    $max = count($characters) - 1;
	    for ($i = 0; $i < $length; $i++)
	    {
		    $rand = mt_rand(0, $max);
		    $str .= $characters[$rand];
		}
	    return $str;
	}

	private function pdfRecreate($f)
  {
      rename($f,str_replace('.pdf','_.pdf',$f));  

      $fileArray=array(str_replace('.pdf','_.pdf',$f));
      $outputName=$f;
      $cmd = "gs -q -dNOPAUSE -dBATCH -sDEVICE=pdfwrite -sOutputFile=$outputName ";

      foreach($fileArray as $file)
      {
        $cmd .= $file." ";
      }
      $result = shell_exec($cmd);
      unlink(str_replace('.pdf','_.pdf',$f));
  }

	public function pdfcheck()
	{
		if (Input::file('fileInput') == false)
		{
			return Response::json('Fout', 500);
		}



		function splitPDF($key, $val, $data)
		{
			set_time_limit(360);

		  $desource = $data['source'];
		  $path    = $data['path'];

		  $naamt = substr( $desource, strrpos( $desource, '/' )+1 );
		  $naamr = substr($naamt, 0, strrpos( $naamt, '.') );

		  $pdf = new FPDI();
		  $pdf->addPage();
		  $pdf->setSourceFile($desource);
		  $tmpl = $pdf->importPage($key);
		  $pdf->useTemplate($tmpl, 10, 10, 200);

		  $user = Auth::user();

		  $filename = $naamr . ' pagina ' . $key;
		  $filename = preg_replace("/[^a-zA-Z0-9]+/", "-", trim($filename));

		  $pdf->Output($path . '/splits/' . $filename . '.pdf', 'f');
		  $pdf->cleanUp();
		  $pdf->_enddoc();
		}

		// File upload

		$pdf 		= new FPDI;
		$user 		= Auth::user();

		$rand 		= $this->randString();
		$file 		= Input::file('fileInput');
		$filename 	= $file->getClientOriginalName();
		$path 		= base_path() . '/private/uploads/' . $user->email . '/' . date('d-m-Y-h-i') . '-' . $rand;

		File::makeDirectory($path, 0777, true);
		File::makeDirectory($path . '/origineel/', 0777, true);
					$str =  basename($filename,".pdf");
					$str = preg_replace("/[^a-zA-Z0-9]+/", "-", trim($str));
					$filename = $str. '.pdf';
		$movefile 	= Input::file('fileInput')->move($path . '/origineel', $filename);

		if ($movefile)
		{
			try{
				$source 	= $pdf->setSourceFile($path . '/origineel/' . $filename);
			}catch (Exception $e){
				//if error pdf_parser: Unable to find object (..., 0) at expected location
				$this->pdfRecreate($path . '/origineel/' . $filename);
				$source 	= $pdf->setSourceFile($path . '/origineel/' . $filename);
			}

		    $sourcer 	= $path . '/origineel/' . $filename;

		    $pdf->addPage();

		    if ($source > 1)
		    {

		    	File::makeDirectory($path . '/splits', 0777, true);

		    	$array = range(1, $source);

				$filet = substr( $sourcer, strrpos( $sourcer, '/' )+1 );
				$filer = $path . '/origineel/' . $filet;

				$data = array('path'=>$path,'source'=>$filer);

				array_walk_recursive($array, 'splitPDF', $data);

				$dir = $path . '/splits/';

				$splits = array();

				$files = scandir($dir);
				natsort($files);
				
				foreach ($files as $file) {
				  $splits[] = $path . '/splits/' . $file;
				}
				$splits[] = $sourcer;		
				$splitPdfs = array_slice($files, 2);
				foreach($splitPdfs as $node){
					$pdfPath = $path . '/splits/' . $node;
					$str =  strstr($node,".pdf",true);
					$str = preg_replace("/[^a-zA-Z0-9]+/", "-", trim($str));
					$uploadPath = 'img/pdfImg/'.$str. '.png';
					$imagick = new Imagick();
					$imagick->setResolution(650,650);
					$imagick->readImage($pdfPath);
					//$imagick->flattenImages(); 
					$imagick->resizeImage(580,780,Imagick::FILTER_LANCZOS,1);
					$imagick->writeImage($uploadPath);
					$var[] = "/" . $uploadPath;
				}
				//$splits[] = $var;	
				return Response::json(array_slice($splits, 2));
		    }
		    else
		    {
			    $str =  strstr($filename,".pdf",true);
			    $str = preg_replace("/[^a-zA-Z0-9]+/", "-", trim($str));
				$uploadPath = 'img/pdfImg/'.$str. '.png';
				$imagick = new Imagick();
				$imagick->setResolution(650,650);
				$imagick->readImage($sourcer);
				//$imagick->flattenImages(); 
				$imagick->resizeImage(600,800,Imagick::FILTER_LANCZOS,1);
				$imagick->writeImage($uploadPath);
				$var = "/" . $uploadPath;
			  
		      $lefile = array();
		      $lefile[] = $path . '/origineel/' . $filename.'&'.$var;

		      return Response::json($lefile);

		    }
		}
	}

	public function plaatsen()
	{
		
		function friendlyURL($string)
		{
		    $string = preg_replace("`\[.*\]`U","",$string);
		    $string = preg_replace('`&(amp;)?#?[a-z0-9]+;`i','-',$string);
		    //$string = htmlentities($string, ENT_COMPAT, 'utf-8');
		    $string = preg_replace( "`&([a-z])(acute|uml|circ|grave|ring|cedil|slash|tilde|caron|lig|quot|rsquo);`i","\\1", $string );
		    $string = preg_replace( array("`[^a-z0-9]`i","`[-]+`") , "-", $string);

		    return $string;
		}
		
		//validate is exist pdf files
		$hasPdf = true;

		if(!Input::get('pdfImg') || !Input::has('file'))
		{
			$hasPdf = false;
		}

		if (count(Input::get('file')) > 1)
		{ //it is selected from checkboxesf
		    $filesArray = Input::get('file');
		    foreach ($filesArray as $file)
		    {
		    	if(!file_exists($file) || is_dir($file))
				  $hasPdf = false;
		    }
		}
		else
		{ //it is from hidden input 'file'
			$fileArray = Input::get('file');
			if(is_array($fileArray))
				$path 			= reset( $fileArray );
			else
				$path 			=  $fileArray;

			if(!file_exists($path) || is_dir($path))
				$hasPdf = false;
		}


		if(!$hasPdf){
			return Redirect::back()->withInput()->with('whoops', 'Selecteer een .pdf bestand.');
		}

		$pdf_single   = Input::get('pdfImg');
		$pdf_multi = Input::get('pdfmultiImg');//selected files



		//unique images single (edited)
		$pdfImages = array_unique(explode(',', $pdf_single));
		$pdfImg = [];
		foreach ($pdfImages as $img) {
			$file = public_path().$img;
			if(trim($img) && file_exists($file) && !is_dir($file)){
				$baseName = basename($img, '_edit.png');
				//if multi is empty (single page) or img is selected
				if(!$pdf_multi || strpos($pdf_multi, $baseName) !== false)
					$pdfImg[] = $img;
			}
		}
		//unique images multi (not edit) and selected
		$pdfImages = array_unique(explode(',', $pdf_multi));
		$pdfImgMulti = [];
		foreach ($pdfImages as $img) {
			$file = public_path().$img;
			if(trim($img) && file_exists($file) && !is_dir($file)){
				$fileName = basename($img, '.png'); //file name without .png and directory
				//if file not exists in single (edited) files
				if(strpos($pdf_single, $fileName) === false) 
					$pdfImgMulti[] = $img;
			}
		}


		//$pdfImg = $pdf_single.','.$pdf_multi;
		$pdfImages = array_merge($pdfImg, $pdfImgMulti);
		$pdfImg = implode(',', $pdfImages);

		$bsc   = Input::get('omschrijving');
		$vor   = Input::get('voorwaarden');
		$titel = preg_replace("/[^A-Za-z0-9 -]/", '', Input::get('titel'));;
		
		$arrangements =  Input::get('arrangement');
		if(!empty($arrangements)){
		$val_arrangement = implode(',',$arrangements);
		}
		$facilities = Input::get('facilities');
		if(!empty($facilities)){
		$val_facilities = implode(',',$facilities);
		}
		
		$input = [
			'type'         => Input::get('type'),
			'titel'        => $titel,
			'omschrijving' => $bsc,
			'voorwaarden'  => $vor,
			'categorie'    => Input::get('categorie'),
			'geldig-vanaf' => Input::get('geldig-vanaf'),
			'geldig-tot'   => Input::get('geldig-tot'),
			'prijs'        => Input::get('prijs'),
			'twv'          => Input::get('twv'),
			'country'      => Input::get('country'),
			'duration'     => Input::get('duration'),
			'rating'       => Input::get('rating'),
			'arrangement'  => isset($val_arrangement) ? $val_arrangement : '',
			'facilities'   => isset($val_facilities) ? $val_facilities : '' ,
			'accomodation' => Input::get('accomodation'),
			'avatar'       => Input::file('avatar'),
			'pdfImg'        => $pdfImg,
		];

		$rules = [
			'type'         => 'required|not_in:0',
			'titel'        => 'required|max:55',
			'categorie'    => 'required|not_in:0|in:vakanties,evenementen,restaurants,hotels,avondje-uit,dagje-weg,sport,overig',
			'geldig-vanaf' => 'required|date_format:"d-m-Y"',
			'geldig-tot'   => 'required|date_format:"d-m-Y"',
			'prijs'        => 'required|numeric',
			'twv'          => 'required|numeric',
			'country'	  => 'required',
			'duration'     => 'required',
			'rating'       => 'required',
			'arrangement'  => 'required',
			'facilities'   => 'required',
			'accomodation' => 'required'
		];

		$messages = [
			'required'         => 'Alle velden zijn verplicht om in te vullen.',
			'type.not_in'      => 'Kies een type.',
			'titel.max'        => 'De titel is te lang. Max. 55 karakters',
			'categorie.not_in' => 'Kies een categorie.',
			'categorie.in'     => 'Je hebt een ongeldige categorie ingevoerd.',
			'date_format'      => 'U heeft een ongeldige datum formaat ingevoerd.',
			'numeric'          => 'De prijs en waarde indicatie mag alleen uit cijfers bestaan.',
		];

		$validator = Validator::make($input, $rules, $messages);

		if ($validator->fails())
		{
			if (is_array(Input::get('file')))
			{
				return Redirect::back()->withInput()->with('fouten', $validator->messages()->all())->with('pdfmulti', Input::get('file'));
			}
			else
			{
				return Redirect::back()->withInput()->with('fouten', $validator->messages()->all())->with('pdf', Input::get('file'));
			}
		}
		else
		{
			$ad               = new Advertentie;
			/*if (Input::has('image'))
			{
				$image = Input::get('image');

				if (strlen($image) > 255)
				{
					return Redirect::back()->withInput()->with('whoops', 'De url die je hebt ingevoerd is te lang. (Foto link)');
				}

				if (getimagesize($image) === false)
				{
					return Redirect::back()->withInput()->with('whoops', 'De url die je hebt ingevoerd is geen directe link naar een foto bestand. De link moet eindigen op een foto bestand extensie. Bijvoorbeeld: .jpg, .png, .bmp, .gif');
				}
				$ad->image = Input::get('image');
			}*/
			if (Input::hasFile('avatar')){
				if (Input::file('avatar')->isValid()) {
				  $destinationPath = 'img/uploads'; 
				  $extension = Input::file('avatar')->getClientOriginalExtension(); 
				  $name = Input::file('avatar')->getClientOriginalName();
				  $file_Name = rand(11111,99999).'.'.$extension; 
				  Input::file('avatar')->move($destinationPath, $file_Name); 
				  $ad->picture = $destinationPath.'/'.$file_Name;
				}
			}
			$ad->verkoperId   = Auth::user()->userId;
			$ad->type         = Input::get('type');
			$ad->titel        = $titel;
			$ad->titelUrl     = strtolower(trim(friendlyUrl($titel), '-'));
			$ad->omschrijving = $bsc;
			$ad->voorwaarden  = $vor;
			$ad->categorie    = Input::get('categorie');
			$ad->rating	   = Input::get('rating');
			$ad->facilities   = isset($val_facilities) ? $val_facilities : '';
			$ad->duration	 = Input::get('duration');
			$ad->country	  = Input::get('country');
			$ad->arrangement  = isset($val_arrangement) ? $val_arrangement : '';
			$ad->accomodatie  = Input::get('accomodation');
			$ad->prijs        = Input::get('prijs');
			$ad->twv          = Input::get('twv');
			$ad->geldigVanaf  = date("Y-m-d", strtotime(Input::get('geldig-vanaf')));
			$ad->geldigTot    = date("Y-m-d", strtotime(Input::get('geldig-tot')));
			$ad->createdAt    = new DateTime();
			$ad->updatedAt    = new DateTime();
			$ad->pdfImg    = $pdfImg;
			

			if($ad->save())
			{
				if (count(Input::get('file')) > 1)
				{ //it is selected from checkboxes
				    $filesArray = Input::get('file');

					$ad->multi = 1;
					$ad->count = count($filesArray);
					$ad->save();

				    foreach ($filesArray as $file)
				    {
						$pdf 				= new Pdf;
						$pdf->advertentieId = $ad->advertentieId;
						$pdf->path 			= Pdf::trimPath($file);
						$pdf->save();
				    }
				}
				else
				{ //it is from hidden input 'file'
					$pdf 				= new Pdf;
					$pdf->advertentieId = $ad->advertentieId;
					
					$fileArray = Input::get('file');
					if(is_array($fileArray))
						$pdf->path 			= Pdf::trimPath(reset( $fileArray ));
					else
						$pdf->path 			=  Pdf::trimPath($fileArray);

					$pdf->save();
				}

				$user = Auth::user();

				$data = ['user' => $user->naam, 'titel' => $titel, 'url' => $ad->advertentieId . '-' . $ad->titelUrl];

				Mail::later(10, 'emails.advertentieGeplaatst', $data, function($message) use($user)
				{
				  $message->to($user->email, $user->naam)
				          ->subject('Je advertentie is geplaatst');
				});

				return Redirect::to('/advertentie/' . $ad->advertentieId . '-' . $ad->titelUrl)->with('success', 'Je advertentie is succesvol geplaatst!');
			}
			else
			{
				return Redirect::back()->withInput()->with('whoops', 'Je advertentie kon op dit moment niet geplaatst worden, probeer het opnieuw.');
			}

		}
	}

	public function zoeken()
	{
		//$categories = DB::table('Search')->lists('categories');
		$countries = '';
		$facilities = '';
		$arrangements = '';
		$searchItmes = Search::orderBy("weight")->get();
		if($searchItmes){
			$countries = $searchItmes->lists('country');
			$facilities = $searchItmes->lists('facilities');
			$arrangements = $searchItmes->lists('arrangement');
		}
		//$stay = DB::table('Search')->lists('length_stay');
		if (Input::has('zoekterm') && Input::get('zoekterm') != '')
		{
			$search = Input::get('zoekterm');
			
			// $ads = DB::table('Advertenties')->where(function($query) use ($search) {
			// 	$query->where('titel', 'LIKE', '%' . $search . '%')->where('verkocht', '=', '0', 'AND');
			// })
			// ->orderBy('advertentieId', 'DESC')
			// ->get();
			//$ads = Advertentie::where('titel', 'LIKE', "%$search%")->where('verkocht', '=', '0', 'AND')->where('verwijderd', '=', '0', 'AND')->get();
			
			$ads[] = DB::select( DB::raw("SELECT advertentieId,verkoperId,titel,titelUrl,categorie,rating,facilities,duration,arrangement,accomodatie,country,prijs,twv,geldigTot FROM Advertenties WHERE titel like '%$search%' AND verkocht = '0' 
			AND verwijderd = '0' "));
				foreach($ads[0] as $ad){
				$arrayList['advertentieId'] = $ad->advertentieId;
				$arrayList['verkoperId'] = $ad->verkoperId;
				$arrayList['titel'] = $ad->titel;
				$arrayList['titelUrl'] = $ad->titelUrl;
				$arrayList['categorie'] = $ad->categorie;
				$arrayList['rating'] = $ad->rating;
				$arrayList['facilities'] = explode(',',$ad->facilities);
				$arrayList['duration'] = $ad->duration;
				$arrayList['country'] = $ad->country;
				$arrayList['accomodation'] = $ad->accomodatie;
				$arrayList['arrangement'] = explode(',',$ad->arrangement);
				$arrayList['prijs'] = $ad->prijs;
				$arrayList['twv'] = $ad->twv;
				$arrayList['geldigTot'] = $ad->geldigTot;
				$arrNew[] = $arrayList;
				}

			$json = "";
			if(isset($arrNew) && $arrNew)
				$json = json_encode($arrNew,JSON_NUMERIC_CHECK);

			return View::make('pages.filter')->with('data', $json)
				->with('countries' , $countries)->with('facilities' , $facilities)
				->with('arrangements' , $arrangements);	
		}
		else
		{
			return Redirect::back()->with('whoops', 'Voer wat in om te zoeken.');
		}
	}
	
	public function download($id)
	{
		if (Auth::check())
		{
			$pdf = Pdf::find($id);
			if ($pdf)
			{
				if ($pdf->koperId === Auth::user()->userId)
				{
					$file = Pdf::getBasePath($pdf->path);
					try{
					if(!file_exists($file) || is_dir($file)) 
						return Redirect::to("/account")->with('whoops' ,'Bestand niet bestaat.');
					} catch(Exception $e){
						return Redirect::to("/account")->with('whoops' ,'Bestand niet bestaat.');
					}

					return Response::download($file, null, ['Content-Type: application/pdf']);
				}
				else
				{
					return View::make('pages.nietGevonden')->with('whoops', 'Je hebt geen toegang tot dit bestand, omdat je deze niet hebt gekocht.');
				}
			}
			else
			{
				return View::make('pages.nietGevonden')->with('whoops', 'Je hebt geen toegang tot dit bestand, omdat je deze niet hebt gekocht.');
			}
		}
		else
		{
			return Redirect::to('/inloggen')->with('whoops', 'Je bent niet ingelogd.');
		}
	}

	public function bewerken($id)
	{
		$c = strlen($id);
		$adId = substr($id, 0, strpos($id, '-'));
		$adUrl 	= substr($id, strlen($adId) + 1, strpos($id, '-') + $c);
		$q = Advertentie::where('advertentieId', '=', $adId)->where('titelUrl', '=', $adUrl, 'AND')->first();

		if ($q)
		{
			function friendlyURL($string)
			{
			    $string = preg_replace("`\[.*\]`U","",$string);
			    $string = preg_replace('`&(amp;)?#?[a-z0-9]+;`i','-',$string);
			    //$string = htmlentities($string, ENT_COMPAT, 'utf-8');
			    $string = preg_replace( "`&([a-z])(acute|uml|circ|grave|ring|cedil|slash|tilde|caron|lig|quot|rsquo);`i","\\1", $string );
			    $string = preg_replace( array("`[^a-z0-9]`i","`[-]+`") , "-", $string);

			    return $string;
			}

			$bsc = Input::get('omschrijving');
			$voo = Input::get('voorwaarden');
			$titel = preg_replace("/[^A-Za-z0-9 -]/", '', Input::get('titel'));;

			$input = [
				'titel'        => $titel,
				'geldig-vanaf' => Input::get('geldig-vanaf'),
				'geldig-tot'   => Input::get('geldig-tot'),
				'prijs'        => Input::get('prijs'),
				'twv'          => Input::get('twv')
			];

			$rules = [
				'titel'        => 'required|max:55',
				'geldig-vanaf' => 'required|date_format:"d-m-Y"',
				'geldig-tot'   => 'required|date_format:"d-m-Y"',
				'prijs'        => 'required|numeric',
				'twv'          => 'required|numeric'
			];

			$messages = [
				'required' => 'Alle velden zijn verplicht om in te vullen.',
				'titel.max' => 'De titel is te lang. Max. 55 karakters',
				'date_format' => 'U heeft een ongeldige datum formaat ingevoerd.',
				'numeric' => 'De prijs en waarde indicatie mag alleen uit cijfers bestaan.'
			];

			$validator = Validator::make($input, $rules, $messages);

			if ($validator->fails())
			{
				return Redirect::back()->withInput()->with('fouten', $validator->messages()->all());
			}
			else
			{
				$ad               = $q;

				if (Input::has('image'))
				{
					$image = Input::get('image');

					if (strlen($image) > 255)
					{
						return Redirect::back()->withInput()->with('whoops', 'De url die je hebt ingevoerd is te lang. (Foto link)');
					}

					if (getimagesize($image) === false)
					{
						return Redirect::back()->withInput()->with('whoops', 'De url die je hebt ingevoerd is geen directe link naar een foto bestand. De link moet eindigen op een foto bestand extensie. Bijvoorbeeld: .jpg, .png, .bmp, .gif');
					}
					$ad->image = $image;
				}
				else
				{
					$ad->image = NULL;
				}

				$ad->titel        = $titel;
				$ad->titelUrl     = strtolower(trim(friendlyUrl($titel), '-'));
				$ad->omschrijving = $bsc;
				$ad->voorwaarden  = $voo;
				$ad->prijs        = Input::get('prijs');
				$ad->twv          = Input::get('twv');
				$ad->geldigVanaf  = date("Y-m-d", strtotime(Input::get('geldig-vanaf')));
				$ad->geldigTot    = date("Y-m-d", strtotime(Input::get('geldig-tot')));
				$ad->updatedAt    = new DateTime();
				$ad->save();
				return Redirect::to('/advertentie/' . $ad->advertentieId . '-' . $ad->titelUrl . '/bewerken')->with('success', 'Je wijzigingen zijn opgeslagen.');
			}
		}
		else
		{
			return View::make('pages.nietGevonden')->with('whoops', 'De advertentie die je wilt bewerken bestaat niet (meer).');
		}

	}

	public function advertentieVerwijderen($id)
	{
		$c = strlen($id);
		$adId = substr($id, 0, strpos($id, '-'));
		$adUrl 	= substr($id, strlen($adId) + 1, strpos($id, '-') + $c);
		$q = Advertentie::where('advertentieId', '=', $adId)->where('titelUrl', '=', $adUrl, 'AND')->first();

		if ($q)
		{
			if ($q->verkoperId === Auth::user()->userId)
			{
				$q->verwijderd = 1;
				$q->save();
				return Redirect::to('/account')->with('success', 'Je advertentie is verwijderd.');
			}
			else
			{
				return View::make('pages.nietGevonden')->with('whoops', 'Dit is niet jou advertentie, je kunt hem dus niet verwijderen.');
			}
		}
		else
		{
			return View::make('pages.nietGevonden')->with('whoops', 'De advertentie die je wilt verwijderen bestaat niet (meer).');
		}
	}
	
	public function sendingEmail()
    {
		$ads = DB::table('Advertenties')->get();           
		foreach($ads as $ad){
			$currentDate = date('Y-m-d');
		    $expireDate = $ad->geldigTot;			
			$combinedDate = $expireDate . ' ' . '-5days';
    		$diff_date = date('Y-m-d', strtotime($combinedDate));
			if($currentDate == $diff_date){
				    $userId = $ad->verkoperId;
					$date = $ad->geldigTot;
					$title = $ad->titel;
					$results = DB::select( DB::raw("SELECT * FROM Users WHERE userId = :userId"), array(
   'userId' => $userId,
 ));
 					foreach($results as $result){
						$data = ['naam' => $result->naam, 'title' => $title, 'date' => $date ];
						Mail::send('emails.advertentieExpMail', $data, function($message) use($result)
						{
						  $message->to($result->email);
						  /*$message->to("B.basakian@coupontrade.nl");*/
							  $message->subject('Uw advertentie verloopt binnen 5 dagen');
						});						
				}
			}			
		}
    }
	
	public function uploadImg()
	{ 					   
      		// get the dataURL
			$dataURL = Input::get("image"); 
			$dataName = Input::get("name");
			//12 is: '/img/pdfImg/'
			$string = substr($dataName, 12);
			$str = strstr($string,".png", true);
			 
			$str = preg_replace("/[^a-zA-Z0-9]+/", "-", trim($str));
			 
			// the dataURL has a prefix (mimetype+datatype) 
			// that we don't want, so strip that prefix off
			$parts = explode(',', $dataURL);  
			$data = $parts[1];  
			
			// Decode base64 data, resulting in an image
			$data = base64_decode($data);  
			
		
			// create a temporary unique file name
			$file = 'img/pdfImg/image/' . $str . '_edit.png';
			
			//check is unique file name
			//Then you need to keep track of copies of files
			// while(file_exists(public_path()."/".$file)){
			// 	$file = 'img/pdfImg/image/' . $str.'_'. $this->randString() .'_edit.png';
			// }
			
			// write the file to the upload directory
			$success = file_put_contents($file, $data);
			
		
			// return the temp file name (success)
			// or return an error message just to frustrate the user (kidding!)
	 if(Request::ajax()) {
			 return $success ? $file : Response::json('error', 500);
     }else{
     	$file = "/". $file;
     	$files = $file;
     	if(Input::has("pdfImg")){
     		$files .= ",".Input::get("pdfImg");
	    }

	    Session::flash("ss_pdfImg", $files);
	    if($success)
	    	Session::flash("ss_isRedirected", "success");
	    else
	    	Session::flash("ss_isRedirected","error");

	    if(!Input::get("pdfmultiImg")){
	    	Session::flash("ss_hidVal", $file);
	    }

     	return Redirect::back()->withInput();
     }

				
	}
	
	public function imageMagick()
	{
		 if(Request::ajax()) {
		 	set_time_limit(300);

      $data = Input::all();
			print_r($data);
      		// get the dataURL
			$sourcer = base_path() . $data["source"]; 
			$filename = $data["file"]; 
			 
			 	$str =  strstr($filename,".pdf",true);
			 	$str = preg_replace("/[^a-zA-Z0-9]+/", "-", trim($str));
				$uploadPath = 'img/pdfImg/'.$str. '.png';
				
				$imagick = new Imagick();
				$imagick->setResolution(650,650);
				$imagick->readImage($sourcer);
				//$imagick->flattenImages(); 
				$imagick->resizeImage(600,800,Imagick::FILTER_LANCZOS,1);
				// Imagick::resizeImage ( 200, 200,  imagick::FILTER_LANCZOS, 1, TRUE)
				$imagick->writeImage($uploadPath);
				
				$var = "/" . $uploadPath;
				
				
			 return $var;
    	 }
				
	}
	
	public function multi_pdfUpload()
	{
		 if(Request::ajax()) {
      		$data = Input::all();
			foreach($data as $a){
			$page =  $a[0];
						
			}
			die();
			$string = substr($dataName,12);
			$str = strstr($string,".png", true);
			$parts = explode(',', $dataURL);  
			$data = $parts[1];  
			$data = base64_decode($data);  
			$file = 'img/pdfImg/image/' . $str . '_edit.png';
			$success = file_put_contents($file, $data);
			return $success ? $file : 'Unable to save this image.';
    	 }
				
	}
	
	public function viewImgCoupon()
	{
		 if(Request::ajax()) {
      		$data = Input::all();
			$advertentieId = $data['id'];
			$results = DB::select( DB::raw("SELECT * FROM Advertenties WHERE advertentieId = :advertentieId"), array(
   'advertentieId' => $advertentieId,
 ));
 			$image = $results[0]->pdfImg;
			return $image;
    	 }
	}
}
	

