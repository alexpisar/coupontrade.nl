<?php

class Verkoop extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $primaryKey = "verkoopId";
	protected $table = 'Verkopen';

	/**
	 * Disable Laravel's Timestamps
	 *
	 */
	public $timestamps = false;

	public function advertentie()
	{
		return $this->hasOne('Advertentie', 'verkoperId', 'verkoperId');
	}

}