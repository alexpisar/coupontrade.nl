<?php

class Partner extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $primaryKey = "partnerId";
	protected $table = 'Partners';

	/**
	 * Disable Laravel's Timestamps
	 *
	 */
	public $timestamps = false;

	// hasOne 'Bdks'
	public function manager()
	{
		return $this->hasOne('Bdks', 'bdksId', 'managerId');
	}

	// hasMany 'Advertentie'
	public function advertenties()
	{
		return $this->hasMany('Advertentie', 'verkoperId', 'referentieId');
	}

}