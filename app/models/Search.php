<?php

class Search extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $primaryKey = "id";
	protected $table = 'Search';

	/**
	 * Disable Laravel's Timestamps
	 *
	 */
	public $timestamps = false;

}