<?php

class Actie extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $primaryKey = "actieId";
	protected $table = 'Acties';

	/**
	 * Disable Laravel's Timestamps
	 *
	 */
	public $timestamps = false;

}