<?php

class Pdf extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $primaryKey = "pdfId";
	protected $table = 'Pdfs';

	/**
	 * Disable Laravel's Timestamps
	 *
	 */
	public $timestamps = false;

	// haseOne 'Advertentie'
	public function advertentie()
	{
		return $this->hasOne('Advertentie', 'advertentieId', 'advertentieId');
	}

	public function verkoper()
	{
		return $this->hasOne('User', 'userId', 'verkoperId');
	}

	public function koper()
	{
		return $this->hasOne('User', 'userId', 'koperId');
	}

	public function pdfExists(){
		$file = $this->getPath();
		return file_exists($file) && !is_dir($file);
	}

	//get absolute path
	public static function getBasePath($file){
	   return base_path("private". $file);
	}

	public function getPath(){
		return base_path("private". $this->path);
	}

	//To convert to relative path
	public static function fixDataBasePaths(){
	  DB::transaction(function (){
		$pdfs = Pdf::all();
		foreach ($pdfs as $pdf) {
			$path = strstr($pdf->path, "/private/");
			if(!$path)
				continue;
			$pdf->path = substr($path, 8); //  "/private" == 8. Cut this.
			$pdf->save();
		}
	  });
	}

	//To convert to relative path
	public static function trimPath($file){
		$path = strstr($file, "/private/");
		if(!$path) return null;
		return substr($path, 8); // "/private" == 8. trim this.
	}

}