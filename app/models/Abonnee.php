<?php

class Abonnee extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $primaryKey = "abonneeId";
	protected $table = 'Abonnees';

	/**
	 * Disable Laravel's Timestamps
	 *
	 */
	public $timestamps = false;

}