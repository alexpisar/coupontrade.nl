<?php

class Comment extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $primaryKey = "commentId";
	protected $table = 'Comments';

	/**
	 * Disable Laravel's Timestamps
	 *
	 */
	public $timestamps = false;

	// haseOne 'User'
	public function from()
	{
		return $this->hasOne('User', 'userId', 'fromId');
	}

	// hasOne 'Partner'
	public function partner()
	{
		return $this->hasOne('Partner', 'referentieId', 'verkoperId');
	}


	public function advertentie()
	{
		return $this->belongsTo('Advertentie', 'advertentieId', 'advertentieId');
	}

}