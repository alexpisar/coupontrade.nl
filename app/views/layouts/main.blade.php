<!--
    _______                                 _______                 _
   (_______)                               (_______)               | |
    _        ___   _   _  ____    ___   ____   _   ____  _____   __| | _____
   | |      / _ \ | | | ||  _ \  / _ \ |  _ \ | | / ___)(____ | / _  || ___ |
   | |_____| |_| || |_| || |_| || |_| || | | || || |    / ___ |( (_| || ____|
    \______)\___/ |____/ |  __/  \___/ |_| |_||_||_|    \_____| \____||_____)
                         |_|

  Design en development door S. Paksa. s.paksa@coupontrade.nl
-->
<!DOCTYPE html>
<html lang="nl">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>CouponTrade</title>
	<link rel="stylesheet" href="/stylesheets/normalize.css">
	<link rel="stylesheet" href="/stylesheets/foundation.min.css">
	<link rel="stylesheet" href="/fonts/font.css">
	<link rel="stylesheet" href="/stylesheets/old.css?v2">
	<link rel="stylesheet" href="/stylesheets/cpteidb.css">
	<link rel="stylesheet" href="/stylesheets/ionicons.min.css">
    <!-- Start WOWSlider.com HEAD section -->
	<link rel="stylesheet" type="text/css" href="/engine1/style.css?v2" />
	<script type="text/javascript" src="/engine1/jquery.js"></script>
	<!-- End WOWSlider.com HEAD section -->

	<meta property="og:title" content="Nooit meer je geld kwijt aan een ongebruikte veiling of waardebon!" />
	<meta property="og:site_name" content="CouponTrade.nl"/>
	<meta property="og:url"	content="http://www.coupontrade.nl" />
	<meta property="og:description" content="Koop of verkoop ongebruikte waardebonnen, coupons, e-tickets en veilingen. Komt het je net niet uit om gebruik te maken van een arrangement dat je hebt gekocht of gewonnen? Verkoop hem makkelijk en snel door en verdien je geld terug!" />
	<meta property="fb:app_id" content="653812781393885" />
	<meta property="og:type" content="website" />
	<meta property="og:image" content="http://www.coupontrade.nl/img/logo.png" />
    <link href="/stylesheets/style.css?v2" media="screen" rel="stylesheet" type="text/css">
    <link rel="stylesheet" media="(max-width: 800px)" href="/stylesheets/mobile.css" />
    
    <?php if( isset($callfrom) && $callfrom == 'searchpagefilter'){?>
    <style type="text/css">
	.ion-search::before {
		display: block;
		line-height: 60px;
		margin-top: -5px;
	}
	</style>
    <?php }?>
</head>
<body>
	<div class="sidebar">
		<a href="/">Home</a>
		@if(Auth::check())
		<a href="/account">{{Auth::user()->naam}}</a>
		@else
		<a href="/inloggen">Inloggen</a>
		@endif
		{{-- <a href="/acties">Acties</a> --}}
		<a href="/over-ons">Over ons</a>
		<a href="/hoe-het-werkt">Hoe het werkt</a>
		<a href="/categorieen">Categoriëen</a>
		<a href="/advertentie/plaatsen">Advertentie plaatsen</a>
		<a href="/contact">Contact</a>
	</div>
	<div class="container">
		<div class="header">
			<div class="row">
				<div class="medium-4 columns logo-head">
					<div class="spinner" id="headerSpinner"></div>
					<a href="/" class="brand">Cou<span>pon</span><span>Trade</span></a>
					<a id="toggleSidebar" class="sidebar-toggle ion-android-more-vertical"></a>
				</div>
				<div class="medium-8 columns">
					<ul class="inline-list">
						<li ><a href="/zoeken"><span class="ion-search"></span></a></li>
						{{-- <li><a href="/acties">Acties</a></li> --}}
						<li><a href="/hoe-het-werkt">Hoe het werkt</a></li>
						<li><a href="/categorieen">Categoriëen</span></a></li>
						@if(Auth::check())
						<li><a href="/account">{{Auth::user()->naam}} <div class="picture" style="background:url({{Auth::user()->pictureUrl}}) 50% 50% / cover no-repeat;"></div></a></li>
						@else
						<li><a href="/inloggen">Inloggen</a></li>
						@endif
						<li><a href="/advertentie/plaatsen" class="button-green radius color">Een advertentie plaatsen</a></li>
					</ul>
				</div>
			</div>
		</div>
		@yield('page')
		<div class="footer">
			<a href="/advertentie/plaatsen">Advertentie plaatsen</a>
			<a href="/over-ons">Over ons</a>
			<a href="/hoe-het-werkt">Hoe het werkt</a>
			<a href="/veelgestelde-vragen">Veelgestelde vragen</a>
			<a href="/algemene-voorwaarden">Algemene voorwaarden</a>
			<a href="/contact">Contact</a>
			<a href="javascript:void(0)">&copy; CouponTrade 2015</a>
		</div>
	</div>
	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		ga('create', 'UA-57925078-1', 'auto');
		ga('send', 'pageview');
	</script>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script src="/js/vendor/modernizr.js"></script>
	<script src="/js/cpteidb.js"></script>
	@yield('scripts')
</body>
</html>