<!DOCTYPE html>
<html lang="nl-NL">
	<head>
		<meta charset="utf-8">
		<style>
			html, body {
				width: 100%;
				height: 100%;
				font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
				font-size: 16px;
				margin: 0;
				padding: 0;
				padding-bottom: 50px;
			}
			h1 {
				font-size: 2em;
				font-style: bold;
			}
			p {
				font-size: 0.85em;
			}
			b {
				font-style: bold;
			}
			a {
				text-decoration: none;
				color: #3498DB;
			}
			a > span:first-of-type {
				color: #E87E04;
			}
			a > span:last-of-type {
				color: white;
				font-weight: 900;
			}
			.row {
				width: 100%;
				max-width: 960px;
				padding: 0 10px;
				margin: 0 auto;
			}
			.header {
				width: 100%;
				height: 50px;
				line-height: 50px;
				background: black;
			}
			.well {
				margin: 10px auto;
				width: 100%;
				max-width: 960px;
				padding: 20px;
				background: #FFFFFF;
				border: 1px solid #d5d5d5;
				border-radius: 2px;
				box-sizing: border-box;
				-webkit-border-radius: 2px;
				-moz-border-radius: 2px;
				-ms-border-radius: 2px;
				-o-border-radius: 2px;
			}
		</style>
	</head>
	<body>
		<div style="padding: 0 10px;">
			<div class="well">
				<div class="row">
					<h2>Beste {{$naam}},</h2>
					<p style="font-size:14px" >

					 	Er is via Coupontrade.nl een aanvraag gedaan om jouw wachtwoord te wijzigen. We hebben jouw wachtwoord veranderd.
                        Je kunt nu inloggen met de volgende gegevens: 
						<br><br>
                        <strong>E-mail adres: </strong> {{$email}}<br>
                        <strong>Wachtwoord: </strong> {{$password}} 
						<br><br>
                        Je kunt een nieuw wachtwoord instellen door op de onderstaande link te klikken. 
                        <br>
						<a href="http://www.coupontrade.nl/change-password" >Uw wachtwoord opnieuw instellen</a> 
						<br><br>
                        Met vriendelijke groet, 
                        <br><br>
                        Het coupontrade team
                        
                    </p>
				</div>
			</div>
			<br><br><br><br>
		</div>
	</body>
</html>