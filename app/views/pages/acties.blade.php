@extends('layouts.main')

@section('page')
	<div class="page">
		<div class="row">
			<div class="medium-12 columns">
				@if (count($acties) > 0)
					@foreach ($acties as $actie)
					<div class="actie">
						<div class="picture" style="background:url('{{$actie->fotoUrl}}') 50% 50% / cover no-repeat;height:300px;"></div>
						<div class="content">
							<h2>{{$actie->titel}}</h2>
							<br><hr><br>
							<div class="row">
								<div class="medium-3 columns text-left">
									<h3>Voorwaarden</h3>
									{{$actie->voorwaarden}}
									<br>
									<h3>Deelnemen?</h3>
									Klik op de knop hieronder; er wordt een bericht op je Facebook geplaatst. Degene die de meeste Likes binnenhaalt wint!
									<br><br>
									<a id="fbDialog({{ $actie->actieId }})" class="button button-actie">Ik doe mee!</a>
								</div>
								<div class="medium-9 columns text-left">
									<h3>Omschrijving</h3>
									{{$actie->omschrijving}}
								</div>
							</div>
						</div>
					</div>
					<br>
					@endforeach
				@else
					<div class="geen-advertenties">
						<h1>
							<i class="ion-sad-outline"></i>
						</h1>
						<h4>
							Er zijn momenteel geen lopende acties!
						</h4>
					</div>
				@endif
			</div>
		</div>
	</div>
@stop

@section('scripts')
<script>
	function fbDialog(id)
	{
		var url = 'http://www.facebook.com/dialog/feed?app_id=' + FBVars.fbAppId +
			'&link=' + FBVars.fbShareUrl +
			'&picture=' + FBVars.fbShareImg +
			'&name=' + encodeURIComponent(FBVars.fbShareName) + 
			'&caption=' + encodeURIComponent(FBVars.fbShareCaption) + 
			'&description=' + encodeURIComponent(FBVars.fbShareDesc) + 
			'&redirect_uri=' + FBVars.baseURL + 'PopupClose.html' + 
			'&display=popup'; 

		return url; 
	}
</script>
@stop