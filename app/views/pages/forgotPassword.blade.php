@extends('layouts.main')

@section('page')
	<div class="page">
		<div class="medium-4 columns">
        <div class="row padding">
				<div class="well">
					<h4>Voer je e-mail adres in </h4>
					<form action="{{ URL::route('account-forgot-password-post') }}" method="post">
                    <div class="field">
                        <input type="text" name="email" value=""> 
                        @if($errors->has('email'))
                            Email adres is verplicht om in te vullen.
                        @endif
                    </div>
                
                    <input type="submit" class="button button-submit full-width radius" value="Herstellen">
                        {{ Form::token() }}
                    </form>
                    </div></div>
			</div>
	</div>
@stop
