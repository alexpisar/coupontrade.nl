@extends('layouts.main')

@section('page')
	<div class="page">
    	<div class="row">
				<div class="medium-10 medium-offset-1 columns text-center">
					<h1>Je hebt betaald!</h1>
				</div>
		</div>
		<!--<div class="hero">
			<div class="row">
				<div class="medium-10 medium-offset-1 columns text-center">
					<h1>Je hebt betaald!</h1>
				</div>
			</div>
		</div>-->
		<div class="row padding">
			<div class="medium-10 medium-offset-1 columns well">
				<h3>Gefeliciteerd met je aankoop</h3>
				<p>
					Een bevestiging van je aankoop is verstuurd naar je e-mail adres <b>{{Auth::user()->email}}</b>.
					<br>
					Je kunt je aankoop downloaden via <a href="/account">jouw account</a>.
					<br><br>
					Wij wensen je veel plezier!
				</p>
			</div>
		</div>
	</div>
@stop