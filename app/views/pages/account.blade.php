@extends('layouts.main')

@section('page')
	<div class="hide">
		{{Form::open(['url' => '/account/wijzig/foto', 'files' => true])}}
		{{Form::file('foto', ['id' => 'accountFotoInput'])}}
		{{Form::close()}}
	</div>
	<div class="page">
    <div class="small-12 columns text-center">
        <div class="picture" id="changePicture" style="background:url({{Auth::user()->pictureUrl}}) 50% 50% / cover;"><div class="white-overlay" id="pictureOverlay" style="display:none;"></div></div>
        <h1>{{Auth::user()->naam}}</h1>
        <div class="progress" style="height:2px;width:300px;margin:0 auto;background:none;border:none;" id="accountPictureProgress">
            <span class="meter" style="height:2px;width:0%;background:rgba(255,255,255,0.5);" id="meter"></span>
        </div>
	</div>
		<!--<div class="hero">
			<div class="small-12 columns text-center">
				<div class="picture" id="changePicture" style="background:url({{Auth::user()->pictureUrl}}) 50% 50% / cover;"><div class="white-overlay" id="pictureOverlay" style="display:none;"></div></div>
				<h1>{{Auth::user()->naam}}</h1>
				<div class="progress" style="height:2px;width:300px;margin:0 auto;background:none;border:none;" id="accountPictureProgress">
					<span class="meter" style="height:2px;width:0%;background:rgba(255,255,255,0.5);" id="meter"></span>
				</div>
			</div>
		</div>-->
		<br>
		<div class="row">
			<div class="medium-10 medium-offset-1 columns">
				@if(Session::has('whoops'))
				  <?php $whoops = Session::get('whoops'); ?>
					<div data-alert class="alert-box alert radius">
						@if(is_array($whoops))
						 @foreach($whoops as $msg)
							<div>{{$msg}}</div>
						 @endforeach
						@else
						  {{$whoops}}
						@endif
					</div>
				@elseif(Session::has('success'))
					<div data-success class="alert-box success radius">
						{{Session::get('success')}}
					</div>
				@endif
			</div>
		</div>
		<div class="row">
			<div class="medium-10 medium-offset-1 columns">
				<div class="well">
					<h3>Account instellingen <div class="right"><small><a href="/uitloggen"><b>Uitloggen</b></a></small></div></h3>
					<hr>
					<div class="row">
						{{Form::open(['url' => '/account/wijzig/gegevens'])}}
						<div class="medium-4 columns">
							<label>Weergavenaam</label>
							{{Form::text('naam', Auth::user()->naam, ["required" => "required", "maxlength"=> "128"])}}
							<label>Woonplaats</label>
							{{Form::text('woonplaats', Auth::user()->woonplaats, ["required" => "required", "maxlength"=> "64"])}}
						</div>
						<div class="medium-4 columns">
							<label>IBAN rekeningnummer</label>
							{{Form::text('iban', Auth::user()->iban, ["required" => "required", "maxlength"=> "32"])}}
							<label>Naam bankhouder</label>
							{{Form::text('bankhouder', Auth::user()->bankhouder, ["required" => "required", "maxlength" => "128"])}}
						</div>
						<div class="medium-4 columns">
							<label>&nbsp;</label>
							{{Form::submit('Wijzigingen opslaan', ['class' => 'button button-submit radius'])}}
						</div>
						{{Form::close()}}
					</div>
				</div>
			</div>
		</div>
		@if(count($aankopen) > 0)
		<br>
		<div class="row">
			<div class="medium-10 medium-offset-1 columns">
				<div class="well">
					<h3>Mijn aankopen</h3>
					<hr>
					<table>
						<thead>
							<tr>
								<th>Titel</th>
								<th>Omschrijving</th>
								<th>Aankoop prijs</th>
								<th>Gekocht op</th>
								<th>Download</th>
							</tr>
						</thead>
						<tbody>
							@foreach($aankopen as $aankoop)
							<tr>
								<td><a href="/advertentie/{{$aankoop->advertentie->advertentieId}}-{{$aankoop->advertentie->titelUrl}}">{{$aankoop->advertentie->titel}}</a></td>
								<td>{{strip_tags(strlen($aankoop->advertentie->omschrijving) > 130 ? substr($aankoop->advertentie->omschrijving,0,130)."..." : $aankoop->advertentie->omschrijving)}}</td>
								<td>&euro;{{$aankoop->advertentie->prijs}}</td>
								<td>{{date("d-m-Y", strtotime($aankoop->updatedAt))}}</td>
								<td><a href="/download/{{$aankoop->pdfId}}">Download</a></td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
		@endif
		<br>
		<div class="row">
			<div class="medium-10 medium-offset-1 columns">
				<div class="well">
					<h3>Mijn advertenties</h3>
					<hr>
					@if(isset($ads) && count($ads) > 0)
					<table>
						<thead>
							<tr>
								<th>Titel</th>
								<th>Omschrijving</th>
								<th>Verkoopprijs</th>
								<th>Geplaatst op</th>
								<th>Aantal</th>
								<th>Status</th>
								<th>Bewerken</th>
							</tr>
						</thead>
						<tbody>
							@foreach($ads as $ad)
							<tr>
								<td><a href="/advertentie/{{$ad->advertentieId}}-{{$ad->titelUrl}}">{{$ad->titel}}</a></td>
								<td>{{strip_tags(strlen($ad->omschrijving) > 130 ? substr($ad->omschrijving,0,130)."..." : $ad->omschrijving)}}</td>
								<td>&euro;{{$ad->prijs}}</td>
								<td>{{date("d-m-Y", strtotime($ad->createdAt))}}</td>
								<td>
								@if($ad->count)
								{{$ad->count}}
								@else
								1
								@endif
								</td>
								<td>
								@if($ad->verkocht == 0)
								Te koop
								@else
								Verkocht
								@endif
								</td>
								@if($ad->verkocht == 0)
								<td><a href="/advertentie/{{$ad->advertentieId}}-{{$ad->titelUrl}}/bewerken">Bewerken</a></td>
								@else
								<td><span style="text-decoration:line-through;">Bewerken</span></td>
								@endif
							</tr>
							@endforeach
						</tbody>
					</table>
					@else
					<p>
						U heeft nog geen advertenties geplaatst.
					</p>
					@endif
				</div>
			</div>
		</div>
	</div>
@stop

@section('scripts')
	<script>
		$(document).ready(function() {
			$('#changePicture').click(function() {
				$('#accountFotoInput').click();
			});
			$('#accountFotoInput').on('change', function(ev) {
				var ext = $('#accountFotoInput').val().split('.').pop().toLowerCase();
				if (!(['jpg', 'jpeg'].indexOf(ext) > -1))
				{		         
					alert('Selecteer een .jpg of .jpeg bestand.');
					$('#accountFotoInput').val("");		         
				}
				else
				{
					var file = ev.target.files[0];
			        var reader = new FileReader();

			        reader.onload = (function(file){
			            return function(e){
			                $('#changePicture').css('background', 'url(' + e.target.result + ') center center / cover no-repeat');
			                setTimeout(function() {
			                	$('#pictureOverlay').fadeIn();

						        $.fn.upload = function(remote,data,successFn,progressFn)
						        {
									if(typeof data != "object")
									{
										progressFn = successFn;
										successFn = data;
									}

									return this.each(function()
									{
										if($(this)[0].files[0])
										{
											var formData = new FormData();
											formData.append($(this).attr("name"), $(this)[0].files[0]);

											// if we have post data too
											if(typeof data == "object") {
												for(var i in data) {
													formData.append(i,data[i]);
												}
											}

											// do the ajax request
											$.ajax({
												url: remote,
												type: 'POST',
												xhr: function() {
													myXhr = $.ajaxSettings.xhr();

													if(myXhr.upload && progressFn)
													{
														myXhr.upload.addEventListener('progress',function(prog) {
															var value = ~~((prog.loaded / prog.total) * 100);
															if (progressFn && typeof progressFn == "function")
															{
																progressFn(prog,value);
															}
															else if (progressFn)
															{
																$(progressFn).val(value);
															}
														}, false);
													}
													return myXhr;
												},
												data: formData,
												dataType: "json",
												cache: false,
												contentType: false,
												processData: false,
												complete : function(res)
												{
													var json;
													try {
														json = JSON.parse(res.responseText);
													} catch(e) {
														json = res.responseText;
													}
													if(successFn) successFn(json);
												}
											});
										}
						            });
						        }

								$('#accountFotoInput').upload('/account/wijzig/foto',
									function(success) {
										$('#pictureOverlay').fadeOut();
										console.log(success);
									}, 
									function(prog, value) {
										$('#accountPictureProgress').css('width', value + '%');
									},
									function(error) {
										console.log(error);
									}
								);
			                }, 500);
			            }
			        })(file);

			        reader.readAsDataURL(file);
			    }
			});
		});
	</script>
@stop