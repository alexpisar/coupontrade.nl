@extends('layouts.main')
<link rel="stylesheet" href="/stylesheets/style1.css"> <link rel="stylesheet" type="text/css" href="/source/jquery.fancybox.css?v=2.1.5" media="screen" />
<style type="text/css">
	.fancybox-custom .fancybox-skin {
		box-shadow: 0 0 50px #222;
	}
	
</style>
@section('page')
	<div class="page">
		<div class="row">
			<div class="medium-12 columns">
				@if(Session::has('custom-errors'))
				<div class="alert-box alert radius">
					<ul style="margin:0 1em;">
						@foreach(Session::get('custom-errors') as $error)
						<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
				<br>
				@endif
				@if(Session::has('custom-success'))
				<div class="alert-box success radius">
					<ul style="margin:0 1em;">
						@foreach(Session::get('custom-success') as $success)
						<li>{{ $success }}</li>
						@endforeach
					</ul>
				</div>
				<br>
				@endif
				<div class="actie advertentie-page" style="margin-top:-20px;">
					<div class="content zwer">
						<div class="row">
                        <div class="medium-9 columns text-left e-padding">
                        
                        		<div class="pl1">
									<div class="text-left">
										<h1>{{$ad->titel}}</h1>										
									</div>
								</div>

								@if($coupon_imgs)
								  <a href="#" class='picture-link'>
									<div class="picture">
									  @foreach($coupon_imgs as $img)
	                                	<img src="{{ $img }}" />
	                                  @endforeach
	                                </div>
                                  </a>
								@elseif( $ad->picture && file_exists(public_path().'/'.$ad->picture))
									<div class="picture">
                                		<img src="/{{ $ad->picture }}" />
	                                </div>
                                <br/>
                                
                                <p>
									Aangeboden door <b>{{$ad->verkoper->naam}}
								</p>
                                
								@else
									<div class="picture">
		                                <img src="/img/categorie/{{$ad->categorie}}.jpg" />
	                                </div>
	                                <br/>
                                <p>
									Aangeboden door <b>{{$ad->verkoper->naam}}
								</p>
								@endif
								
								<hr><br>
								<div class="pl1">
									<h4>Vraag & Antwoord</h4>
									<div class="vraagnantwoord">
										{{ Form::open(['url' => '/advertentie/' . $ad->advertentieId . '/comment', 'class' => 'form']) }}
											{{ Form::hidden('toId', $ad->verkoperId) }}
											{{ Form::textarea('comment', '', ['rows' => '2']) }}
											@if (Auth::check())
											{{ Form::submit('Verzenden', ['class' => 'button button-submit right radius']) }}
											@else
											{{ Form::submit('Log in om een vraag te stellen.', ['class' => 'button button-submit right radius', 'disabled' => 'true']) }}
											@endif
										{{ Form::close() }}
										<br><br><br>
										<div class="well" id="commentsList" style="padding-bottom: 0;">
											@if(count($ad->comments()->get()) > 0)
											@foreach($ad->comments()->get() as $comment)
											<div class="comment" id="comment-id-{{ $comment->commentId }}">
												<div class="commenter" style="background:url({{ $comment->from->pictureUrl }}) 50% 50% / cover no-repeat;"></div>
												<div class="content">
													<b>{{ $comment->from->naam }}</b>
													<br>
													op {{ date('d-m-Y', strtotime($comment->createdAt)) }}
												</div>
												<div class="comment-content">
													<p>{{ $comment->comment }}</p>
													<a onclick="replyToComment({{ $comment->commentId }})">hierop antwoorden</a>
												</div>
												<div class="hide" id="reply-{{ $comment->commentId }}">
													{{ Form::open(['url' => '/advertentie/' . $ad->advertentieId . '/comment', 'class' => 'form']) }}
														{{ Form::hidden('toId', $comment->from->userId) }}
														{{ Form::textarea('comment', '', ['rows' => '2']) }}
														@if (Auth::check())
														{{ Form::submit('Verzenden', ['class' => 'button button-submit right radius']) }}
														@else
														{{ Form::submit('Log in om een vraag te stellen.', ['class' => 'button button-submit right radius', 'disabled' => 'true']) }}
														@endif
														<br>
													{{ Form::close() }}
												</div>
											</div>
											@endforeach
											@else
											<p>Er zijn nog geen vragen gesteld.</p>
											@endif
											<div class="clearfix"></div>
										</div>
									</div>
								</div>
							</div>
						  @if($ad->verkocht == '0')
							<div class="medium-3 columns text-center">
								<div class="pl2"><br/>
                                <h6>Geldig tot: &nbsp;
                                	{{ date("d/m/Y", strtotime($ad->geldigTot))}} 
                                	</h6>                               
                                T.w.v. &euro;<b>{{$ad->twv}}</b> voor
									<div class="prijzer"><span>&euro;</span>{{$ad->prijs}}</div>
									{{Form::open(['url' => '/advertentie/' . $ad->advertentieId . '-' . $ad->titelUrl . '/betalen'])}}
									@if($ad->multi == '1')
									<span style="line-height:2.75em;">Nog {{$ad->count}} beschikbaar.</span>
									<select name="aantal">
										<option value="0">Hoeveel wil je er kopen?</option>
										@foreach(range(1, $ad->count) as $opt)
										<option value="{{$opt}}">{{$opt}}</option>
										@endforeach
									</select>
									@endif
									{{Form::submit('Betalen met iDeal', ['class' => 'button button-buys radius orange'])}}
									{{Form::close()}}
                                    <!--<button id="viewCoupon" class="fancybox"> View Coupon </button>-->
                                 	<p id='boxImages'>
   									  @foreach($coupon_imgs as $i => $img)
   									  	@if($i == 0)
   									  		<a href="{{$img}}" class='fancybox' data-fancybox-group='gallery'><button class='button button-buys radius'> Vergroot Coupon </button></a>
   									  	@else
   									  		<a href="{{$img}}" class='fancybox' data-fancybox-group='gallery'></a>
	                                	@endif
	                                  @endforeach
                                    </p>
                                    <input type="hidden" id="hidId" value="{{$ad->advertentieId}}"  />
									<hr>
									<h3 class="text-left" style="margin-top: 0;margin-bottom: 5px;"><b>Voorwaarden</b></h3>
									<div style="text-align:left;">{{$ad->voorwaarden}}</div>
                                    <br />
									<h3 class="text-left" style="margin-top: 0;margin-bottom: 5px;">
										<b>Omschrijving</b>
										<br>
										<small style="line-height:1;">Vragen over de advertentie kunt u gerust onder de omschrijving stellen.</small>
									</h3>
									<div style="text-align:left;">{{$ad->omschrijving}}</div>
                                    
									<br>
                                    <div class="like-button" style="margin-left:2px;">
                						<iframe src=	"http://www.facebook.com/plugins/like.php?href=http%3A%2F%2Fwww.coupontrade.nl%2Fadvertentie%2F{{$ad->advertentieId}}-{{$ad->titelUrl}}&amp;width=225&amp;layout=button&amp;action=like&amp;show_faces=true&amp;share=true&amp;height=21&amp;appId=653812781393885" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:225px; height:21px;" allowTransparency="true"></iframe>
            						</div>
								</div>
							</div>
						@else
							<div class="medium-3 columns text-left"><br/>
                            <h6>Valid until &nbsp;
                                	{{ date("d/m/Y", strtotime($ad->geldigTot))}}
                                	</h6>                                 
								T.w.v. &euro;<b>{{$ad->twv}}</b> voor
								<div class="prijzer"><span>&euro;</span>{{$ad->prijs}}</div>
								<button class="button button-blue radius" disabled>Dit aanbod is uitverkocht</button>
							</div>
						@endif
							
						</div>
					</div>
				</div>
			</div>
		</div>
		@if(count($meer) > 0)
		<div class="row">
			<div class="medium-12 no-side-padding">
			<hr>
			<h3>&nbsp;<small>Andere advertenties in de categorie <a href="/categorie/{{$ad->categorie}}">{{ucwords(str_replace('-', ' ', $ad->categorie))}}</a></small></h3>
				@foreach($meer as $bon)
					@if($bon->advertentieId !== $ad->advertentieId)
						@include("pages.partials.bon")
					@endif
				@endforeach
				<div class="medium-4 columns end">
					<a href="/advertentie/plaatsen" class="bon-plus-link">
						<div class="bon-plus">
							<i class="ion-ios-plus-outline"></i>
							<br>
							<span>Er zijn verder geen advertenties in deze categorie... Plaats de jouwe gratis!</span>
						</div>
					</a>
				</div>
			</div>
		</div>
		@endif
        
        
	</div>
@stop

@section('scripts')
<script type="text/javascript" src="/lib/jquery-1.10.1.min.js"></script>
<script type="text/javascript" src="/source/jquery.fancybox.js?v=2.1.5"></script>
<script>
	function replyToComment(id)
	{
		$('#reply-' + id).toggleClass('hide');
	}
</script>
<script type="text/javascript">

$(document).ready(function() {
	 $('.fancybox').fancybox();
	 
{{--	 
	  var id = $('#hidId').val();
    $.ajax({
		  type: "POST",
		  url: "viewCoupon",
		  data: {id: id
		 }
		}).done(function( respond ) {
			var pdf = respond.split(',');
			var pages = [];
			
			$.each(pdf,function(key,value){
				if(typeof(value) !== 'undefined'){
				var v = value.split('_edit.png');
				if(value!= v[0]){
						var p = value.substring(0,value.indexOf("_edit.png"));
						var r = p.substring(0,p.indexOf("/image/"));
						var q = p.substr(17);
						var field = r + q + '.png';
					}
				}
			var check = $.inArray(field,pdf);	
			if(check != -1){
				pdf.splice(check,1);
			}
		});
			var length = pdf.length - 1;
			$.each(pdf,function(i,k){
				if(i<length){
					pages.push(k)
				}
			});
			$.each(pages,function(index,val){
				var value = val.replace(/\s/g,"%20");
				if(index == 0){
					$("#boxImages").append("<a href="+value+" class='fancybox' data-fancybox-group='gallery'><button class='button button-buys radius'> Vergroot Coupon </button></a>");
					//$(".picture-link").attr('href', value);
				}
				if(index > 0){
				$("#boxImages").append("<a href="+value+" class='fancybox' data-fancybox-group='gallery'></a>");
				}

				});
			
		});

--}}
	$(".picture-link").on("click", function (e) {
		e.preventDefault();
		$("#boxImages>a>button").trigger('click');
	})

});

	
</script>
@stop