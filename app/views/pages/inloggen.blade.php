@extends('layouts.main')

@section('page')
	<div class="page">
		<div class="row">
			<div class="small-12 columns">
				@if(Session::has('whoops') || Session::has('fouten'))
					<div data-alert class="alert-box alert radius">
					  	@if(Session::has('whoops'))
							{{Session::get('whoops')}}
					  	@endif
						@if(Session::has('fouten'))							
						<?php $fouten = []; ?>
						@foreach(Session::get('fouten') as $error)
						<?php $fouten[] = $error; ?>
						@endforeach
						<?php $foten = array_unique($fouten) ?>
						@foreach($foten as $fout)
							{{$fout}}
						@endforeach
						@endif
					  <a href="#" class="close">&times;</a>
					</div>
				@endif
                @if(Session::has('result'))
					<div data-alert class="alert-box success radius">
                        @if(Session::has('result'))
                        	{{Session::get('result')}}
                        @endif
                    </div>
                @endif
               </div>
			<div class="medium-4 columns">
				<div class="well">
					<h3>Inloggen</h3>
					{{Form::open(['url' => '/inloggen'])}}
					<label>E-mail adres</label>
					{{Form::email('email', '', ['class' => 'radius'])}}
					<label>Wachtwoord</label>
					{{Form::password('password', ['class' => 'radius'])}}
					{{Form::submit('Inloggen', ['class' => 'button button-submit full-width radius'])}}
					{{Form::close()}}
                    <!--@if (Auth::check())
                    {
                       <a href="change-password" id="resetPass">Reset wachtwoord</a>
                    } 
                    @endif  -->                 
                    <a href="forgot-password" id="frgtPass">Wachtwoord vergeten?</a>
					<a href="{{Facebook::getLoginUrl()}}" class="button button-facebook radius full-width" style="margin-top:-35px;"><span class="ion-social-facebook"></span> Inloggen met Facebook</a>
				</div>
			</div>
			<div class="medium-8 columns">
				<div class="well">
					<h3>Nog geen account?</h3>
					<p>
						Vul dan je gegevens hieronder in. Een profiel foto is verplicht, omdat wij van CouponTrade het belangrijk vinden dat onze gebruikers weten met wie zij zaken doen. Na het registreren wordt je automatisch ingelogd.
					</p>
					{{Form::open(['url' => '/registreren', 'files' => true])}}
					<div class="row">
						<div class="small-12 text-center columns">
							<div class="register-picture" style="background:url(img/profilePicture.jpg) center center / cover no-repeat;"></div>
							<div class="inpfile">
								<a class="button button-submit radius" id="profielRegistrerenFoto">Kies een profiel foto</a>
								{{Form::file('foto', ['id' => 'profielRegistrerenFotoInput'])}}
							</div>
						</div>
						<div class="small-6 columns">
							<label>Voornaam:</label>
							{{Form::text('voornaam', '', ['class' => 'radius'])}}
						</div>
						<div class="small-6 columns">
							<label>Achternaam:</label>
							{{Form::text('achternaam', '', ['class' => 'radius'])}}
						</div>
						<div class="small-12 columns">
							<label>E-mail adres:</label>
							{{Form::email('email', '', ['class' => 'radius'])}}
						</div>
						<div class="small-12 columns">
							<label>Wachtwoord:</label>
							{{Form::password('wachtwoord', '', ['class' => 'radius'])}}
						</div>
						<div class="small-12 columns">
							<label>Wachtwoord herhalen:</label>
							{{Form::password('wachtwoord_confirmation', '', ['class' => 'radius'])}}
						</div>
						<div class="small-12 columns">
							<label>Woonplaats:</label>
							{{Form::text('woonplaats', '', ['class' => 'radius'])}}
							<hr>
							{{Form::submit('Registreren', ['class' => 'button button-submit radius'])}}
						</div>
					</div>
					{{Form::close()}}
				</div>
			</div>
		</div>
	</div>
@stop

@section('scripts')
	<script>
		$(document).ready(function() {
			$('#profielRegistrerenFoto').click(function() {
				$('#profielRegistrerenFotoInput').click();
			});
			$('#profielRegistrerenFotoInput').on('change', function(ev) {
				var file = ev.target.files[0];
				var ext = $('#profielRegistrerenFotoInput').val().split('.').pop().toLowerCase();
				if (!(['jpg', 'jpeg'].indexOf(ext) > -1))
				{		         
					alert('Selecteer een .jpg of .jpeg bestand.');
					$('#accountFotoInput').val("");		         
				}
				else
				{
			        var reader = new FileReader();
			        reader.onload = (function(file){
			            return function(e){
			                $('.register-picture').css('background', 'url(' + e.target.result + ') center center / cover no-repeat');
			            }
			        })(file);
			        reader.readAsDataURL(file);
			    }
			});
		});
	</script>
@stop