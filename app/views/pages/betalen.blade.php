@extends('layouts.main')

@section('page')
	<div class="page">
    	<div class="row">
				<div class="small-12 columns text-center">
					<h1>Betalen met iDeal</h1>
				</div>
		</div>
		<!--<div class="hero">
			<div class="row">
				<div class="small-12 columns text-center">
					<h1>Betalen met iDeal</h1>
				</div>
			</div>
		</div>-->
		<div class="row padding">
			<div class="medium-8 medium-offset-2 columns well">
				{{Form::open(['url' => '/advertentie/' . $ad->advertentieId . '-' . $ad->titelUrl . '/betalen/transactie'])}}
					<div class="left">
						<b>Verkoper</b><br>
						<b>Titel</b><br>
						<b>Aantal</b><br>
						<b>Voor de prijs van</b><br>
						{{-- <b>+ 5% bemiddelingskosten</b> --}}
					</div>
					<div class="right text-right">
						@if($ad->type == 5)
						{{$ad->partner->naam}}<br>
						@else
						{{$ad->verkoper->naam}}<br>
						@endif
						{{$ad->titel}}<br>
						@if(Session::has('aantal'))
						{{Session::get('aantal')}}<br>
						&euro; {{number_format($ad->prijs * Session::get('aantal'), 2)}}<br><br>
						{{-- &euro; {{number_format($ad->prijs * Session::get('aantal') * 0.05, 2)}} --}}
						<h1 class="prijzer"><span>&euro;</span>{{number_format($ad->prijs * Session::get('aantal'), 2)}}</h1><br>
						@else
						1<br>
						&euro; {{number_format($ad->prijs, 2)}}<br>
						{{-- &euro; {{number_format($ad->prijs * 0.05, 2)}}<br><br> --}}
						<h1 class="prijzer"><span>&euro;</span>{{number_format($ad->prijs, 2)}}</h1><br>
						@endif
					</div>
					<hr>
					<p>Kies uw bank:</p>
					<select name="bank">
						<option value="0">Kies je bank...</option>
						@foreach($banken as $bank)
						<option value="{{$bank['Id']}}">{{$bank['Name']}}</option>
						@endforeach
					</select>
					{{Form::submit('Betalen met iDeal', ['class' => 'button button-blue radius'])}}
				{{Form::close()}}
			</div>
		</div>
	</div>
@stop