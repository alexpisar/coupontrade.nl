@extends('layouts.main')

@section('page')
	<div class="page">
        <div class="row">
				<div class="medium-10 medium-offset-1 columns text-center">
					<h1>Alle categorieën</h1>
				</div>
	    </div>
		<!--<div class="hero">
			<div class="row">
				<div class="medium-10 medium-offset-1 columns text-center">
					<h1>Alle categorieën</h1>
				</div>
			</div>
		</div>-->
		<div class="row smallnav">
			<div class="medium-12 columnss">
					<a href="/categorieen">Alle</a>
				@foreach($cats as $c)
					<a href="/categorie/{{$c}}">{{str_replace('-', ' ', $c)}}</a>
				@endforeach
			</div>
		</div>
		<div class="row padding">
		@if(count($ads) > 0)
			@foreach($ads as $bon)
				@include("pages.partials.bon")
			@endforeach
			<div class="medium-4 columns end">
				<a href="/advertentie/plaatsen" class="bon-plus-link">
					<div class="bon-plus">
						<i class="ion-ios-plus-outline"></i>
						<br>
						<span>Er zijn verder geen recente advertenties... Plaats de jouwe gratis!</span>
					</div>
				</a>
			</div>
		@else
			<div class="geen-advertenties">
				<h1>
					<i class="ion-sad-outline"></i>
				</h1>
				<h4>
					Helaas... We zijn uitverkocht!
				</h4>
				<p>
					Coupon, e-ticket of een gewonnen veiling te koop? <a href="/advertentie/plaatsen">Plaats gratis een advertentie!</a>
				</p>
			</div>
		@endif
		</div>
	</div>
@stop