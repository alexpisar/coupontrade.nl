@extends('layouts.main')

@section('page')
	<div class="page">
		<div class="medium-4 columns">
        <div class="row padding">
				<div class="well">
					<h3>Maak een nieuw wachtwoord aan </h3>
                    <form action="{{ URL::route('account-change-password-post') }}" method="post">
                        <div class="field">
                           Vul hieronder uw tijdelijke wachtwoord in
                              <input type="password" name="old_password" > 
                            @if($errors->has('old_password'))
                                er wordt automatisch een wachtwoord gegenereerd verplicht.
                             @endif
                        </div>
                
                        <div class="field">
                            Nieuw wachtwoord : <input type="password" name="password" > 
                            @if($errors->has('password'))
                                Het is verplicht om een wachtwoord in te vullen en de  dient minimaal 6 karakters lang te zijn
                            @endif
                        </div>
                
                         <div class="field">
                            Nieuw wachtwoord opnieuw : <input type="password" name="password_again" > 
                            @if($errors->has('password_again'))
                                Vul nogmaals je nieuwe wachtwoord in en de  dient minimaal 6 karakters lang te zijn
                            @endif
                        </div>
                
                                        
                        <input type="submit" class="button button-submit full-width radius" value="Uw wachtwoord opnieuw instellen">
                            {{ Form::token() }}
                
                    </form>
                    </div></div>
			</div>
	</div>
@stop
