@extends('layouts.main')

@section('page')
	<div class="page">
    	<div class="row">
				<div class="medium-10 medium-offset-1 columns">
					<h1>Over ons</h1>
				</div>
		</div>
		<!--<div class="hero">
			<div class="row">
				<div class="medium-10 medium-offset-1 columns">
					<h1>Over ons</h1>
				</div>
			</div>
		</div>-->
		<div class="row padding">
			<div class="medium-10 medium-offset-1 columns well">
                <center><img src="/img/logo.png" alt="CouponTrade logo" height="200px"></center>
                <br>
                <h2>Over ons</h2>
                <p>
                    Wij van CouponTrade bestaan uit een team van ervaren Marketeers en ICT-specialisten op het gebied van e-commerce solutions. Op basis van eigen ervaringen kwamen wij tot de conclusie dat er niets zo vervelend is als een ongebruikte coupon. Coupons worden gekocht of gewonnen met de intentie om te genieten van een leuk arrangement. Helaas komt het weleens voor dat je tot de verloopdatum van de coupon niet in de gelegenheid komt om je coupon te verzilveren. CouponTrade biedt daarom de mogelijkheid om jouw ongebruikte coupons online te verkopen. 
                </p>
                <p>
                    Vragen of opmerkingen? Mail je vraag naar <a href="mailto:info@coupontrade.nl">info@coupontrade.nl</a> en onze collega’s helpen je zo snel mogelijk verder.
                </p>
                <br>
                <h2>Integriteit</h2>
                <p>
                    Onze producten moet voldoen aan strenge voorwaardes en worden hierop gecontroleerd en gekeurd. Transparantie en vertrouwen zijn kernwaarden binnen ons bedrijf. Daarom is het gebruiken van een account verplicht om een aankoop en/of verkoop te doen. Tevens worden gebruiksgegevens niet aan derden verkocht en wordt je privacy gerespecteerd. 
                </p>
			</div>
		</div>
        {{-- <div class="row">
            <div class="medium-10 medium-offset-1">
                <div class="info-box">
                    <b>Help!</b>
                    <br>
                    Voor vragen en/of opmerkingen kun je altijd mailen naar <a href="mailto:info@coupontrade.nl">info@coupontrade.nl</a>.
                </div>
            </div>
        </div> --}}
	</div>
@stop