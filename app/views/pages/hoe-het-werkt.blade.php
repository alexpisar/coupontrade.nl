@extends('layouts.main')

@section('page')
	<div class="page">
    	<div class="row">
				<div class="medium-10 medium-offset-1 columns">
					<h1>Hoe het werkt</h1>
				</div>
		</div>
		<!--<div class="hero">
			<div class="row">
				<div class="medium-10 medium-offset-1 columns">
					<h1>Hoe het werkt</h1>
				</div>
			</div>
		</div>-->
		<div class="row padding">
			<div class="medium-10 medium-offset-1 columns well">
                <div class="video-container">
                    <iframe width="1280" height="720" src="//www.youtube.com/embed/8rOgl2VBoUo?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
                </div>
                <br>
                <h1>Algemene uitleg</h1>
                <p>
                    CouponTrade maakt het mogelijk om digitale hotelbonnen op een veilige, makkelijke en snelle wijze te kopen en verkopen. Wij maken dit mogelijk door de kopers snel en veilig te laten betalen met iDEAL. Na ontvangst van betaling verstuurt Coupontrade per mail de gekochte hotelbon naar de koper. Daaropvolgend ontvangt de verkoper zijn geld binnen 1-2 werkdagen (afhankelijk van de bank). 
                </p>
                <br>
                <h1>Uitleg voor verkopers</h1>
                <p>
                    CouponTrade zorgt ervoor dat de verkoper bij de verkoop altijd zijn geld ontvangt. Het gebruik van het betaalsysteem iDEAL zorgt ervoor dat de koper alleen een hotelbon ontvangt wanneer de betaling succesvol is uitgevoerd. CouponTrade maakt het mogelijk om je hotelbon op een veilige, snelle en gemakkelijke manier te verkopen.
                </p>
                <br>
                <h2>Hoe werkt CouponTrade?</h2>
                <p>
                    Om je hotelbon te verkopen moet je de onderstaande stappen doorlopen:
                    <ul>
                        <li>Het uploaden van jouw hotelbon als .pdf bestand*</li>
                        <li>Het bepalen van de verkoopprijs en het invullen van verplichte informatie</li>
                        <li>Het kiezen van een promotiemethode</li>
                    </ul>
                    Bij de verkoop van je hotelbon ontvang je direct een e-mailnotificatie. Je ontvangt het bedrag binnen 1-2 werkdagen op je bankrekening(afhankelijk van de bank waarbij je bent aangesloten). 
                </p>
                <br>
                <h2>Voordelen</h2>
                <p>
                    Het verkopen van je hotelbon via CouponTrade brengt de volgende voordelen met zich mee:
                    <ul>
                        <li>Garantie: je ontvangt altijd je geld</li>
                        <li>Simpel: je zet je advertentie online in enkele simpele stappen</li>
                        <li>Snel: wij bieden ondersteuning bij het promoten van je advertentie</li>
                    </ul>
                </p>
                <br>
                {{-- <h2>Geldgarantie</h2>
                <p>
                    CouponTrade weet hoe belangrijk het voor de verkoper is om zijn geld te onvangen. Daarom garandeert CouponTrade dat de verkoper altijd zijn geld ontvangt.
                </p>
                <br>
                <h2>Promotie van je advertentie</h2>
                <p>
                    De verkoper krijgt de optie om te bepalen op welke plaatsen de advertentie geplaatst wordt. Een voorbeeld hiervan is de advertentie automatisch laten plaatsen op je eigen Facebook.
                </p>
                <br> --}}
                <h2>Met weinig inspanning</h2>
                <p>
                    CouponTrade zorgt ervoor dat hotelbonnen met minimale inspanning gekocht en verkocht kunnen worden. De verkoper upload het PDF bestand met de bijbehorende informatie. De opgegeven informatie zorgt ervoor dat de koper alleen maar de betaling hoeft uit te voeren. Na een verkoop ontvang je als verkoper meteen een emailnotificatie waarin vermeld word dat de verkoop succesvol uitgevoerd is. CouponTrade maakt het bedrag binnen 1 a 2 werkdagen over naar de verkoper.
                </p>
                <br>
                <h2>Veilig</h2>
                <p>
                    Onze werkwijze maakt het verkopen en kopen van hotelbonnen op een veilige manier mogelijk. <br>
                    Wij controleren de bonnen met o.a. een geautomatiseerde barcode check om te voorkomen dat hotelbonnen niet dubbel worden verkocht. Daarnaast controleren wij alle bankgegevens en of er meldingen zijn van internetoplichting. Om de kopers zekerheid te bieden krijgen zij inzicht in de onderstaande gegevens van de koper:
                    <ul>
                        <li>Naam</li>
                        <li>Woonplaats</li>
                        <li>Profiel foto</li>
                    </ul>
                </p>
                <br>
                <h1>Wat zijn de kosten?</h1>
                <p>
                    Voor het plaatsen van een advertentie betaal je niks! CouponTrade brengt pas kosten in rekening wanneer het gelukt is om je hotelbon te kopen of verkopen. Het kost je dan een klein percentage van het verkoopprijs: 15%. Als het gaat om een hotelbon van &euro;20, ontvangt de verkoper &euro;17. 
                </p>
                <h1>Uitleg voor kopers</h1>
                <p>
                    De strenge voorwaarden van CouponTrade zorgen ervoor dat het uitvoeren van een aankoop op CouponTrade veilig is. Wij bieden een platform waarop de koper op een veilige, snelle en gemakkelijke manier een hotelbon kan kopen. Deze veiligheid proberen wij te waarborgen door het onderstaande mogelijk te maken:
                    <ul>
                        <li>Bij CouponTrade is het verplicht om als koper en verkoper in te loggen met Facebook of je te registreren met je e-mail adres, met als doel dat je weet wie de verkoper is en wij van CouponTrade weten bij wie jij een aankoop doet.</li>
                        <li>Op CouponTrade ben je verplicht een profielfoto te plaatsen. </li>
                        <li>De geldigheid en authenticiteit van hotelbonnen die op CouponTrade worden geplaatst, worden door een automatische scan gecontroleerd.</li>
                        <li>De bankgegevens van de verkoper wordt telkens gecontroleerd op mijnpolitie.nl om oplichters/fraudeurs te herkennen.</li>
                        <li>CouponTrade neemt altijd direct contact op met beide partijen wanneer een koper een klacht indient m.b.t. de verkoper van een hotelbon. Indien, deze negatieve klacht klopt zal de verkoper geblokkeerd worden en verwijderd worden van het platform.</li>
                        <li>CouponTrade beschikt over de gegevens(naam, bankrekening, telefoonnummer, email, etc.) van de verkoper. Bij het constateren van fraude zullen wij deze informatie aan de betrokken autoriteit verstrekken.</li>
                    </ul>
                </p>
                <br>
                <h2>Simpel</h2>
                <p>
                    Een hotelbon kopen op CouponTrade is niet moeilijk! Als koper log je in met je e-mail adres of Facebook account daarna reken je af met iDEAL en ontvang je de hotelbon. Deze werkwijze zorgt ervoor dat het aangeboden hotelbon 24/7 gekocht kan worden zonder contact te maken met de verkoper. 
                </p>
                <br>
                <h1>Wat zijn de kosten?</h1>
                <p>
                    CouponTrade brengt geen extra kosten in rekening voor het kopen van een hotelbon. 
                </p>
			</div>
		</div>
		<div class="row">
			<div class="medium-10 medium-offset-1">
				<div class="info-box">
					<b>Help!</b>
					<br>
					Voor vragen en/of opmerkingen kun je altijd mailen naar <a href="mailto:info@coupontrade.nl">info@coupontrade.nl</a>.
				</div>
			</div>
		</div>
	</div>
@stop