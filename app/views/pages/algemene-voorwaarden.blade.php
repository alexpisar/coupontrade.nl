@extends('layouts.main')

@section('page')
	<div class="page">
    	<div class="row">
				<div class="medium-10 medium-offset-1 columns">
					<h1>Algemene voorwaarden</h1>
				</div>
		</div>
		<!--<div class="hero">
			<div class="row">
				<div class="medium-10 medium-offset-1 columns">
					<h1>Algemene voorwaarden</h1>
				</div>
			</div>
		</div>-->
		<div class="row padding">
			<div class="medium-10 medium-offset-1 columns well">
				<p>
					Deze algemene voorwaarden zijn opgesteld door Coupontrade ("Coupontrade"), gevestigd op Nienke van Hichtumstraat 21, 1064 MH te Amsterdam en ingeschreven in de Kamer van Koophandel onder nummer nog invullen. Deze algemene voorwaarden zijn van toepassing op alle diensten die Coupontrade aanbiedt en op elke tot stand gekomen overeenkomst tussen Coupontrade en jou. Coupontrade behoudt zich het recht voor deze algemene voorwaarden te wijzigen. Door gebruik te maken van de diensten van Coupontrade stem je in met de meest recente versie van deze algemene voorwaarden.
				</p>

				<h2>Dienst</h2>
				<p>
					CouponTrade biedt via haar digitale online platform (het "Platform") een dienst aan waarmee je coupons voor hotels kunt kopen en verkopen (de "Dienst"). 
				</p>

				<h2>Registratie en Privacy</h2>
				<p>
					Je kunt van de Dienst gebruik maken door je te registreren met een Facebook account of met een bestaande e-mail adres. Je dient alle persoonlijke gegevens naar waarheid in te vullen. CouponTrade beschermt je persoonlijke gegevens in overeenstemming met de Wet bescherming persoonsgegevens. CouponTrade verwerkt de door jou verstrekte persoonsgegevens enkel om het gebruik van de Dienst mogelijk te maken en in dit verband met je te communiceren. Voor meer informatie: zie de privacy policy op onze website. 
				</p>
				<h2>Verkopen van coupons</h2>
				<h3>Uploaden van coupons</h3>
				<p>
					Je kunt als "Verkoper" een coupon te koop aanbieden op het Platform door een PDF bestand van de coupon te uploaden. Het bestand moet de originele digitale versie zijn dat verstrekt is door de provider. De verkoper moet gerechtigd zijn de coupon te verkopen en gescande coupons, foto's van coupons of aangepaste bestanden zijn niet toegestaan. 
				</p>
				<p> 
				   De Verkoper kan, zolang zijn aanbod niet is geaccepteerd, zijn aanbod tot verkoop van geüploade coupon te allen tijde intrekken. Ter preventie van twijfel: zodra het aanbod is aanvaard, komt er een finale overeenkomst tot stand en kan de Verkoper zijn aanbod dus niet meer intrekken. </p>				
				<h3>Beschrijving coupon</h3>
				<p>
					De Verkoper vult de gegevens over de coupon zo volledig mogelijk en naar waarheid in. Die gegevens omvatten in elk geval de naam van het evenement, de prijs (exclusief servicekosten) en het persoonlijke bankrekeningnummer van de Verkoper. Het bankrekeningnummer dient van een Nederlandse bank te zijn. Als het vereist is om als koper op de hoogte te zijn van specifieke informatie moet dit duidelijk in het veld "omschrijving" worden vermeld. CouponTrade behoudt zich het recht voor om de informatie over een aangeboden coupon aan te passen aan de informatie die op het geüploade coupon staat vermeld. 
				</p>
				<h3>Kosten en Prijs</h3>
				<p>
					De Verkoper bepaalt zelf de verkoopprijs van ieder coupon dat hij op het Platform aanbiedt. De Verkoper is voor ieder coupon waar een koopovereenkomst voor tot stand komt, geen bedrag schuldig aan CouponTrade. De BTW over de kaarten die de Verkoper via CouponTrade verkoopt moet al betaald zijn over het originele coupon voordat de Verkoper de coupon aanbiedt. CouponTrade rekent BTW over de transactiekosten voor de Verkoper. 
				</p>
				<h3>Verkochte coupons</h3>
				<p>
					De Verkoper ontvangt bij verkoop een e-mail met een bevestiging, waarna het verkochte ticket automatisch van het Platform wordt verwijderd. De Verkoper dient te controleren of de coupon als verkocht staat aangegeven op de website van CouponTrade. De Verkoper is verplicht alle duplicaten van het verkochte ticket te vernietigen. 
				</p>

				<h2>Kopen van coupons</h2>
				<h3>Risico's</h3>
				<p>
					Het kopen van een coupon via CouponTrade is niet risico vrij. CouponTrade spant zich in om enkel veilige en succesvolle transacties via het Platform af te wikkelen, maar zij garandeert niet dat een coupon dat via het Platform is aangeschaft altijd toegang zal geven tot het gewenste evenement. 
				</p>
				<h3>Kosten, betaling en levering</h3>
				<p>
					De koper van een of meerdere coupons kan online betalen via de aangeboden betaalmethodes. De transactiekosten voor de aankoop van een coupon bedragen 15% van de verkoopprijs en zijn inclusief de kosten van de betaling en BTW. Voor een aantal betaalmethodes wordt extra kosten in rekening gebracht. Zo spoedig mogelijk na betaling levert CouponTrade per email de coupon in de vorm van een PDF bestand.
				</p>
				<h3>Wijzigingen van het evenement</h3>
				<p>
					Zodra de coupon is verkocht via CouponTrade kan de organisator van het arrangement de bezitter van de coupon niet benaderen. De koper dient te allen tijde de informatie rondom een arrangement in de gaten te houden en zelf op te zoeken. Coupontrade is niet aansprakelijk voor wijzigingen van het arrangement. Als het arrangement wijzigt dient de koper zelf vergoedingen te verhalen op de organisator van het arrangement (de oorspronkelijke prijs van de coupon exclusief servicekosten). 
				</p>

				<h2>Ongeldige coupons</h2>
				<p>
					De risico's m.b.t. geldigheid van de coupon en de eventuele kosten daarvoor zijn voor de Verkoper en de Koper. Een coupon nogmaals verkopen of gebruiken is niet toegestaan. Als de coupon niet geldig blijkt, dan zal CouponTrade de Verkoper en Koper via een chat discussie met elkaar in contact brengen om in goed overleg een oplossing te zoeken. 
				</p>

				<h2>Aansprakelijkheid</h2>
				<p>
					CouponTrade is niet aansprakelijk voor enige schade die je als gevolg van het gebruik van de Dienst of het Platform mocht lijden, tenzij die schade is ontstaan door opzet, grove nalatigheid of bewuste roekeloosheid van CouponTrade. Hoewel CouponTrade zoveel mogelijk maatregelen treft om te zorgen dat de verkoop zo veilig mogelijk verloopt, garanderen wij op geen enkele wijze de juistheid van de Dienst. CouponTrade is niet aansprakelijk voor de gevolgen van (eventuele) onjuistheden met betrekking tot de Dienst. 
				</p>

				<h2>Misbruik</h2>
				<p>
					Als voorwaarde voor het gebruik van de Dienst, ga je ermee akkoord geen informatie, gegevens of inhoud via CouponTrade aan te bieden die inbreuk maken op enige wet- of regelgeving. Bovendien ga je ermee akkoord dat je: A. geen onjuiste verwantschap pretendeert met een persoon of entiteit; B. geen niet-openbare/beveiligde gebieden van de Dienst betreedt; C. geen virussen, wormen, junk mail, spam, kettingbrieven, ongevraagde aanbiedingen of advertenties van welke aard dan ook en voor welk doel dan ook stuurt; D. niet de kwetsbaarheid van de Dienst of een ander gerelateerd systeem of netwerk onderzoekt, scant of test, of in strijd handelt met enige beveiliging of authenticiteit. Als CouponTrade vermoedt dat je niet voldoet aan deze algemene voorwaarden, dan kan zij je toegang tot het Platform en de Dienst met onmiddellijke ingang (blijvend) ontzeggen. 
				</p>

				<h2>Vrijgeven van informatie</h2>
				<p>
					CouponTrade heeft te allen tijde het recht om informatie openbaar te maken (i) om zichzelf te verdedigen in rechtszaken, (ii) een gerechtelijk bevel te volgen, (iii) zich te houden aan alle wetten, voorschriften of verzoeken van de overheid, (iv) om de nationale veiligheid, defensie, de openbare veiligheid en de volksgezondheid te beschermen en (v) de algemene voorwaarden te handhaven. 
				</p>

				<h2>Klachtenregeling</h2>
				<p>
					Als je een klacht hebt over het Platform of de Dienst, dan hoort CouponTrade dat graag. Stuur je klacht (zo volledig mogelijk omschreven) naar <a href="mailto:info@coupontrade.nl">info@coupontrade.nl</a>. 
				</p>

				<h2>Geschillen</h2>
				<p>
					Op deze algemene voorwaarden is uitsluitend Nederlands recht van toepassing. Geschillen zullen worden voorgelegd aan de bevoegde rechter te Amsterdam.
				</p>
			</div>
		</div>
		<div class="row">
			<div class="medium-10 medium-offset-1">
				<div class="info-box">
					<b>Help!</b>
					<br>
					Voor vragen en/of opmerkingen kun je altijd mailen naar <a href="mailto:info@coupontrade.nl">info@coupontrade.nl</a>.
				</div>
			</div>
		</div>
	</div>
@stop