@extends('layouts.main')
     	<link rel="stylesheet" href="/stylesheets/style1.css"> <link rel="stylesheet" type="text/css" href="/source/jquery.fancybox.css?v=2.1.5" media="screen" />
        <link rel="stylesheet" href="/stylesheets/jquery-ui.css">
  		<link rel="stylesheet" href="/stylesheets/style1.css">
		<style type="text/css">
            .fancybox-custom .fancybox-skin {
                box-shadow: 0 0 50px #222;
            }
			
        </style>
@section('page')
	<div class="page">
     
    	<div class="row">
				<div class="medium-10 medium-offset-1 columns text-center">
					<h1>Een advertentie plaatsen</h1>
				</div>
		</div>
        <br>
		<div class="row">
			<div class="medium-10 medium-offset-1">
				@if(Session::has('whoops') || Session::has('fouten'))
					<div data-alert class="alert-box alert radius">
					  	@if(Session::has('whoops'))
							{{Session::get('whoops')}}
					  	@endif
						@if(Session::has('fouten'))
						<?php $fouten = []; ?>
						@foreach(Session::get('fouten') as $error)
						<?php $fouten[] = $error; ?>
						@endforeach
						<?php $foten = array_unique($fouten) ?>
						@foreach($foten as $fout)
							{{$fout}}<br>
						@endforeach
						@endif
					  <a href="#" class="close">&times;</a>
					</div>
				@endif
			</div>
		</div>
        <div class="row">
			<div class="medium-10 medium-offset-1 well">
            <div class="summary" style="border: 1px solid #aaaaaa; border-radius: 4px; padding: 30px; color:grey"> 
            	<h5>Om je coupon, veiling of e-ticket te verkopen moet je de onderstaande korte stappen doorlopen: </h5> <br>
                <p>1. Upload je e-ticket, coupon of veiling. Kies een geldig PDF bestand die jij wenst te verkopen.</p>
                <p>2. Bewerk je PDF bestand: Elk PDF bestand heeft een unieke barcode die nodig is om een e-ticket,coupon of veiling in te wisselen. Bewerk deze in ons systeem om de veiligheid van je code te waarborgen.</p>
                <p>3. Verkoopinformatie: Vul de gewenste verkoopprijs in, verzorging en land of regio. Hierdoor wordt je e-ticket, coupon of veiling beter gevonden door potentiële kopers.</p> 
                <p>4. Aanvullende informatie: Mocht je nog aanvullende informatie hebben? Vul deze dan in zodat je potentiële kopers zo goed mogelijk informeert.</p> 
                <p>5. Upload een foto: Tip van Coupontrade! Een foto verhoogt de kans op verkoop van je e-ticket, coupon of veiling.</p> 
                6. Advertentie plaatsen: Heb je alle 6 de stappen doorlopen? Ga dan akkoord met onze voorwaarden en plaats je advertentie. Het verkopen kan beginnen!
            </div>
             <br/>
                {{Form::open(['url' => '/advertentie/plaatsen','files'=>true])}}
                   <div id="accordion">
                      <h3><a href="javascript:void(0)">1. Upload je e-ticket, coupon of veiling </a>
                            <p class="text">
                                Selecteer een geldig PDF bestand en upload deze. Na het uploaden verschijnt er een succesmelding.Je krijgt de mogelijkheid om een voorbeeldweergave te bekijken die laat zien hoe je je pdf bestand in stap 2 moet gaan bewerken. 

                            </p>
                      </h3>
                      <div id="panel1" data-index='1'>

                                            <input type="hidden" value="{{Session::get('ss_isRedirected')}}" name="isRedirected" id="isRedirected">
                                            {{Form::file('fileInput', ['class' => 'file-input', 'id' => 'fileInput'])}}
                                            <div class="file-upload-box" id="fileUploadBox" style="display:none;">
                                                <h1 class="ion-plus">
                                                    <small><br>Kies een geldig PDF bestand(selecteer een bestand van je computer)</small>
                                                </h1>
                                            </div>
                                            <div id="messageProgress" style="display:none">Bezig met uploaden...</div>
                                            <div class="progress" style="height:2px;">
                                                <span class="meter" style="height:2px;width:0%;" id="meter"></span>
                                            </div>
                                           {{--if multipages - will be write to checkboxes
                                            @if(is_array(Input::old('file')))
                                                <div id="m-form" style="display:block;">
                                                    U heeft al een .pdf bestand geuploadt
                                                    @foreach(Input::old('file') as $i => $file)
                                                        <input type="hidden" name="file[$i]" value="{{$file}}">
                                                    @endforeach
                                                    <br/>
                                                </div> --}}
                                            @if (Input::old('file') && !is_array(Input::old('file')))
                                                <div id="m-form" style="display:block;">
                                                    U heeft al een .pdf bestand geuploadt
                                                    <input type="hidden" name="file" value="{{Input::old('file')}}">
                                                </div>
                                                <br/>
                                            @else
                                            <div id="m-form"></div>
                                            <br/>
                                            @endif
                                            <input type="button" id="nxtEdit" disabled="disabled" class='nxtBtn' value="Volgende">

                                            <a href="javascript:void(0)" id="back" style="display:none;"> Opnieuw uploaden </a> 
                             @if(isset($notification) && $notification)
                                <div class="alert-box radius notification">{{$notification}}
                                </div>
                             @endif
											<hr>
                      </div>
                      <h3><a href="javascript:void(0)">2.  Bewerk je e-ticket, coupon of veiling</a>
                         <p class="text">Elk bestand heeft een unieke code die u natuurlijk niet wilt laten zien voor de verkoop. Als je op de onderstaande link klikt, kan je het PDF bestand bewerken. </p>
                      </h3>
                      <div id="panel2" data-index='2'>
                        {{--@if(Session::has('pdf') || Session::has('pdfmulti'))
                                        <div class="white-overlay" style="display:none;"></div>
                                        @else
                                        <div class="white-overlay"></div>
                                        @endif --}}
                                        
                                        <div id="example" style="display:none">
                                        <img id="exampleImg" src="/img/example.png"/>
                                        </div>
                                        
                                        <div id="canvasDiv" style="display:none; width:730px;">
                                            <canvas id="canvasSignature" width="590px" height="790px" style="cursor: url(/img/brush.cur), auto;"></canvas>
                                            
                                            <input type="hidden" name="hidVal" id="hidVal" value="{{Session::get('ss_hidVal')}}">
                                            <div id="buttons" style="float:right">
                                                <button id="btn"></button>
                                                <p id="preview">Bekijk voorbeeld</p>
                                                <button id="btn_unhappy">  </button>
                                                <p id="re-edit">Opnieuw Bewerken</p>
                                                <button id="staticBtn2" style="display:block"> </button>
                                                <p id="save" style="display:block">Bevestigen (Hierna krijg je een melding dat het bestand succesvol is bewerkt)</p>
                                            </div>
                                            <div style="clear:both"></div>
                                            <img id="image" src=""/>
                                        </div>
                                    	<div id="check-form">
                                         @if(Input::old("value_checkbox"))
                                            <?php 
                                                $checkedFiles = Input::old("file", []);
                                                $valueCheckboxes = Input::old("value_checkbox", []);
                                                $labelCheckboxes = Input::old("label_checkbox", []);
                                            ?>
                                            @foreach($valueCheckboxes as $i => $fileCheckbox)
                                             {{Form::hidden("value_checkbox[$i]", $fileCheckbox)}}
                                              {{Form::hidden("label_checkbox[$i]", $labelCheckboxes[$i])}}
                                              <label class="checkbox-inline">
                                              {{Form::checkbox("file[$i]", $fileCheckbox, in_array($fileCheckbox, $checkedFiles), ["class"=>"inlineCheckbox"])}}
                                              {{$labelCheckboxes[$i]}}</label>
                                            @endforeach
                                         @endif
                                        </div>
                                         {{Form::hidden("pdfmultiImg", null, ["id" => "pdfmultiImg"])}} 
                                         <input type="hidden" name="pdfImg" id="pdfImg" value="{{Session::get('ss_pdfImg')}}">

                                        <a id="secondstep" href="#">Klik op deze link om je PDF bestand te bewerken. </a>			
                                        <a class="fancybox" id="editimage" href="#canvasDiv"></a>				
                                        <br/><br/>
                                        <input type="button" class='prevBtn' value=" Vorige "> 
                                        <?php
                                            if(isset($checkedFiles) && count($checkedFiles) < 1 
                                                || !isset($valueheckboxes) && !Session::get('ss_pdfImg'))
                                                $disabledNxtTag = 'disabled="disabled"';
                                            else
                                                $disabledNxtTag = '';
                                          ?>
                                        <input type="button"  id="nxtTags" {{$disabledNxtTag}} class='nxtBtn' value=" Volgende" > 
                             @if(isset($notification) && $notification)
                                <div class="alert-box radius notification">{{$notification}}
                                </div>
                             @endif
                                        <hr />
                      </div>
                      <h3><a href="javascript:void(0)">3. Verkoopinformatie(advertentietitel, geldigheid, type advertentie en prijs) </a>
                         <p class="text">Alle velden zijn verplicht om in te vullen. Mocht er een veld niet van toepassing zijn vul dan ‘overig’ in.</p>
                     </h3>
                      <div id="panel3" data-index='3'>
                   			 <div class="row">
                        <div class="medium-3 columns">
                            <label class="left inline">Categorie</label>
                        </div>
                        <div class="medium-9 columns">
                            {{Form::select('categorie', ['' => 'Kies een categorie', 'vakanties' => 'Vakanties', 'evenementen' => 'Evenementen', 'restaurants' => 'Restaurants', 'hotels' => 'Hotels', 'avondje-uit' => 'Avondje uit', 'dagje-weg' => 'Dagje weg', 'sport' => 'Sport', 'overig' => 'Overig'],null, ['id' => 'categorie'])}}
                        </div>
                      </div>  
                                        <div class="row">
                                            <div class="medium-3 columns">
                                                <label class="left inline">Geldigheid</label>
                                            </div>
                                            <div class="medium-9 columns">
                                                <div class="left" style="margin-right:25px;"><label>Vanaf (dd-mm-jjjj): {{Form::text('geldig-vanaf' ,'', array('id' =>'geldig-vanaf'))}}</label></div>
                                                <div class="left"><label>Tot (dd-mm-jjjj): {{Form::text('geldig-tot', '', array('id' => 'geldig-tot'))}}</label></div>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="medium-3 columns">
                                                <label>Prijs en waarde</label>
                                            </div>
                                            <div class="medium-9 columns">
                                                    <div class="left">
                                                        <label>Verkoopprijs</label>
                                                        <h1><span>&euro;</span> {{ Form::number('prijs', '', ['placeholder' => '0', 'min' => '0', 'id' => 'prijs']) }}</h1>
                                                    </div>
                                                    <div class="left mlt">
                                                        <label>Ter waarde van</label>
                                                        <h1><span>&euro;</span> {{ Form::number('twv', '', ['placeholder' => '0', 'min' => '0', 'id' => 'twv']) }}</h1>
                                                    </div>
                                                </div>
                                        </div>
                                        <div class="row">
                                            <div class="medium-3 columns">
                                                <label class="left inline">Type advertentie</label>
                                            </div>
                                            <div class="medium-9 columns">
                                                {{Form::select('type', ['' => 'Kies het type', '1' => 'Coupon', '2' => 'E-ticket', '3' => 'Veiling'],null, ['id' => 'type'])}}
                                            </div>
                                        </div>
                                        <br><br>
                                        <div class="row">
                                            <div class="medium-3 columns">
                                                <label class="left inline">Advertentie titel</label>
                                            </div>
                                            <div class="medium-9 columns">
                                                {{Form::text('titel', '', array('id' => 'titel', "maxlength"=> "55"))}}
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="medium-3 columns">
                                                <label class="left inline">Land/Regio</label>
                                            </div>
                                            <div class="medium-9 columns">
                                                {{Form::select('country', ['' => 'Kies','Noord-Holland' => 'Noord-Holland', 'Zuid-Holland' => 'Zuid-Holland', 'Utrecht' => 'Utrecht', 'Noord-Brabant' => 'Noord-Brabant', 'Gelderland' => 'Gelderland', 'Zeeland' => 'Zeeland', 'Overijssel' => 'Overijssel',
                    'Flevoland' => 'Flevoland', 'Drenthe' => 'Drenthe', 'Friesland' => 'Friesland','Groningen' => 'Groningen', 'Limburg' => 'Limburg',
                    'Nederland' => 'Nederland','België' => 'België','Duitsland' => 'Duitsland','Frankrijk' => 'Frankrijk','Spanje' => 'Spanje','Overige landen' => 'Overige landen' ],null, ['id' => 'country'])}}
                                            </div>
                                        </div>
                                        <br><br>
                                        <div class="row">
                                            <div class="medium-3 columns">
                                                <label class="left inline">Verblijfsduur</label>
                                            </div>
                                            <div class="medium-9 columns">
                                                <div class="left">
                                                      <div class="fldsInput">
                                                        {{ Form::number('duration', '', ['placeholder' => '1', 'min' => '1', 'max' => '30', 'id' => 'duration']) }}
                                                      </div>
                                                      <div class="textInput">dagen</div>
                                                      <div class="clearBoth"></div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="row">
                                            <div class="medium-3 columns">
                                                <label class="left inline">Aantal sterren</label>
                                            </div>
                                            <div class="medium-9 columns">
                                                {{Form::select('rating', ['' => 'Kies','2sterren' => '2 sterren', '3sterren' => '3 sterren', '4sterren' => '4 sterren', '5sterren' => '5 sterren', 'overig' => 'Overig'],null, ['id' => 'rating'])}}
                                            </div>
                                        </div>
                                        <br><br>
                                        <div class="row">
                                            <div class="medium-3 columns">
                                                <label class="left inline">Verzorging</label>
                                            </div>
                                            <div class="medium-9 columns">
                                                {{ Form::checkbox('arrangement[]','Logies',null, ['class' => 'arrangement']) }}&nbsp;&nbsp;<span>Logies</span>&nbsp;&nbsp;
                                                {{ Form::checkbox('arrangement[]','Logies en ontbijt',null, ['class' => 'arrangement']) }}&nbsp;&nbsp;<span>Logies en ontbijt</span>&nbsp;&nbsp;
                                                {{ Form::checkbox('arrangement[]','Half pension',null, ['class' => 'arrangement']) }}&nbsp;&nbsp;<span>Half pension</span>&nbsp;&nbsp;<br />
                                                {{ Form::checkbox('arrangement[]','Hotelarrangement',null, ['class' => 'arrangement']) }}&nbsp;&nbsp;<span>Hotelarrangement</span>&nbsp;&nbsp;
                                                {{ Form::checkbox('arrangement[]','Overig',null, ['class' => 'arrangement']) }}&nbsp;&nbsp;<span>Overig</span>
                                            </div>
                                        </div>
                    
                                        <br>
                                        <div class="row">
                                            <div class="medium-3 columns">
                                                <label class="left inline">Faciliteiten</label>
                                            </div>
                                            <div class="medium-9 columns">
                                                {{ Form::checkbox('facilities[]','Zwembad',null, ['class' => 'facilities']) }}&nbsp;&nbsp;<span>Zwembad</span>&nbsp;&nbsp;
                                                {{ Form::checkbox('facilities[]','Sauna',null, ['class' => 'facilities']) }}&nbsp;&nbsp;<span>Sauna</span>&nbsp;&nbsp;
                                                {{ Form::checkbox('facilities[]','Wifi',null, ['class' => 'facilities']) }}&nbsp;&nbsp;<span>Wifi</span>&nbsp;&nbsp;
                                                {{ Form::checkbox('facilities[]','Wellness',null, ['class' => 'facilities']) }}&nbsp;&nbsp;<span>Wellness</span>&nbsp;&nbsp;
                                                <br />
                                                {{ Form::checkbox('facilities[]','Parkeerplaats',null, ['class' => 'facilities']) }}&nbsp;&nbsp;<span>Parkeerplaats</span>&nbsp;&nbsp;
                                                {{ Form::checkbox('facilities[]','Restaurant',null, ['class' => 'facilities']) }}&nbsp;&nbsp;<span>Restaurant</span>
                                                {{ Form::checkbox('facilities[]','Rolstoeltoegankelijk',null, ['class' => 'facilities']) }}&nbsp;&nbsp;<span>Rolstoeltoegankelijk</span>&nbsp;&nbsp;
                                                <br />
                                                {{ Form::checkbox('facilities[]','Overig',null, ['class' => 'facilities']) }}&nbsp;&nbsp;<span>Overig</span>
                                            </div>
                                        </div>
                                        <br>
                                        
                                        <div class="row">
                                            <div class="medium-3 columns">
                                                <label class="left inline">Accomodatie</label>
                                            </div>
                                            <div class="medium-9 columns">
                                                {{Form::select('accomodation', ['' => 'Kies','hotel' => 'Hotel', 'bedenbreakfast' => 'Bed en breakfast', 'suite' => 'Suite', 'pension' => 'Pension','herberg' => 'Herberg', 'overig' => 'Overig'],null, ['id' => 'accomodation'])}}
                                                
                                            </div>
                                        </div>
                                        <input type="button" class='prevBtn' value=" Vorige ">
                                        <input type="button" id="nxtInfo" disabled="disabled" class='nxtBtn' value=" Volgende"> 

                             @if(isset($notification) && $notification)
                                <div class="alert-box radius notification">{{$notification}}
                                </div>
                             @endif
                                        <hr>
                       </div>                 
                      <h3><a href="javascript:void(0)">4. Aanvullende informatie</a> 
                         <p class="text">Hier kan je aanvullende informatie toevoegen die wellicht niet te vinden is op je e-ticket, coupon of veiling.</p>
                      </h3>
                      <div id="panel4" data-index='4'>
                       <div class="row">
                                            <div class="medium-3 columns">
                                                <label class="left inline">Omschrijving</label>
                                            </div>
                                            <div class="medium-9 columns">
                                                {{Form::textarea('omschrijving', '', ['style' => 'resize:auto;height:250px;', 'id' => 'omschrijvingVal'])}}
                                                {{Form::textarea('omschrijvingMarkdown', '', ['id' => 'omschrijvingInput', 'style' => 'display:none;'])}}
                    
                                                <br><br>
                                            </div>
                                        </div>
                       			<div class="row">
                                            <div class="medium-3 columns">
                                                <label class="left inline">Voorwaarden</label>
                                            </div>
                                            <div class="medium-9 columns">
                                                {{Form::textarea('voorwaarden', '', ['style' => 'resize:auto;height:250px;', 'id' => 'voorwaardenVal'])}}
                                                {{Form::textarea('voorwaardenMarkdown', '', ['id' => 'voorwaardenInput', 'style' => 'display:none;'])}}
                    
                                                <br><br>
                                            </div>
                                        </div>
                                <input type="button" class='prevBtn' value=" Vorige ">
                                <input type="button" id="nxtUpload" class='nxtBtn' value=" Volgende"> 
                       <hr>
                      </div>
                      <h3><a href="javascript:void(0)">5. Foto uploaden</a>
                       <p class="text"> *Tip: Wij raden je aan om een aantrekkelijke foto te uploaden zodat potentiele kopers sneller interesse kunnen hebben in de advertentie. </p>
                      </h3>
                      <div id="panel5" data-index='5'>
                          <div class="row">
                                                <div class="medium-3 columns">
                                                    <label class="left inline">Image upload</label>
                                                </div>
                                                <div class="medium-9 columns">
                                                    {{ Form::file('avatar', array('id'=>'img_upload')) }}
                                                    <input type="hidden" id="hidImg" name="avatar" value="">
                                                </div>
                                            </div>
                               <input type="button" class='prevBtn' value=" Vorige ">             
                               <input type="button" id="nxtAd" class='nxtBtn' value="Volgende" > 
                           <hr>
                       </div>
                      <h3>
                      	 <a href="javascript:void(0)">6. Advertentie plaatsen </a>
                          <p class="text"> Heb je alle 6 de stappen doorlopen? Ga dan akkoord met onze voorwaarden en plaats je advertentie. Het verkopen kan beginnen!</p>
                      </h3>
                      <div id="panel6" data-index='6'>
                        <br />
                        <div class="confirmBox">
                            <input type="checkbox" value="1" id="confirmBox" name="confirmBox" style="visibility:hidden;" />
                            <label for="confirmBox" style="margin-left:0px;"></label>
                        </div> 
                        <p style="line-height:1; margin-left:30px;">Ik ga akkoord met de <a id="terms_conditions" href="javascript:void(0);" style="text-decoration:underline">algemene voorwaarden</a> en verklaar dat mijn ticket een geldig toegangsbewijs is.</p> 
                        <div class="confirmBox">
                            <input type="checkbox" value="1" id="confirmBox1" name="confirmBox1" style="visibility:hidden;" />
                            <label for="confirmBox1" style="margin-left:0px;"></label>
						</div> <p style="line-height:1; margin-left:30px;">
                        Ik ga ermee akkoord dat mijn ticket ieder moment, en zonder tussenkomst van mij, verkocht kan worden via Coupontrade. Als ik het kaartje toch zelf wil gebruiken, verwijder ik deze eerst van Coupontrade</p>
                      	<br><br>
                        
                        <input type="button" class='prevBtn' value=" Vorige ">
                        {{Form::submit('Advertentie plaatsen', ['class' => 'button button-submit radius', 'id' => 'submitButton', 'disabled' => 'disabled'])}} 
                     @if(isset($notification) && $notification)
                        <div class="alert-box radius notification">{{$notification}}
                        </div>
                     @endif                      
                      </div>
                  </div>
               {{Form::close()}}
        	</div>
        </div>
        <div class="spacer"></div>
	</div>
@stop

@section('scripts')
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
 	<script src="/js/jquery-ui.js"></script>
	<script type="text/javascript" src="/lib/jquery.mousewheel-3.0.6.pack.js"></script>
	<script type="text/javascript" src="/source/jquery.fancybox.js?v=2.1.5"></script>
	<script src="/js/showdown.js"></script>
	<script>
		$(window).load(function(){
			$("#accordion > div").height("auto");
			
		});
		$(document).ready(function()
		{
             var editedFiles = []; //pdfImg - edited files
            var activeIndex = 0;
            var redirect = $("#isRedirected").val();
            if(redirect == "success"){
                activeIndex = 1;
            }            
			$("#accordion").accordion({ disabled: true, active: activeIndex });

			$(".nxtBtn").click(function(){
				var flag = $(this).closest('div').data('index');
				var count = flag + 1 ;
				$('#accordion').accordion('option', 'active', $("#panel"+count).index("#accordion > div"));
				
			});
			$(".prevBtn").click(function(){
				
				var flag = $(this).closest('div').data('index');
				var count = flag - 1 ;
				$( "#accordion" ).accordion('option', 'active', $("#panel"+count).index("#accordion > div"));
				
			});
			//already open panel 3
			// $("#alreadyUpload").click(function(e) {
   //              $( "#accordion" ).accordion('option', 'active', $("#panel3").index("#accordion > div"));
   //          });
			
			validate();
			$('#categorie, #geldig-vanaf, #geldig-tot, #prijs, #twv, #type, #titel, #country,  #duration, #rating, .arrangement, .facilities,  #accomodation').change(validate);
			
			$('.fancybox').fancybox({
			  
			  helpers: {
				overlay: {
				  locked: false
				}
			  }
			});
			$('#fileUploadBox').click(function()
			{
				$('#fileInput').click();
			});


            function hideProgress () {
                    $('#fileInput').prop('disabled', true);
                    setTimeout(function() {
                        $('.progress').fadeOut();
                        $('#m-form').fadeIn();
                        $('#check-form').fadeIn();
                        $('#messageProgress').hide();
                    }, 750);
            }
            function showSuccess () {
                $('#m-form').append('De pdf die u heeft geselecteerd is succesvol geupload.');
                $('#m-form').append('<br><a class="fancybox" href="#example" style="color:#3498db">Klik op het voorbeeld </a> om te zie hoe je PDF bestand eruit komt te zien nadat je jouw PDF bestand hebt bewerkt. De koper ziet je PDF op deze manier tot op het moment dat de betaling wordt uitgevoerd. Na betaling ontvangt de koper het PDF met unieke code weergegeven. Op deze manier kunnen wij een veilige transactie garanderen. ');
                $("#nxtEdit").prop("disabled",false);
                $("#back").show();
            }
            function showError () {
                $('#m-form').append('<span style="color:#FF0000">Het is alleen mogelijk om PDF bestanden te uploaden met de versie 1.4 of lager. </span> ');
                $("#back").show();
            }

            if(redirect){
                if(redirect == "success"){
                    hideProgress();
                    showSuccess();
                    editedFiles.push($("#pdfImg").val());
                }else if(redirect == "error"){
                    hideProgress();
                    showError();
                }
            }else{
                $('#fileUploadBox').show();
            }

		    $('#fileInput').change(function() {
				var ext = $(this).val().split('.').pop().toLowerCase();
				if (ext != 'pdf')
				{
					alert('Selecteer een .pdf bestand.');
					$('#fileInput').val("");
				}
				else
				{
					$('#fileUploadBox').fadeOut(1000);
					$('#messageProgress').show(1000);

			        $.fn.upload = function(remote,data,successFn,progressFn)
			        {
						if(typeof data != "object")
						{
							progressFn = successFn;
							successFn = data;
						}

						return this.each(function()
						{
							if($(this)[0].files[0])
							{
								var formData = new FormData();
								formData.append($(this).attr("name"), $(this)[0].files[0]);
								// if we have post data too
								if(typeof data == "object") {
									for(var i in data) {
										formData.append(i,data[i]);
									}
								}
								// do the ajax request
								$.ajax({
									url: remote,
									type: 'POST',
									xhr: function() {
										myXhr = $.ajaxSettings.xhr();

										if(myXhr.upload && progressFn)
										{
											myXhr.upload.addEventListener('progress',function(prog) {
												var value = ~~((prog.loaded / prog.total) * 100);
												if (progressFn && typeof progressFn == "function")
												{
													progressFn(prog,value);
												}
												else if (progressFn)
												{
													$(progressFn).val(value);
												}
											}, false);
										}
										return myXhr;
									},
									data: formData,
									dataType: "json",
									cache: false,
									contentType: false,
									processData: false,
									complete : function(res)
									{
										var json;
										try {
                                            json = JSON.parse(res.responseText);
                                        } catch(e) {
                                            json = res.responseText;
                                        }
                                        
                                        if(successFn) successFn(json);
                                    }
                                });
                            }
                        });
                    }



					$('#fileInput').upload('/pdfcheck',
						function(success) {
							if (success.length > 1) //multi
							{

                                hideProgress();
								$('#check-form').empty();
								$("#secondstep").prop('disabled',true);
								var array = [];
								$('#m-form').append('<a style="color:#3498db" class="fancybox" href="#example">Klik op het voorbeeld</a> om te zie hoe je PDF bestand eruit komt te zien nadat je jouw PDF bestand hebt bewerkt.De koper ziet je PDF op deze manier tot op het moment dat de betaling wordt uitgevoerd. Na betaling ontvangt de koper het PDF met unieke code weergegeven. Op deze manier kunnen wij een veilige transactie garanderen. <br>');
								$('#m-form').append('<br>');
								var cnt = 0;
								$.each(success, function(arrayID, group) {
									cnt++;
                                    //to avoid absolute paths
									var nogiets = group.split('/private/');
                                    var filePath = nogiets[1];
                                    nogiets = filePath.split('/');
									var image_pdf_multi = nogiets[4].substring(0,nogiets[4].indexOf(".pdf"));
									var multiImg = '/img/pdfImg/' + image_pdf_multi + '.png';
									array.push(multiImg);
									if (cnt < success.length) {
                                        var labelName = nogiets[4].split('.')[0];
                                      $('#check-form').append('<label class="checkbox-inline"><input type="checkbox" name="file[]" class="inlineCheckbox" value="' + group + '"> ' + labelName + '</label>');
									  $('#check-form').append('<input type="hidden" name="label_checkbox[]" value="'+ labelName +'"> <input type="hidden" name="value_checkbox[]" value="'+ group +'" >');
									} 
								});
								
								$("#pdfmultiImg").val(array);
								
								$('#m-form').append('<input type="hidden" name="file" id="file" value="' + success[2] + '"> ');
								$('#m-form').append('<input type="hidden" name="defaultImg" id="defaultImg"  value="' + success[0] + '">');
								
									$("#nxtEdit").prop("disabled",false);
									$("#back").show();
							}
							else
							{
                                var myString = '';
                                var path = '';

								if( success.error )
								{
									if( success.error.message !== undefined && success.error.message.indexOf("/private/") > 0 ){
										path = "/private/"+success.error.message.split("/private/")[1].split(".pdf")[0]+".pdf";
										var file = success.error.message.split(") probably")[0].split("/origineel/")[1];
										var filename = file.substring(0,file.indexOf(".pdf"));
										myString = '/img/pdfImg/' + filename + '.png';

										 $.ajax({
												  type: "POST",
												  url: "imgick",
												  data: {source: path, file:  file
												  }
												}).done(function( respond ) {
                                                    hideProgress();
                                                    showSuccess();
												}).fail(function(){
                                                    hideProgress();
                                                    showError();
 
                                                });
										$('#check-form').empty();
									}
                                    else //error is not fpdi lib.
                                    {
                                        hideProgress();
                                        showError();
                                        $('#check-form').empty();
                                    }
								}
								else if($.isArray(success) && success.length > 0
                                        && success[0].indexOf("/private/") > 0){
                                    hideProgress();
                                    showSuccess();
                                    $('#check-form').empty();

                                    myString = success[0].substr(success[0].indexOf("&") + 1);
                                    path = success[0].substring(0,success[0].indexOf("&"));

                                } else{ //another error
                                    hideProgress();
                                    showError();
									$('#check-form').empty();

								}

                                $("#hidVal").val(myString);
                               //  initialize();
                                $('#m-form').append('<input type="hidden" name="file" id="file" value="' + path + '"> ');

        						setTimeout(function() {
        							$('.white-overlay').fadeOut();
        						}, 1000);
                            }
						},
						function(prog, value) {
							$('#meter').css('width', value + '%');
						}
					);
		   	}
			
			
			});
			$('#m-form').on('click', '#singleCheckbox', function() {
				if ($('#singleCheckbox').prop("checked") == true) {
					$('[class=inlineCheckbox]').prop('checked', false);
					$('[class=inlineCheckbox]').prop('disabled', true);
					$('#defaultImg').prop('checked', false);
					$('#defaultImg').prop('disabled', true);
					$('.white-overlay').fadeOut();
				}
				else
				{
					$('[class=inlineCheckbox]').prop('disabled', false);
					$('.white-overlay').fadeIn();
				}
				
				var array = [];
								
				$('input[class*=inlineCheckbox]:not(:checked)').each(function() {
					var notChecked_value = $(this).val();
					var subFile = notChecked_value.split('/splits/');
					var imageSingleFile = subFile[1].substring(0,subFile[1].indexOf(".pdf"));
				    var singleImg = '/img/pdfImg/' + imageSingleFile + '.png';
					array.push(singleImg);
				});
				$('#hidVal').val(array);
			//	initialize();
				$("#nxtEdit").prop("disabled",false);
				$("#back").show();
				
			});
			$('#check-form').on('click', 'input[class*=inlineCheckbox]', function() {
				if ($(this).prop('checked') == false)
				{
					$('#secondstep').prop('disabled',true);	
					var sigCanvas = document.getElementById("canvasSignature");
				 	sigCanvas.width = sigCanvas.width;
				}
				else
				{
					//var selected = [];
					$('#secondstep').prop('disabled',false);
					$('input[class*=inlineCheckbox]:checked').each(function() {
						var checked_value = $(this).val();
						//selected.push($(this).val());
						var checkedFile = checked_value.split('/splits/');
						var imageFile = checkedFile[1].substring(0,checkedFile[1].indexOf(".pdf"));
						var actualImg = '/img/pdfImg/' + imageFile + '.png';
						$("#hidVal").val(actualImg);
						var sigCanvas = document.getElementById("canvasSignature");
							sigCanvas.width = sigCanvas.width;
					});
				//	initialize();
					
				}
				/*$.ajax({
						  type: "POST",
						  url: "multi_pdfUpload",
						  data: {data: selected
						  }
						}).done(function( respond ) {
						  console.log(respond);
						});
				*/
				$("#nxtEdit").prop("disabled",false);
				$("#back").show();
                
                if($('input[class*=inlineCheckbox]:checked').length && $('#pdfImg').val()){
                    $("#nxtTags").prop("disabled",false);
                }
                else{
                    $("#nxtTags").prop("disabled",true);
                }

			});

$("#nxtTags").on("click", function(){
    if(!$('input[class*=inlineCheckbox]').length)
        return;

    var selectedFiles = [];
    //set selected files to #pdfmultiImg
    $('input[class*=inlineCheckbox]:checked').each(function() {
        var checked_value = $(this).val();
        var checkedFile = checked_value.split('/splits/');
        var imageFile = checkedFile[1].substring(0,checkedFile[1].indexOf(".pdf"));
        var actualImg = '/img/pdfImg/' + imageFile + '.png';
        selectedFiles.push(actualImg);
    });

    $("#pdfmultiImg").val(selectedFiles);
});

$("#secondstep").on("click", function(e){
    e.preventDefault();
    if(!$("#hidVal").val())
        return false;

    initialize();
    $("#editimage").trigger("click");
});

			$('#confirmBox,#confirmBox1').click(function(e) {
					 if ($("#confirmBox").is(':checked') == true && $("#confirmBox1").is(':checked') == true) {
						
						$('#submitButton').prop('disabled',false);	
					}
				  else{
					$('#submitButton').prop('disabled',true);
				  }
            });
			
			
			$('#terms_conditions').click(function(e) {
				var win = window.open("/algemene-voorwaarden", "win", "scrollbars=1,width=600,height=600");
            });
			
			$('#back').click(function(e) {
				var sigCanvas = document.getElementById("canvasSignature");
				sigCanvas.width = sigCanvas.width;
						$("#back,#m-form").hide();
						$("#m-form").empty();
                        $("#fileUploadBox,.progress").fadeIn();
						$("#fileInput").removeAttr("disabled");
						$("#nxtEdit").prop("disabled",true);

                        editedFiles = [];
                        $("#pdfImg").val('');
                        $("#pdfmultiImg").val('');
						$("#hidVal").val('');
                        $("#nxtTags").attr("disabled",true);
            });
			
			$("#btn").click(function(e) {
			var sigCanvas = document.getElementById("canvasSignature");
		   
			if (sigCanvas.getContext) {
				var ctx = sigCanvas.getContext("2d");               
				var myImage = sigCanvas.toDataURL("image/png");     
			}
			var image = document.getElementById("image");
			image.src = myImage; 
			/*var imgName = $("#hidVal").val();
		   $.ajax({
			  type: "POST",
			  url: "upload",
			  data: {image: myImage, name: imgName
			  }
			}).done(function( respond ) {
			  console.log(respond);
			  $('#pdfImg').val(respond);
			});*/
			
			var winHeight = 600;
			var winWidth = 600;
			var left = (screen.width/2)-(winWidth/2);
  			var top = (screen.height/2)-(winHeight/2);
			window.open(image.src,  null, 'height=' + winHeight + ', width=' + winWidth + ', top=' + top + ', left=' + left + ', toolbar=0, location=0, status=0, scrollbars=0, resizable=0'); 
			
			$("#staticBtn2,#save").css("display","block");
					        	
    	});
		//	var arr = []; //editedFiles
			$("#staticBtn2").click(function(e) {
					var sigCanvas = document.getElementById("canvasSignature");
					if (sigCanvas.getContext) {
						var ctx = sigCanvas.getContext("2d");               
						var myImage = sigCanvas.toDataURL("image/png");     
					}
					var image = document.getElementById("image");
					image.src = myImage; 
					var imgName = $("#hidVal").val();
				   $.ajax({
					  type: "POST",
					  url: "upload",
					  data: {image: myImage, name: imgName
					  }
					}).done(function( respond ) {
					 var data = '/' + respond;
					  editedFiles.push(data);//need set by key imgName
					  $('#pdfImg').val(editedFiles);
					   alert("Je pdf is met succes bewerkt. Het PDF bestand wordt onbewerkt verstuurd naar de koper.");
					   $("#nxtTags").prop("disabled",false);
                    }).fail(function (respond) {
                        alert("Een ogenblik geduld a.u.b. De door u opgevraagde pagina wordt geladen.");
                        //submit form
                        $("form").attr("action", "/advertentie/upload");
                        $("form").append('<input type="hidden" name="image" value="'+ myImage +'">');       
                        $("form").append('<input type="hidden" name="name" value="'+ imgName +'">');
                        $("form").eq(0).trigger("submit");
                    });

                    $.fancybox.close();
			});
			$("#btn_unhappy").click(function(e) {
				$("#staticBtn2,#save").css("display","block");
				 var sigCanvas = document.getElementById("canvasSignature");
				 sigCanvas.width = sigCanvas.width;
				 var context = sigCanvas.getContext("2d");
				 context.strokeStyle = 'Black';
				 context.lineWidth = 20;
			  	 var img = new Image();
				 var hiddenSrc = document.getElementById("hidVal").value;
			  	 img.src = hiddenSrc;
				// console.log(img.src);
				 img.onload = function(){
				   context.drawImage(img,0,0);
			   } 
			   $("#canvasDiv").show();
			   $("#image,#status").hide();
			   $("#btn,#preview").show();
			});
			
		  function validate(){
				if ($('#categorie').val().length   	 >   0   &&
					$('#geldig-vanaf').val().length  >   0   &&
					$('#geldig-tot').val().length    >   0   && 
					$('#prijs').val().length    	 >   0   &&
					$('#twv').val().length  		   >   0   &&
					$('#type').val().length          >   0   &&
					$('#titel').val().length         >   0   &&
					$('#country').val().length       >   0   &&
					$('#duration').val().length      >   0   && 
					$('#rating').val().length        >   0   &&
					$('.arrangement:checked').length >   0   &&
					$('.facilities:checked').length >   0   &&
					$('#accomodation').val().length  >   0) 
				{
					$("#nxtInfo").prop('disabled',false);
				}
				else {
					$("#nxtInfo").prop('disabled',true);
				}
			}
		  function getPosition(mouseEvent, sigCanvas) {
			 var x, y;
			 var width_doc = $(window).width();
			 var height_doc = $(window).height();
			 var w = $('.fancybox-desktop').width(); 
			// var h = $('.fancybox-desktop').height();
			 
			 var total_width = width_doc - w;
			 var actual_wid =  total_width/2;
			 
			 var minOffsetTop = 20; // settings (calculation) on fancybox
			
				x = mouseEvent.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
				y = mouseEvent.clientY - $("#canvasDiv").position().top - $(".fancybox-wrap")[0].offsetTop + minOffsetTop;

			 return { X: x - (actual_wid +18), Y: y - 36  };
		  }
		  function initialize() {
			 var sigCanvas = document.getElementById("canvasSignature");
			 var context = sigCanvas.getContext("2d");
			 context.strokeStyle = 'black';
			 context.lineWidth = 20;
			 var img = new Image();
			 var a = document.getElementById("hidVal").value;
			 img.src = a;
			 img.onload = function(){
				  context.drawImage(img,0,0);
			 }
			 var is_touch_device = 'ontouchstart' in document.documentElement;
			 if (is_touch_device) {
				var drawer = {
				   isDrawing: false,
				   touchstart: function (coors) {
					  context.beginPath();
					  context.moveTo(coors.x, coors.y);
					  this.isDrawing = true;
				   },
				   touchmove: function (coors) {
					  if (this.isDrawing) {
						 context.lineTo(coors.x, coors.y);
						 context.stroke();
					  }
				   },
				   touchend: function (coors) {
					  if (this.isDrawing) {
						 this.touchmove(coors);
						 this.isDrawing = false;
					  }
				   }
				};
				function draw(event) {
				   var coors = {
					  x: event.targetTouches[0].pageX,
					  y: event.targetTouches[0].pageY
				   };
				   var obj = sigCanvas;
				   if (obj.offsetParent) {
					  do {
						 coors.x -= obj.offsetLeft;
						 coors.y -= obj.offsetTop;
					  }
					  while ((obj = obj.offsetParent) != null);
				   }
				   drawer[event.type](coors);
				}
				sigCanvas.addEventListener('touchstart', draw, false);
				sigCanvas.addEventListener('touchmove', draw, false);
				sigCanvas.addEventListener('touchend', draw, false);
				sigCanvas.addEventListener('touchmove', function (event) {
				   event.preventDefault();
				}, false); 
			 }
			 else {
				$("#canvasSignature").mousedown(function (mouseEvent) {
				   var position = getPosition(mouseEvent, sigCanvas);
	 
				   context.moveTo(position.X, position.Y);
				   context.beginPath();
				   $(this).mousemove(function (mouseEvent) {
					  drawLine(mouseEvent, sigCanvas, context);
				   }).mouseup(function (mouseEvent) {
					  finishDrawing(mouseEvent, sigCanvas, context);
				   }).mouseout(function (mouseEvent) {
					  finishDrawing(mouseEvent, sigCanvas, context);
				   });
				});
	 
			 }
		  }
		  function drawLine(mouseEvent, sigCanvas, context) {
			 var position = getPosition(mouseEvent, sigCanvas);
			 context.lineTo(position.X, position.Y);
			 context.stroke();
		  }
		  function finishDrawing(mouseEvent, sigCanvas, context) {
			 drawLine(mouseEvent, sigCanvas, context);
			 context.closePath();
			 $(sigCanvas).unbind("mousemove")
						 .unbind("mouseup")
						 .unbind("mouseout");
		  }
		});
	</script>
@stop