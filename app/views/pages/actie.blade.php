@extends('layouts.main')

@section('page')
	<div class="page">
		<div class="row">
			<div class="medium-12 columns">
				<div class="actie">
					<div class="picture" style="background:url('{{$actie->fotoUrl}}') 50% 50% / cover no-repeat;height:300px;"></div>
					<div class="content">
						<h2>{{$actie->titel}}</h2>
						<br><hr><br><br>
						<div class="row">
							<div class="medium-3 columns text-left">
								<h3>Voorwaarden</h3>
								{{$actie->voorwaarden}}
							</div>
							<div class="medium-9 columns text-left">
								<h3>Omschrijving</h3>
								{{$actie->omschrijving}}
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@stop