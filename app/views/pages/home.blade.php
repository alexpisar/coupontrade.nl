@extends('layouts.main')
@section('page')

	@if(!Auth::check())
	<div class="popup-overlay"></div>
	<div class="popup-container">
		<div class="row">
			<div class="medium-6 medium-offset-3 columns">
				<div class="box" id="subscribeBox">
					<a id="subscribeClose"><i class="ion-close"></i></a>
					<h3>Mis nooit meer de beste deals van CouponTrade!</h3>
					<p>
						Laat je mail achter voor de beste deals en maak kans op een gratis overnachting in een suite in een van de Van der Valk hotels naar keuze!
					</p>
					<input type="email" placeholder="Voer je e-mail adres in" id="subscribeValue">
					<button class="button button-submit radius" id="subscribeBtn">Ik doe mee!</button>
				</div>
			</div>
		</div>
	</div>
	@endif

		{{-- <div class="padding"> --}}
        {{-- <br/><br/><br/> --}}
         <!-- Start WOWSlider.com BODY section -->
      <div class="topSliderBlock">
        <div id="wowslider-container1">
        
        <div class="ws_images"><ul class="ws_list">
                {{-- <li><img src="/data1/images/slider_1.jpg" alt="1" title="" id="wows1_0"/>Coupontrade is DE Marktplaats voor hotelbonnen!</li> --}}
                {{-- <li><img src="/data1/images/slider_2.jpg" alt="css slider" title="" id="wows1_1"/>  Verkoop makkelijk, veillig en snel jouw coupons op Coupontrade! </li> --}}
                {{-- <li><img src="/data1/images/slider_3.jpg" alt="3" title="" id="wows1_2"/> Koop op Coupontrade de leukste en voordeligste hotelvakanties, dagjes uit en reizen</li> --}}
                {{-- <li><img src="/data1/images/slider_4.jpg" alt="4" title="" id="wows1_3"/></li>
                <li><img src="/data1/images/slider_5.jpg" alt="5" title="" id="wows1_4"/></li> --}}
                <li><img src="/data1/images/slide31_full.jpg" title="" alt="1"></li>
            </ul></div>
            	
       {{-- <script type="text/javascript" src="/engine1/wowslider.js"></script> 
        <script type="text/javascript" src="/engine1/script.js"></script> --}}
        <!-- End WOWSlider.com BODY section -->
        
		</div><?php /* wowslider-container1 end */?>   
     <div class="container">
      <div class="row padding relative-pos"> 
          <div class="text-container">
            <h2>Goedkope hotelbonnen vindt je op Coupontrade! Dit is de marktplaats voor hotelbonnen. Koop of verkoop snel je hotelvakanties of dagjes uit  <br />
            <small> Veilig, Makkelijk en Snel </small></h2>
                <div class="actie-buttons pull-bottom">
                    @if(!Auth::check())
                        <a href="{{Facebook::getLoginUrl()}}" class="button button-facebook radius"><span class="ion-social-facebook"></span>Log in met Facebook</a>
                        <a href="/inloggen" class="button button-action radius"><span class="ion-email"></span> Of registreer met je e-mail adres</a>
                    @endif
                 </div>
                 
                  <div id="fb-root"></div>
                  <script>(function(d, s, id) {
                    var js, fjs = d.getElementsByTagName(s)[0];
                    if (d.getElementById(id)) return;
                    js = d.createElement(s); js.id = id;
                    js.src = "//connect.facebook.net/nl_BE/sdk.js#xfbml=1&version=v2.5";
                    fjs.parentNode.insertBefore(js, fjs);
                  }(document, 'script', 'facebook-jssdk'));</script>

                  <div class="fb-like" data-href="https://www.facebook.com/coupontradeNL" data-layout="button_count" data-action="like" data-show-faces="true" data-share="true">

                  </div>
          </div>
        <div class="extra">
                <div class="search">
          <h1>Filter advertenties</h1>
            <form id="formSearch" name="formSearch" method="post" action="searchPage">
                <div style=" width:90%; margin:auto;">
                    <div style="width:50%; float:left;  text-align:left; margin-left:-20px" >
                      <h2>Land/Regio</h2>
                        <select name="country">
                         <option value="">Kies</option>
                          <?php 
                  $countries = isset($countries) ? $countries : '';
                 if(!empty($countries)){

                  foreach($countries as $country){ 
                  if(!empty($country)){
                  ?>
                                 <option value="<?php echo $country ?>"><?php echo $country ?></option>
                <?php } } }?>
                        </select>
                    </div>
                    
                    <div style="width:50%; float:right;margin-right:5px " >
                      <h2>Prijs</h2>
                       <select name="price">
                         <option value="">Kies</option>
                         <?php 
                  $price = isset($price) ? $price : '';
                 if(!empty($price)){

                  foreach($price as $money){ 
                  if(!empty($money)){
                  ?>
                                 <option value="<?php echo $money ?>">&euro;<?php echo $money ?></option>
                <?php } } }?>
                        </select>
                    </div>
                    <br/>
                    <h2 style="margin-left:-7px">Verzorging</h2>
                    <table width="200" border="0" align="left" cellpadding="0" cellspacing="0" class="check_table">
                      <tr>
                        <td class="tableRow">Logies</td>
                        <td class="tableRow"><input name="arrangement[]" type="checkbox" value="Logies" class="check"></td>
                        <td class="tableRow">Logies en ontbijt</td>
                        <td class="tableRow"><input name="arrangement[]" type="checkbox" value="Logies en ontbijt" class="check"></td>
                      </tr>
                      <tr>
                        <td class="tableRow">Half pension</td>
                        <td class="tableRow"><input name="arrangement[]" type="checkbox" value="Half pension" class="check"></td>
                        <td class="tableRow">Hotelarrangement</td>
                        <td class="tableRow"><input name="arrangement[]" type="checkbox" value="Hotelarrangement" class="check"></td>
                      </tr>
                       <tr>
                        <td class="tableRow">Overig</td>
                        <td class="tableRow"><input name="arrangement[]" type="checkbox" value="Overig" class="check"></td>
                      </tr>
          </table>
                    <input type="submit" name="searchBtn" id="searchBtn" value="Zoeken" class="buttonSearch">
                    <input type="submit" name="searchBtn2" value="Uitgebreid Zoeken" id="buttonSearch" style="margin-left:23px">
                </div>
          </form>
        </div>
      
        </div>

        </div> {{-- end internal row --}}
        <div class="clearBoth"></div>
        </div> {{-- end container --}}
        <div class="clearBoth"></div>
        </div><?php /* topSliderBlock end */?>
      {{--  <div class="ws_script" style="position:absolute;left:-99%"><a href="http://wowslider.com">image carousel</a> by WOWSlider.com v7.9</div> --}}
        <div class="ws_shadow"></div>
        {{-- </div> --}} {{-- end external row padding --}}
        
	 <div class="extraAdv">
      <div class="row">
         <div class="small-3 medium-3 columns">
           <img src="/img/recentadv/icon-step1.png" alt="icon-step1">
           <span>Login met facebook of maak gratis een account</span>
         </div>
         <div class="small-3 medium-3 columns">
            <img src="/img/recentadv/icon-step2.png" alt="icon-step2">
            <span>Plaats je advertentie</span>
         </div>
         <div class="small-3 medium-3 columns">
            <img src="/img/recentadv/icon-step3.png" alt="icon-step3">
            <span>Deel je advertentie </span>
         </div>
         <div class="small-3 medium-3 columns">
            <img src="/img/recentadv/icon-step4.png" alt="icon-step4">
            <span>In een mum van tijd wordt je advertentie verkocht</span>
         </div>
      </div>
       <div class="clearBoth"></div>
    </div>

  <div class="row">
      <div class="">
               <div class="recentAdvert">
                <h3 style="margin-left:15px;">Recente advertenties <small>
                <a href="/categorieen" class="button button-submit radius">Alles bekijken</a></small></h3>
               </div>
              <div class="clearBoth"></div>
        </div>
      <div class="clearBoth"></div>
  </div>
    <div class="row padding">
    @if(count($ads) > 0)
            

            @foreach($ads as $bon)
          <!--{{$ads}}-->
				@include("pages.partials.bon")
			@endforeach
			@if(count($ads) != 12)
				<div class="medium-4 columns end">
					<a href="/advertentie/plaatsen" class="bon-plus-link">
						<div class="bon-plus">
							<i class="ion-ios-plus-outline"></i>
							<br>
							<span>Er zijn verder geen recente advertenties... Plaats de jouwe gratis!</span>
						</div>
					</a>
				</div>
			@endif
		@else
		<div class="geen-advertenties">
			<h1>
				<i class="ion-sad-outline"></i>
			</h1>
			<h4>
				Helaas... We zijn uitverkocht!
			</h4>
			<p>
				Coupon, e-ticket of een gewonnen veiling te koop? <a href="/advertentie/plaatsen">Plaats gratis een advertentie!</a>
				<br>
				Of <a href="/inloggen">registreer</a> je om een melding te ontvangen wanneer er weer advertenties zijn geplaatst!
			</p>
		</div>
		@endif
	</div>
	@if(count($gemist) > 0)
	<div class="row padding">
		<h3 style="margin-left:15px;">Dit heb je gemist</h3>
		@foreach($gemist as $g)
		<div class="medium-4 columns end">
			<div class="well gemist-box">
				<div class="gemist-titel">
					<h5 style="line-height:1.1;">{{{ str_replace('<br />', '', strlen($g->titel) > 65 ? substr($g->titel,0,65)."..." : $g->titel) }}}</h5>
				</div>
				<div class="gemist-text">
					{{{ str_replace('<br />', '', strlen($g->omschrijving) > 95 ? substr($g->omschrijving,0,95)."..." : $g->omschrijving) }}}
				</div>
				<h3 style="line-height:0.4em;">
					<small>Verkocht op {{$g->datum}}</small>
				</h3>
				<h2 style="position:absolute;bottom:-10px;right:10px;"><b>&euro;{{$g->prijs}}</b></h2>
			</div>
		</div>
		@endforeach
	</div>
	@endif
	<div class="bekend-van">
		<h3>Bekend van o.a.</h3>
		<div class="row">
			<div class="medium-4 columns text-center">
				<img src="/img/bekendvan/nu.png" width="150px">
			</div>
			<div class="medium-4 columns text-center">
				<img src="/img/bekendvan/frankwatching.png">
			</div>
			<div class="medium-4 columns text-center">
				<img src="/img/bekendvan/dutchcowboys.png" width="200px">
			</div>
		</div>
	</div>
    <br />
@stop
@section('scripts')
{{--<script src="js/foundation.min.js"></script>
<script type="text/javascript">
    $(document).foundation();
</script> --}}
@stop