@extends('layouts.main')
@section('page')
    <div class="page">
    <script type="text/javascript">
      $(document).ready(function () {
	   initialize();  
	   $("#headingCnvs1,#headingCnvs2,#headingCnvs3,#headingCnvs4,#btnFinal").css("display","none");
	   $("#headingCnvs").click(function(e) {
			$("#upload").toggle();
		});
	   $("#headingCnvs1").click(function(e) {
			$("#canvasDiv").toggle();
		}); 
	   $("#headingCnvs2").click(function(e) {
			$(".canvasBtn").toggle();
		});
	   $("#headingCnvs3").click(function(e) {
			$(".canvasBuy,#info").toggle();
		});	
       $("#headingCnvs4").click(function(e) {
			$("#info").toggle();
			$("#btnFinal").show();
		});
	   $("#btn").click(function(e) {
		   $("#loadingImg").css("display","block");
		   setTimeout(function(){
			   $("#btn").css("display","none");
			   $("#loadingImg").css("display","none");
			   $("#image").css("display","block");
			   $("#staticBtn2,#btn_unhappy").css("display","block");
			   },5000);
		   
		   var sigCanvas = document.getElementById("canvasSignature");
		   //var a = convertCanvasToImage(sigCanvas);
		   
			if (sigCanvas.getContext) {
				var ctx = sigCanvas.getContext("2d");                // Get the context for the canvas.
				var myImage = sigCanvas.toDataURL("image/png");      // Get the data as an image.
			}
			
			var image = document.getElementById("image");  // Get the image object.
			image.src = myImage; 
					   
		   $.ajax({
			  type: "POST",
			  url: "upload",
			  data: {image: myImage
			  }
			}).done(function( respond ) {
			  // you will get back the temp file name
			  // or "Unable to save this image."
			  console.log(respond);
			});
        	
    	});
		
	   $("#btnBuy").click(function(e) {					   
		   $.ajax({
			  type: "POST",
			  url: "mailPdf",
			  data: {
			  }
			}).done(function( respond ) {
			  // you will get back the temp file name
			  // or "Unable to save this image."
			  alert("Mail sent successfully!!")
			});
        	
    	});
		

		$("#staticBtn").click(function(e) {
 		   $("#headingCnvs1").css("display","block");
		   $("#upload,#staticBtn").css("display","none");
		   $("#canvasDiv").css("display","block");
        	
    	});
		
		$("#staticBtn1").click(function(e) {					   
		   $("#headingCnvs2").css("display","block");
		   $("#canvasDiv").hide();
        	
    	});
	   
		$("#staticBtn2").click(function(e) {					   
		   /*$("#headingCnvs3").css("display","block");
		   $(".canvasBtn").hide();*/
		   $("#headingCnvs3").css("display","block");
		   $(".canvasBtn").hide();
		});
		
			$("#btnFinal").click(function(e) {					   
		   /*$("#headingCnvs3").css("display","block");
		   $(".canvasBtn").hide();*/
		   $("#headingCnvs3").css("display","block");
		   $("#info").hide();
		});
		
		
		

		$("#btn_unhappy").click(function(e) {
				$(".canvasBtn").hide();
				
				var sigCanvas = document.getElementById("canvasSignature");
			sigCanvas.width = sigCanvas.width;
			 var context = sigCanvas.getContext("2d");
			 context.strokeStyle = 'Black';
			 context.lineWidth = 20;
			 
			 
			  var img = new Image();
				//img.src = '1.jpg';
				
				img.src = document.getElementById("hidVal").value;
				console.log(img.src);
	
				img.onload = function(){
				   context.drawImage(img,0,0);
				  // context.beginPath();
				   //context.moveTo(30,96);
				   //context.lineTo(70,66);
				   //context.lineTo(103,76);
				   //context.lineTo(170,15);
				   //context.stroke();
			   } 
			   
			   $("#canvasDiv").show();
			   $("#image").hide();
			   $("#btn").show();
			});
	   
      });
	  

      // works out the X, Y position of the click inside the canvas from the X, Y position on the page
      function getPosition(mouseEvent, sigCanvas) {
         var x, y;
         if (mouseEvent.pageX != undefined && mouseEvent.pageY != undefined) {
            x = mouseEvent.pageX;
            y = mouseEvent.pageY;
         } else {
            x = mouseEvent.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
            y = mouseEvent.clientY + document.body.scrollTop + document.documentElement.scrollTop;
         }
 
         return { X: x - sigCanvas.offsetLeft, Y: y - sigCanvas.offsetTop };
      }
 
      function initialize() {
         // get references to the canvas element as well as the 2D drawing context
         var sigCanvas = document.getElementById("canvasSignature");
         var context = sigCanvas.getContext("2d");
         context.strokeStyle = 'Black';
		 context.lineWidth = 20;
		 
		 
		  var img = new Image();
			//img.src = '1.jpg';
			
			img.src = document.getElementById("hidVal").value;
			console.log(img.src);

			img.onload = function(){
			   context.drawImage(img,0,0);
			  // context.beginPath();
			   //context.moveTo(30,96);
			   //context.lineTo(70,66);
			   //context.lineTo(103,76);
			   //context.lineTo(170,15);
			   //context.stroke();
		   }
 
         // This will be defined on a TOUCH device such as iPad or Android, etc.
         var is_touch_device = 'ontouchstart' in document.documentElement;
 
         if (is_touch_device) {
            // create a drawer which tracks touch movements
            var drawer = {
               isDrawing: false,
               touchstart: function (coors) {
                  context.beginPath();
                  context.moveTo(coors.x, coors.y);
                  this.isDrawing = true;
               },
               touchmove: function (coors) {
                  if (this.isDrawing) {
                     context.lineTo(coors.x, coors.y);
                     context.stroke();
                  }
               },
               touchend: function (coors) {
                  if (this.isDrawing) {
                     this.touchmove(coors);
                     this.isDrawing = false;
                  }
               }
            };
 
            // create a function to pass touch events and coordinates to drawer
            function draw(event) {
 
               // get the touch coordinates.  Using the first touch in case of multi-touch
               var coors = {
                  x: event.targetTouches[0].pageX,
                  y: event.targetTouches[0].pageY
               };
 
               // Now we need to get the offset of the canvas location
               var obj = sigCanvas;
 
               if (obj.offsetParent) {
                  // Every time we find a new object, we add its offsetLeft and offsetTop to curleft and curtop.
                  do {
                     coors.x -= obj.offsetLeft;
                     coors.y -= obj.offsetTop;
                  }
				  // The while loop can be "while (obj = obj.offsetParent)" only, which does return null
				  // when null is passed back, but that creates a warning in some editors (i.e. VS2010).
                  while ((obj = obj.offsetParent) != null);
               }
 
               // pass the coordinates to the appropriate handler
               drawer[event.type](coors);
            }
            // attach the touchstart, touchmove, touchend event listeners.
            sigCanvas.addEventListener('touchstart', draw, false);
            sigCanvas.addEventListener('touchmove', draw, false);
            sigCanvas.addEventListener('touchend', draw, false);
 
            // prevent elastic scrolling
            sigCanvas.addEventListener('touchmove', function (event) {
               event.preventDefault();
            }, false); 
         }
         else {
 
            // start drawing when the mousedown event fires, and attach handlers to
            // draw a line to wherever the mouse moves to
            $("#canvasSignature").mousedown(function (mouseEvent) {
               var position = getPosition(mouseEvent, sigCanvas);
 
               context.moveTo(position.X, position.Y);
               context.beginPath();
 
               // attach event handlers
               $(this).mousemove(function (mouseEvent) {
                  drawLine(mouseEvent, sigCanvas, context);
               }).mouseup(function (mouseEvent) {
                  finishDrawing(mouseEvent, sigCanvas, context);
               }).mouseout(function (mouseEvent) {
                  finishDrawing(mouseEvent, sigCanvas, context);
               });
            });
 
         }
      }
	  
 
      // draws a line to the x and y coordinates of the mouse event inside
      // the specified element using the specified context
      function drawLine(mouseEvent, sigCanvas, context) {
 
         var position = getPosition(mouseEvent, sigCanvas);
 
         context.lineTo(position.X, position.Y);
         context.stroke();
      }
 
      // draws a line from the last coordiantes in the path to the finishing
      // coordinates and unbind any event handlers which need to be preceded
      // by the mouse down event
      function finishDrawing(mouseEvent, sigCanvas, context) {
         // draw the line to the finishing coordinates
         drawLine(mouseEvent, sigCanvas, context);
 
         context.closePath();
 
         // unbind any events which could draw
         $(sigCanvas).unbind("mousemove")
                     .unbind("mouseup")
                     .unbind("mouseout");
      }
   </script>
   
   
       <h3 id="headingCnvs">Step 1 : Upload Pdf </h3>
       		<div id="upload">
                <form enctype= "multipart/form-data" action="/uploadPdf" method="post">
                Please choose a .pdf to upload: <input type="file" name="fileInput" id="fileInput"><br>
                <input type="submit" id="submitbtn" name="submit" value="Upload" class="button"></form><br>
            </div>
            <button id="staticBtn"> Move to next step >> </button>

       <h3 id="headingCnvs1">Step 2 : Edit Pdf-image </h3>
            <div id="canvasDiv">
                <canvas id="canvasSignature" width="1275px" height="1650px"></canvas>
                <input type="hidden" value="{{ Session::get('var') }}" id="hidVal">
                <br>
                <button id="staticBtn1"> Move to next step >> </button>
            </div>
            
       <h3 id="headingCnvs2">Step 3 : Preview Image </h3>
            <div class="canvasBtn">
                <button id="btn">Preview</button>
                <img id="loadingImg" src="/img/loading.gif"/>
                <img id="image" src=""/>
                <button id="staticBtn2"> Happy with the Preview Image & move to final </button>
                <button id="btn_unhappy" style="display:none"> Re-edit the Image</button>
          	</div>
                 

        <!--<h3 id="headingCnvs4">Step 4 : Additional Information </h3>
       <div id="info" style="display:none">
           {{Form::open(['url' => ''])}}
            <div class="row">
						<div class="medium-3 columns">
							<label class="left inline"> Information 1</label>
						</div>
						<div class="medium-9 columns">
							{{ Form::text('info1', '', ['placeholder' => '(additional information)']) }}
						</div>
		    </div>
             <div class="row">
						<div class="medium-3 columns">
							<label class="left inline"> Information 1</label>
						</div>
						<div class="medium-9 columns">
							{{ Form::text('info1', '', ['placeholder' => '(additional information)']) }}
						</div>
		    </div>
             <div class="row">
						<div class="medium-3 columns">
							<label class="left inline"> Information 1</label>
						</div>
						<div class="medium-9 columns">
							{{ Form::text('info1', '', ['placeholder' => '(additional information)']) }}
						</div>
		    </div>

          </div>-->
           

			<h3 id="headingCnvs3">Final Step  : Buy Coupon </h3>
            <div id="info" style="display:none">
           {{Form::open(['url' => ''])}}
            <div class="row">
						<div class="medium-3 columns">
							<label class="left inline"> Information 1</label>
						</div>
						<div class="medium-9 columns">
							{{ Form::text('info1', '', ['placeholder' => '(additional information)']) }}
						</div>
		    </div>
             <div class="row">
						<div class="medium-3 columns">
							<label class="left inline"> Information 1</label>
						</div>
						<div class="medium-9 columns">
							{{ Form::text('info1', '', ['placeholder' => '(additional information)']) }}
						</div>
		    </div>
             <div class="row">
						<div class="medium-3 columns">
							<label class="left inline"> Information 1</label>
						</div>
						<div class="medium-9 columns">
							{{ Form::text('info1', '', ['placeholder' => '(additional information)']) }}
						</div>
		    </div>
            {{ Form::close() }}

          </div>
            
           <div class="canvasBuy"> 
           		<button id="btnBuy">Buy Coupon</button>
 		   </div>
           
	</div>
@stop