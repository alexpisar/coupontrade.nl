 <div class="medium-4 columns end">
  <a href="/advertentie/{{$bon->advertentieId}}-{{$bon->titelUrl}}">
   <div class="well bon bon-advertine">
     <div class="content">
        @if( $bon->picture && file_exists(public_path().'/'.$bon->picture) )
        <div class="img" style="background:url('/{{ $bon->picture }}') 50% 50% / cover;"></div>
        @else
        <div class="img imgRadius" style="background:url('/img/categorie/{{$bon->categorie}}.jpg') 50% 50% / cover;"></div>
        @endif

        @if( $bon->arrangement || $bon->facilities || $bon->accomodation || $bon->rating)
            <div class="prijs has-tip radius" data-tooltip aria-haspopup="true" data-options="show_on:large" title="">
                <div class="head-prijs">
                    <small class="text-red-small">t.w.v. &euro;{{$bon->twv}}</small> <span>&euro;</span>{{ $bon->prijs }}
                </div>
                {{-- Pop up --}}
                <span class="tooltip radius" role="tooltip">
                @if($bon->arrangement)
                    <h6>Verzorging: </h6>
                    <ul>
                        @foreach(explode(',', $bon->arrangement) as $arrang)
                            <li>{{$arrang}}</li>
                        @endforeach
                    </ul>
                    <hr>
                @endif
                @if($bon->facilities)
                    <h6>Faciliteiten: </h6>
                    <ul>
                        @foreach(explode(',', $bon->facilities) as $facility)
                            <li>{{$facility}}</li>
                        @endforeach
                    </ul>
                    <hr>
                @endif
                @if($bon->accomodation)
                    <h6>Accommodatie: {{$bon->accomodation}}</h6>
                    <hr>
                @endif
                @if($bon->rating)
                    <h6>Aantal sterren: 
                        @if(intval($bon->rating) > 1)
                            {{intval($bon->rating)}}
                        @else
                            Overig
                        @endif
                    </h6>
                @endif
                <span class="nub"></span>
                </span>
            </div>
         @else
          <div class="prijs radius" data-tooltip aria-haspopup="true" data-options="show_on:large" title="">
            <div class="head-prijs">
              <small class="text-red-small">t.w.v. &euro;{{$bon->twv}}</small> <span>&euro;</span>{{ $bon->prijs }}
            </div>
          </div>
         @endif


        <div class="text">
            <h4 style="color:#3498db">
                {{ $bon->titel}}
            </h4>
            <h6 class="text-left"> Geldig tot:
                {{ date("d/m/Y", strtotime($bon->geldigTot)) }}
            </h6>
            <h6 class="text-left"> <i class="icon ion-ios-location"> </i> 
                {{$bon->country}}
            </h6>
            <h6 class="text-left"> Verblijfsduur: 
                {{$bon->duration}} dagen
            </h6>
            <div class="bon-footer">
               <button class="button button-buy radius right">Bekijk coupon</button>
            </div>                                
        </div>
     </div>
        
   </div>
  </a>
</div>