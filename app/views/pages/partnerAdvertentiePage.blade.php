@extends('layouts.main')

@section('page')
	<div class="page">
		<div class="row">
			<div class="medium-12 columns">
				<div class="actie advertentie-page" style="margin-top:-20px;">
					<div class="picture" style="background:url(/img/categorie/{{$ad->categorie}}.jpg) 50% 50% / cover no-repeat;height:300px;"></div>
					<div class="content">
						<h2>{{$ad->titel}}</h2>
						<br>
						<p>
							Aangeboden door <b>{{$ad->partner->naam}}</b> (partner)
						</p>
						<br><hr><br><br>
						<div class="row">
						@if($ad->verkocht == '0')
							<div class="medium-3 columns text-center">
								<h3 style="margin-top:-30px;" class="text-left">Voorwaarden</h3>
								<div style="text-align:left;">{{$ad->voorwaarden}}</div>
								<hr>
								T.w.v. &euro;<b>{{$ad->twv}}</b> voor
								<div class="prijzer"><span>&euro;</span>{{$ad->prijs}}</div>
								{{Form::open(['url' => '/advertentie/' . $ad->advertentieId . '-' . $ad->titelUrl . '/betalen'])}}
								@if($ad->multi == '1')
								<span style="line-height:2.75em;">Nog {{$ad->count}} beschikbaar.</span>
								<select name="aantal">
									<option value="0">Hoeveel wil je er kopen?</option>
									@foreach(range(1, $ad->count) as $opt)
									<option value="{{$opt}}">{{$opt}}</option>
									@endforeach
								</select>
								@endif
								{{Form::submit('Betalen met iDeal', ['class' => 'button button-buys radius'])}}
								{{Form::close()}}
							</div>
						@else
							<div class="medium-3 columns text-center">
								T.w.v. &euro;<b>{{$ad->twv}}</b> voor
								<div class="prijzer"><span>&euro;</span>{{$ad->prijs}}</div>
								<button class="button button-blue radius" disabled>Dit aanbod is uitverkocht</button>
							</div>
						@endif
							<div class="medium-9 columns text-left">
								<h3 style="margin-top:-30px;">Omschrijving</h3>
								{{$ad->omschrijving}}
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		@if(count($meer) > 0)
		<div class="row">
			<div class="medium-12 no-side-padding">
			<hr>
			<h3>&nbsp;<small>Andere advertenties van deze partner</small></h3>
				@foreach($meer as $bon)
					@if($bon->advertentieId !== $ad->advertentieId)
						<div class="medium-4 col">
							<div class="well bon">
								<div class="verkoper-details">
									<h4><small>Aangeboden door</small></h4>
									<h3>{{$bon->partner->naam}}</h3>
									<p>in <a href="/categorie/{{$bon->categorie}}">{{ucwords(str_replace('-', ' ', $bon->categorie))}}</a></p>
								</div>
								<div class="picture" style="background:url({{$bon->partner->pictureUrl}}) center center / cover no-repeat;"></div>
								<div class="content">
									<h4>
										<a class="bonlink" href="/advertentie/{{$bon->advertentieId}}-{{$bon->titelUrl}}">{{ $bon->titel }}</a>
									</h4>
									<p>
										{{ strip_tags(strlen($bon->omschrijving) > 130 ? substr($bon->omschrijving,0,130)."..." : $bon->omschrijving) }}
									</p>
									<div class="prijs">
										<a href="/advertentie/{{$bon->advertentieId}}-{{$bon->titelUrl}}"><button class="button button-buy radius left">Kopen</button></a>
										<h1 class="right">
											<span>&euro;</span>{{ $bon->prijs }}
										</h1>
									</div>
								</div>
							</div>
						</div>
					@endif
				@endforeach
				<div class="medium-4 columns end">
					<a href="/advertentie/plaatsen" class="bon-plus-link">
						<div class="bon-plus">
							<br><br><br>
							<span>Deze partner heeft verder geen advertenties op CouponTrade.</span>
						</div>
					</a>
				</div>
			</div>
		</div>
		@endif
	</div>
@stop