<script src="/js/assets/js/jquery-1.11.1.min.js" type="text/javascript"></script>
<script src="/js/assets/js/jquery-ui-1.10.2.custom.min.js" type="text/javascript"></script>
<script src="js/lib/json_query.js" type="text/javascript"></script>
<script src="/js/filter_old.js" type="text/javascript"></script>
<script src="/js/customFilter_old.js" type="text/javascript"></script>  
<link href="/js/assets/css/style.css" media="screen" rel="stylesheet" type="text/css">
<link href="/js/assets/css/jquery-ui-1.10.2.custom.min.css" media="screen" rel="stylesheet" type="text/css">
@extends('layouts.main')
   @section('page')   
   
   <div class="page">
   		
        <script type="text/javascript">
             var services = <?php echo $data ?>;
	    </script>
	 <div class="row padding">  
     
      <div class="sidebar_bar">
       <div class="left_panel">
        	<div class="panel_content">
            		<h1 style="margin-left:-22px;text-align: center;">Advertentie zoeken</h1>
                    <div class="filter_box">
                    	 <h3>Prijs</h3><br/>
						 <span id="price_range_label">&euro;0-&euro;200</span>
						 <div id="price_slider"></div>
						 <input type="hidden" id="price_filter" value="0-200"/>
                    </div>
                    
                    <div class="filter_box">
                    	 <h3>Verblijfsduur</h3><br/>
						 <span id="duration_range_label" style="margin-left:75px">1 - 30</span>
						 <div id="duration_slider"></div>
						 <input type="hidden" id="duration_filter" value="1-30"/>
                    </div>
                    
                    <div class="filter_box">
                    	<h3>Accomodatie</h3>
                        <br>
                            <select id="type">
                                <option value="all">Kies</option>
                                <option value="hotel">Hotel</option>
                                <option value="suite">Suite</option>
                                <option value="bedenbreakfast">Bed en breakfast</option>
                                <option value="pension">Pension</option>
                                <option value="herberg">Herberg</option>
                                <option value="overig">Overig</option>
                            </select>
                    </div>
                    <div class="filter_box">
                        <h3>Aantal sterren</h3>
                           <ul id="rating">
                                 <li><input type="checkbox" value="2sterren" /><span>2 sterren</span></li>
                                 <li><input type="checkbox" value="3sterren" /><span>3 sterren</span></li>
                                 <li><input type="checkbox" value="4sterren" /><span>4 sterren</span></li>
                                 <li><input type="checkbox" value="5sterren" /><span>5 sterren</span></li>
                                 <li><input type="checkbox" value="overig" /><span>Overig</span></li>
                            <ul>
                     </div>
                    <div class="filter_box">
							<h3>Land/Regio</h3>
                            <br/>
							<select id="country">
                            <option value="all">Kies</option>
							   <?php 
							    $countries = isset($countries) ? $countries : '';
							   if(!empty($countries)){

								  foreach($countries as $country){ 
								  if(!empty($country)){
								  ?>
                                 <option value="<?php echo $country ?>"><?php echo $country ?></option>
								<?php } } }?>
							</select>
                    </div>
                    <div class="filter_box">
							<h3>Faciliteiten</h3>
							<ul id="facility">
							   <?php 
							    $facilities = isset($facilities)?$facilities:'';
							   if(!empty($facilities)){

								  foreach($facilities as $facility){ 
								  if(!empty($facility)){
								  ?>
								 <li><input type="checkbox" value="<?php echo $facility ?>" id="<?php echo $facility ?>" /><span><?php echo $facility ?></span></li>
								<?php } } }?>
							<ul>
                    </div>
                    <div class="filter_box">
							<h3>Verzorging</h3>
							<ul id="arrangement">
							   <?php 
							    $arrangements = isset($arrangements)?$arrangements:'';
							   if(!empty($arrangements)){

								  foreach($arrangements as $arrangement){ 
								  if(!empty($arrangement)){
								  ?>
								 <li><input type="checkbox" value="<?php echo $arrangement ?>" id="<?php echo $arrangement ?>" /><span><?php echo $arrangement ?></span></li>
								<?php } } }?>
							<ul>
                    </div>
                    
            </div>
        </div>
      </div>
      <div class="featured_services_find">
      <h1 class="result_count"></h1>
        <div class="featured_list_find">
            <div id="service_list"></div>
        	<div id="lastContent" class="medium-4 columns end">
						 <a href="/advertentie/plaatsen" class="bon-plus-link">
							<div class="bon-plus">
								<i class="ion-ios-plus-outline"></i>
								<br>
								<span>Er zijn verder geen advertenties gevonden... Plaats de jouwe gratis!</span>
							</div>
						</a>
			 </div>
        </div>
      </div>
    
      <div class="clear"></div>
    <script id="template" type="text/html">
        <div class="medium-4 columns end">
         <a href="/advertentie/<%= advertentieId %>-<%= titelUrl %>">
          <div class="well bon width">
            <div class="content">
              <div style="background:url(/img/categorie/vakanties.jpg) 50% 50% / cover;" class="img imgRadius"></div>
               <div class="prijs">
                    <div class="head-prijs"><small class="text-red-small">t.w.v. &euro;<%= twv %></small><span>&euro;</span><%= prijs %></div>
               </div>
              <div class="text">
                <h4 style="color:#3498db"> <%= titel %> </h4>
                  <div class="bon-footer">
                    <button class="button button-buy radius right">Bekijk coupon</button>
                  </div> 
              </div> 
            </div>
           </div>
          </a>
       </div>
   </script>
     </div>      
    </div>
   @stop