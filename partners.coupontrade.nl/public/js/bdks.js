$(document).ready(function() {
	var heroHeight = $('.hero').height();
	var docuHeight = $(document).height();
	// var Showdown = require('/showdown');
	var converter = new Showdown.converter();

	if (heroHeight)
	{
		$(window).scroll(function()
		{
			var distance = $(window).scrollTop();
			if (distance > heroHeight - 50)
			{
				$('.header').addClass('nono');
			}
			else
			{
				$('.header').removeClass('nono');
			}
		});
	}
	else
	{
		$('.header').addClass('nono');
	}

	$('.footer').css('top', $(document).height() - 65);

	$('.toggleActiefPartnerAdvertentie').click(function() {
		var status 	= $(this).data('active');
		var id 		= $(this).data('aid');

		$.get('/advertentie/' + id + '/toggle-actief', function(data, res) {
			if (res == 'success')
			{
				if (status == 0)
				{
					$('.toggleActiefPartnerAdvertentie').html('<h3><span class="ion-toggle"></span></h3>');
					$('.toggleActiefPartnerAdvertentie').data('active', 1);
				}
				else
				{
					$('.toggleActiefPartnerAdvertentie').html('<h3 class="toggled"><span class="ion-toggle-filled"></span></h3>');
					$('.toggleActiefPartnerAdvertentie').data('active', 0);
				}
				setTimeout(function() {
					alert(data);
				}, 250);
			}
			else
			{
				alert(data);
			}
		});
	});


	$('#makeFactuurBtn').click(function() {
		$('#iconFactuurBtn').hide();
		$('#spinner').show();
		var id = $('#partnerId').val();
		setTimeout(function() {
			window.location.href = '/partner/' + id + '/make/maand-factuur';
		}, 500);
	});

});