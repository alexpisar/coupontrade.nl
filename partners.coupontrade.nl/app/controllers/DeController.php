<?php

class DeController extends BaseController {

	/* Views */
	public function loginPage()
	{
		if (Auth::check())
		{
			// $arr = [];
			// $nieuws = Nieuws::where('ontvangerId', '=', 'iedereen')->where('ontvangerId', '=', Auth::user()->referentieId, 'OR')->get();
			// foreach ($nieuws as $n)
			// {
			// 	$arr[] = $n;
			// }
			// $verkopen = Verkoop::where('verkoperId', '=', Auth::user()->referentieId)->get();
			// foreach ($verkopen as $v)
			// {
			// 	$arr[] = $v;
			// }
			// usort($arr, 'cmp');
			// return View::make('pages.feed')->withNieuws($arr);
			return Redirect::to('/overzicht');
		}
		else
		{
			return View::make('pages.loginPage');
		}
	}

	public function overzichtPage()
	{
		if (Auth::check())
		{
			return View::make('pages.overzichtPage');
		}
		else
		{
			return Redirect::to('/');
		}
	}

	/* DO's */
	public function doLogin()
	{
		$username = Input::get('username');
		$password = Input::get('password');

		if ($username && $password)
		{
			if (Auth::attempt(['referentieId' => $username, 'password' => $password]))
			{
				return Redirect::to('/');
			}
			else
			{
				return Redirect::back()->withInput()->with('whoops', 'Onjuiste referentie ID of wachtwoord ingevoerd.');
			}
		}
		else
		{
			return Redirect::back()->withInput()->with('whoops', 'Vul beide velden in.');
		}
	}

	public function doLogout()
	{
		Auth::logout();
		return Redirect::to('/');
	}

	public function partnerAdvertentieToggleActief($id)
	{
		if (Auth::check())
		{
			$ad = Advertentie::find($id);
			if ($ad->verkoperId == Auth::user()->referentieId)
			{
				if ($ad->verwijderd == 0)
				{
					if ($ad->increment('verwijderd'))
					{
						return Response::json('Advertentie op inactief gesteld.', 200);
					}
					else
					{
						return Response::json('Kon geen verbinding maken met de server.', 500);
					}
				}
				else
				{
					if ($ad->decrement('verwijderd'))
					{
						return Response::json('Advertentie op actief gesteld.', 200);
					}
					else
					{
						return Response::json('Kon geen verbinding maken met de server.', 500);
					}
				}
			}
			else
			{
				return Response::json('U kunt deze advertentie niet wijzigen.', 500);
			}
		}
		else
		{
			return Redirect::to('/');
		}
	}

	public function maandFactuurPartners($id)
	{
		if (Auth::check())
		{
			$partner = Partner::find($id);
			$verkopen 	= Verkoop::where('verkoperId', '=', $partner->referentieId)->orderBy('verkoopId', 'DESC')->get();
			$fctnr = date('mY') . '-' . mt_rand(100000, 999999);
			$totaal = 0;
			$tk = 0;

			if (count($verkopen) > 0)
			{
				$timestamp	= strtotime("now");
				$date1 = new DateTime();
				$date1->setTimestamp($timestamp);
				foreach ($verkopen as $v)
				{
					$date2 = new DateTime();
					$date2->setTimestamp(strtotime($v->createdAt));

					if ($date1->format('Y-m') === $date2->format('Y-m'))
					{
					    $boekingen[] = $v;
					    $totaal = $totaal + $v->advertentie->prijs;
					    $tk = $tk + 0.35;
					}
				}
			}
			else
			{
				$boekingen = [];
			}

			$data = [
				'factuurnummer'    => $fctnr,
				'datum'            => date('d-m-Y'),
				'btwnummer'        => $partner->btwnummer,
				'naam'             => $partner->naam,
				'straat'           => $partner->straat,
				'postcode'         => $partner->postcode . ' ' . $partner->stad,
				'ref'              => $partner->referentieId,
				'totaal'           => $totaal,
				'transactiekosten' => $tk
			];

		    $html = View::make('pdf.factuurVoorPartners')->withData($data)->withBoekingen($boekingen);
		    return PDFLIB::load($html, 'A4', 'portrait')->show();
		}
		else
		{
			return Redirect::to('/');
		}
	}

}

function cmp($a, $b) {
  if ($a['createdAt'] == $b['createdAt']) {
    return 0;
  }

  return ($a['createdAt'] < $b['createdAt']) ? 1 : -1;
}