<?php

/*
|--------------------------------------------------------------------------
| Get Routes
|--------------------------------------------------------------------------
|
|
|
*/

Route::get('/', 'DeController@loginPage');
Route::get('/uitloggen', 'DeController@doLogout');
Route::get('/overzicht', 'DeController@overzichtPage');
Route::get('/advertenties', 'DeController@advertentiesPage');
Route::get('/verkopen', 'DeController@verkopenPage');
Route::get('/advertentie/{id}/toggle-actief', 'DeController@partnerAdvertentieToggleActief');
Route::get('/partner/{id}/make/maand-factuur', 'DeController@maandFactuurPartners');

/*
|--------------------------------------------------------------------------
| Post Routes
|--------------------------------------------------------------------------
|
|
|
*/

Route::post('/login', 'DeController@doLogin');