<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;
use SammyK\LaravelFacebookSdk\FacebookableTrait;

class Partner extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait, FacebookableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $primaryKey = "partnerId";
	protected $table = 'Partners';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');


	/**
	 * Disable Laravel's Timestamps
	 *
	 */
	public $timestamps = false;

	// hasOne 'Bdks'
	public function manager()
	{
		return $this->hasOne('Bdks', 'bdksId', 'managerId');
	}

	// hasMany 'Advertentie'
	public function advertenties()
	{
		return $this->hasMany('Advertentie', 'verkoperId', 'referentieId');
	}

	public function verkopen()
	{
		return $this->hasMany('Verkoop', 'verkoperId', 'referentieId');
	}

	public function waardenummers()
	{
		return $this->hasMany('Waardenummer', 'partnerRef', 'referentieId');
	}

	public function nieuws()
	{
		return $this->hasMany('Nieuws', 'ontvangerId', 'referentieId');
	}

}