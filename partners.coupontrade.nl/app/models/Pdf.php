<?php

class Pdf extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $primaryKey = "pdfId";
	protected $table = 'Pdfs';

	/**
	 * Disable Laravel's Timestamps
	 *
	 */
	public $timestamps = false;

	// haseOne 'Advertentie'
	public function advertentie()
	{
		return $this->hasOne('Advertentie', 'advertentieId', 'advertentieId');
	}

	// Inverse of hasMany from 'User'
	public function user()
	{
		return $this->belongsTo('User', 'userId', 'koperId');
	}

}