<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;
use SammyK\LaravelFacebookSdk\FacebookableTrait;

class Bdks extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait, FacebookableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $primaryKey = "bdksId";
	protected $table = 'bdks';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');

	/**
	 * Disable Laravel's Timestamps
	 *
	 */
	public $timestamps = false;

	// Inverse of hasOne from 'Partner'
	public function partner()
	{
		return $this->belongsTo('Bdks', 'bdksId', 'managerId');
	}

}