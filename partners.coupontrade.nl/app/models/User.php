<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;
use SammyK\LaravelFacebookSdk\FacebookableTrait;

class User extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait, FacebookableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $primaryKey = "userId";
	protected $table = 'Users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token', 'access_token', 'iban', 'bankhouder');

	/**
	 * Disable Laravel's Timestamps
	 *
	 */
	public $timestamps = false;

	// hasMany 'Advertentie'
	public function advertenties()
	{
		return $this->hasMany('Advertentie', 'verkoperId', 'userId');
	}

	// hasMany 'Pdf'
	public function aankopen()
	{
		return $this->hasMany('Pdf', 'koperId', 'userId');
	}

	// Inverse of hasOne relation from 'Advertentie'
	public function advertentie()
	{
		return $this->belongsTo('Advertentie', 'userId', 'verkoperId');
	}

}