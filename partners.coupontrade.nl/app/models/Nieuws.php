<?php

class Nieuws extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $primaryKey = "nieuwsId";
	protected $table = 'Nieuws';

	/**
	 * Disable Laravel's Timestamps
	 *
	 */
	public $timestamps = false;

}