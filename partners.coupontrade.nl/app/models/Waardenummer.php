<?php

class Waardenummer extends Eloquent {

	protected $primaryKey = "waardeId";
	protected $table = 'Waardenummers';

	public $timestamps = false;

	public function partner()
	{
		return $this->hasOne('Partner', 'referentieId', 'partnerRef');
	}

	public function advertentie()
	{
		return $this->hasOne('Advertentie', 'verkoperId', 'partnerRef');
	}

	public function koper()
	{
		return $this->hasOne('User', 'userId', 'koperId');
	}

	public function pdf()
	{
		return $this->hasOne('Pdf', 'pdfId', 'pdfId');
	}

}