<!--
    _______                                 _______                 _        
   (_______)                               (_______)               | |       
    _        ___   _   _  ____    ___   ____   _   ____  _____   __| | _____ 
   | |      / _ \ | | | ||  _ \  / _ \ |  _ \ | | / ___)(____ | / _  || ___ |
   | |_____| |_| || |_| || |_| || |_| || | | || || |    / ___ |( (_| || ____|
    \______)\___/ |____/ |  __/  \___/ |_| |_||_||_|    \_____| \____||_____)
                         |_|  
    
  Design en development door co-founder Sharif Paksa. http://followestu.com/  
-->
<!DOCTYPE html>
<html lang="nl">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>CouponTrade Partners</title>
	<link rel="stylesheet" href="/stylesheets/normalize.css">
	<link rel="stylesheet" href="/stylesheets/foundation.min.css">
	<link rel="stylesheet" href="/fonts/font.css">
	<link rel="stylesheet" href="/stylesheets/cpteidb.css">
	<link rel="stylesheet" href="/stylesheets/bdks.css">
	<link rel="stylesheet" href="/stylesheets/ionicons.min.css">
</head>
<body>
	<div class="container">
		@if(Auth::check())
		<div class="header">
			<div class="row">
				<div class="medium-4 columns">
					<div class="spinner" id="headerSpinner"></div>
					<a href="/" class="brand">Cou<span>pon</span><span>Trade</span></a>
				</div>
				<div class="medium-8 columns">
					<ul class="inline-list">
						<li>{{Auth::user()->naam}}</li>
						<li><a href="/uitloggen">Uitloggen</a></li>
					</ul>
				</div>
			</div>
		</div>
		@endif
		@yield('page')
	</div>
	<div class="footer">
		<a style="text-transform:none;">Voor vragen of opmerkingen kunt u contact opnemen met uw account manager.</a>
	</div>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script src="/js/vendor/modernizr.js"></script>
	<script src="/js/showdown.js"></script>
	<script src="/js/bdks.js"></script>
	@yield('scripts')
</body>
</html>