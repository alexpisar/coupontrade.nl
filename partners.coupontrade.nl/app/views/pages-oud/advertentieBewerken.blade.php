@extends('layouts.main')

@section('page')
	<div class="page">
		<div class="hero">
			<div class="row">
				<div class="medium-10 medium-offset-1 columns text-center">
					<h1>Advertentie bewerken</h1>
				</div>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="medium-10 medium-offset-1">
				@if(Session::has('success'))
					<div data-alert class="alert-box success radius">
						{{Session::get('success')}}
					</div>
				@endif
				@if(Session::has('whoops') || Session::has('fouten'))
					<div data-alert class="alert-box alert radius">
					  	@if(Session::has('whoops'))
							{{Session::get('whoops')}}
					  	@endif
						@if(Session::has('fouten'))							
						<?php $fouten = []; ?>
						@foreach(Session::get('fouten') as $error)
						<?php $fouten[] = $error; ?>
						@endforeach
						<?php $foten = array_unique($fouten) ?>
						@foreach($foten as $fout)
							{{$fout}}<br>
						@endforeach
						@endif
					  <a href="#" class="close">&times;</a>
					</div>
				@endif
			</div>
		</div>
		<div class="row">
			<div class="medium-10 medium-offset-1 columns well" id="stap2">
				{{Form::open(['url' => '/advertentie/' . $ad->advertentieId . '-' . $ad->titelUrl . '/bewerken'])}}
					<div class="row">
						<div class="medium-3 columns">
							<label class="left inline">Advertentie titel</label>
						</div>
						<div class="medium-9 columns">
							{{Form::text('titel', $ad->titel)}}
						</div>
					</div>
					<div class="row">
						<div class="medium-3 columns">
							<label class="left inline">Omschrijving</label>
						</div>
						<div class="medium-9 columns">
							{{Form::textarea('omschrijving', str_replace('<br />', "\n", $ad->omschrijving))}}
						</div>
					</div>
					<div class="row">
						<div class="medium-3 columns">
							<label class="left inline">Geldigheid</label>
						</div>
						<div class="medium-9 columns">
							<div class="left" style="margin-right:25px;"><label>Vanaf (dd-mm-jjjj): {{Form::text('geldig-vanaf', date('d-m-Y', strtotime($ad->geldigVanaf)))}}</label></div>
							<div class="left"><label>Tot (dd-mm-jjjj): {{Form::text('geldig-tot', date('d-m-Y', strtotime($ad->geldigTot)))}}</label></div>
						</div>
					</div>
					<br>
					<div class="row">
						<div class="medium-3 columns">
							<label>Prijs en waarde</label>
						</div>
						<div class="medium-9 columns">
							<div class="left">
								<label>Verkoopprijs</label>
								<h1><span>&euro;</span> {{ Form::number('prijs', $ad->prijs, ['placeholder' => '0', 'min' => '0', 'id' => 'prijs']) }}</h1>
							</div>
							<div class="left mlt">
								<label>Ter waarde van</label>
								<h1><span>&euro;</span> {{ Form::number('twv', $ad->twv, ['placeholder' => '0', 'min' => '0', 'id' => 'twv']) }}</h1>
							</div>					
						</div>
					</div>
					<hr>
					<div class="row">
						<div class="medium-9 medium-offset-3 columns">
							{{Form::submit('Wijzigingen opslaan', ['class' => 'button button-green radius'])}}
				{{Form::close()}}
							<div class="right">
								{{Form::open(['url' => '/advertentie/' . $ad->advertentieId . '-' . $ad->titelUrl . '/verwijderen'])}}
									{{Form::submit('Advertentie verwijderen', ['class' => 'button button-red radius'])}}
								{{Form::close()}}
							</div>
						</div>
					</div>
			</div>
		</div>
	</div>
@stop