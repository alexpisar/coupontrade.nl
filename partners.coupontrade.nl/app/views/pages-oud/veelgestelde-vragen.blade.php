@extends('layouts.main')

@section('page')
	<div class="page">
		<div class="hero">
			<div class="row">
				<div class="medium-10 medium-offset-1 columns">
					<h1>Veelgestelde vragen</h1>
				</div>
			</div>
		</div>
		<div class="row padding faq">
			<div class="medium-10 medium-offset-1 columns well">
                <h1>Geen antwoord op uw vraag?</h1>
                <p>
                    Stuur een mailtje met jouw vraag naar <a href="mailto:info@coupontrade.nl">info@coupontrade.nl</a>. Op werkdagen reageren wij binnen 1 - 3 uur.
                </p>
                <br>
                <h2>Algemene vragen</h2>
                <h3>Waarom zou ik CouponTrade gebruiken om mijn e-ticket, veiling of coupon te verkopen?</h3>
                <p>
                    Wat jij wilt dat hebben wij, wat jij niet meer wilt dat kan je bij ons kwijt. Bij Coupontrade kan je 24/7 je E-Tickets, Coupons en Veilingen kopen of verkopen. Geen onderhandelingen, telefoontjes of langdradige gesprekken, maar gewoon veilig en snel verkopen voor je gewenste prijs. Wij van Coupontrade staan daarbij altijd voor je klaar en per e-mail of telefoon zijn we altijd bereikbaar.
                </p>
                <h3>Hoeveel kost het gebruik van CouponTrade?</h3>
                <p>
                    Voor het plaatsen van een advertentie betaal je niks! CouponTrade brengt pas kosten inrekening wanneer het gelukt is om je coupon, e-ticket of veiling te kopen of verkopen. Het kost je dan een klein percentage van het verkoopprijs: 5%. Als het gaat om een Coupon van &euro;20, ontvangt de verkoper &euro;19. Ook de koper betaalt 5%, dus die rekent hier &euro;21(inclusief 5% transactiekosten) af.
                </p>
                <h3>Kan ik zien wat ik gekocht of verkocht heb?</h3>
                <p>
                    Je kan terugvinden wat je gekocht en verkocht hebt als je inlogt en op je naam rechtsboven in beeld klikt. Je krijgt dan een overzicht te zien met al aankopen en aangeboden advertenties.
                </p>
                <br>
                <h2>Kopen</h2>
                <h3>Is het kopen via CouponTrade veilig?</h3>
                <p>
                    Uiteraard,onze werkwijze maakt het verkopen en kopen van coupons, e-tickets en veilingen op een veilige wijze mogelijk. Wij controleren de tickets met een geautomatiseerde barcodecheck om te voorkomen dat coupons, e-tickets of veilingen worden verkocht. Daarnaast controleren wij alle bankgegevens en of er meldingen zijn van internetoplichting. Om de kopers zekerheid te bieden krijgen zij inzicht in de onderstaande gegevens van de koper:
                    <ul>
                        <li>naam</li>
                        <li>woonplaats</li>
                        <li>een profiel foto</li>
                    </ul>
                </p>
                <h3>Wat zijn de kosten bij het kopen van een e-ticket, coupon of veiling op CouponTrade?</h3>
                <p>
                    Naast de verkoopprijs rekenen we een klein percentage aan bemiddelingskosten van 5% van het verkoopbedrag.
                </p>
                <h3>Welke acties worden er ondernomen wanneer er geconstateerd wordt dat mijn aankoop niet werkt?</h3>
                <p>
                    Bij constatering van een niet werkende aankoop gaan wij je in contact brengen met de verkoper en ondersteuning bieden waar het nodig is.
                </p>
                <br>
                <h2>Verkopen</h2>
                <h3>Wat voor e-tickets, coupons of veilingen kan ik verkopen via CouponTrade?</h3>
                <p>
                    Via Coupontrade kan je coupons, e-tickets en veilingen van allemaal verschillende soorten arrangementen verkopen. Van pretparken tot festivals, van theaters tot concerten, van nachtclubs tot een hotelovernachtingen, als het maar een e-ticket, coupon of veiling is in de vorm van een PDF bestand. De tickets moeten natuurlijk niet persoonsgebonden zijn, anders heeft de koper er niets aan.
                </p>
                <h3>Wat kost het verkopen via CouponTrade?</h3>
                <p>
                    Het plaatsen van een advertentie is gratis! We berekenen pas kosten als het gelukt is om je kaartje te verkopen. Het kost je dan een klein percentage van het verkoopprijs: 5%. Als je een kaartje van € 20 verkoopt, ontvang je dus € 19.
                </p>
                <h3>Is het verkopen via CouponTrade veilig?</h3>
                <p>
                    Ja! Als jij je kaartje verkoopt via Coupontrade, krijgt de koper pas zijn ticket opgestuurd als wij het geld hebben ontvangen.
                </p>
                <h3>Kan ik mijn aanbod zelf gebruiken als ik deze niet verkoop?</h3>
                <p>
                    Dit is mogelijk, maar het is belangrijk dat je het ticket dan wel van Coupontrade verwijderd vóórdat je hem gebruikt.
                </p>
                <h3>Hoe weet ik dat mijn aanbod verkocht is?</h3>
                <p>
                    Er zijn verschillende manieren om er achter te komen of en welk arrangement verkocht is. Je krijgt van elke verkoop een mailtje. Je kan het ook op je profiel zien.
                </p>
			</div>
		</div>
		<div class="row">
			<div class="medium-10 medium-offset-1">
				<div class="info-box">
					<b>Help!</b>
					<br>
					Voor vragen en/of opmerkingen kun je altijd mailen naar <a href="mailto:info@coupontrade.nl">info@coupontrade.nl</a>.
				</div>
			</div>
		</div>
	</div>
@stop