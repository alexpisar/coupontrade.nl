@extends('layouts.main')

@section('page')
	<div class="hero">
		<div class="content">
			<div class="row">
				<div class="small-12 columns text-center">
					<h1>Upload jouw ongebruikte coupon, e-ticket<br />of gewonnen veiling</h1>
					<h3>
						En maak kans op een overnachting in een suite
						<br>van een van de Van der Valk hotels naar keuze.
					</h3>
					<div class="actie-buttons">
						@if(!Auth::check())
							<a href="{{Facebook::getLoginUrl()}}" class="button button-facebook radius"><span class="ion-social-facebook"></span>Log in met Facebook</a>
							<a href="/inloggen" class="button button-action radius"><span class="ion-email"></span> Of registreer met je e-mail adres</a>
						@endif
					</div>
				</div>
			</div>
		</div>
		<div class="like-button">
			<iframe src="//www.facebook.com/plugins/like.php?href=http%3A%2F%2Fwww.coupontrade.nl%2F&amp;width=225&amp;layout=button_count&amp;action=like&amp;show_faces=true&amp;share=true&amp;height=21&amp;appId=653812781393885" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:225px; height:21px;" allowTransparency="true"></iframe>
		</div>
	</div>
	<div class="padding">
		<div class="row">
			@if(count($ads) > 0)
				@foreach($ads as $bon)
					<div class="medium-4 columns end">
						<div class="well bon">
							<div class="verkoper-details">
								<h4><small>Aangeboden door</small></h4>
								<h3>{{$bon->verkoper->naam}}</h3>
								<p>in <a href="/categorie/{{$bon->categorie}}">{{ucwords(str_replace('-', ' ', $bon->categorie))}}</a></p>
							</div>
							<div class="picture" style="background:url({{$bon->verkoper->pictureUrl}}) center center / cover no-repeat;"></div>
							<div class="content">
								<h4>
									<a class="bonlink" href="/advertentie/{{$bon->advertentieId}}-{{$bon->titelUrl}}">{{ $bon->titel }}</a>
								</h4>
								<p>
									{{{ str_replace('<br />', '', strlen($bon->omschrijving) > 130 ? substr($bon->omschrijving,0,130)."..." : $bon->omschrijving) }}}
								</p>
								<div class="prijs">
									<a href="/advertentie/{{$bon->advertentieId}}-{{$bon->titelUrl}}"><button class="button button-buy radius left">Kopen</button></a>
									<h1 class="right">
										<span>&euro;</span>{{ $bon->prijs }}
									</h1>
								</div>
							</div>
						</div>
					</div>
				@endforeach
				@if(count($ads) != 12)
					<div class="medium-4 columns end">
						<a href="/advertentie/plaatsen" class="bon-plus-link">
							<div class="bon-plus">
								<i class="ion-ios-plus-outline"></i>
								<br>
								<span>Er zijn verder geen recente advertenties... Plaats de jouwe gratis!</span>
							</div>
						</a>
					</div>
				@endif
			@else
			<div class="geen-advertenties">
				<h1>
					<i class="ion-sad-outline"></i>
				</h1>
				<h4>
					Helaas... We zijn uitverkocht!
				</h4>
				<p>
					Coupon, e-ticket of een gewonnen veiling te koop? <a href="/advertentie/plaasen">Plaats gratis een advertentie!</a>
				</p>
			</div>
			@endif
		</div>
	</div>
	<div class="spacer"></div>
@stop