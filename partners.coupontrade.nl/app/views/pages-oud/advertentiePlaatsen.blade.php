@extends('layouts.main')

@section('page')
	<div class="page">
		<div class="hero">
			<div class="row">
				<div class="medium-10 medium-offset-1 columns text-center">
					<h1>Een advertentie plaatsen</h1>
				</div>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="medium-10 medium-offset-1">
				@if(Session::has('whoops') || Session::has('fouten'))
					<div data-alert class="alert-box alert radius">
					  	@if(Session::has('whoops'))
							{{Session::get('whoops')}}
					  	@endif
						@if(Session::has('fouten'))							
						<?php $fouten = []; ?>
						@foreach(Session::get('fouten') as $error)
						<?php $fouten[] = $error; ?>
						@endforeach
						<?php $foten = array_unique($fouten) ?>
						@foreach($foten as $fout)
							{{$fout}}<br>
						@endforeach
						@endif
					  <a href="#" class="close">&times;</a>
					</div>
				@endif
			</div>
		</div>
<!-- 		@if(!Auth::user()->facebookId)
		<div class="row">
			<div class="medium-10 medium-offset-1">
				<div class="info-box alert">
					<b>Let op!</b>
					<br>
					Je hebt je Facebook account niet gelinkt aan je CouponTrade account, of je hebt ons geen toestemming gegeven om berichten namens jou te plaatsen op Facebook. Hierdoor kun je je advertentie niet direct delen op je Facebook na het plaatsen hiervan op CouponTrade. Dit kun je natuurlijk wel zelf doen.
				</div>
			</div>
		</div>
		@endif
		<br> -->
		<div class="row">
			<div class="medium-10 medium-offset-1 well">
				<div id="stap1" class="stap">
					<h3>1. Kies het .pdf bestand</h3>
					{{Form::open(['url' => '/advertentie/plaatsen'])}}
					@if(Session::has('pdfmulti'))
						<div id="m-form" style="display:block;">
							U heeft al een .pdf bestand geuploadt
							@foreach(Session::get('pdfmulti') as $file)
								<input type="hidden" name="file[]" value="{{$file}}">
							@endforeach
						</div>	
					@elseif (Session::has('pdf'))
						<div id="m-form" style="display:block;">
							U heeft al een .pdf bestand geuploadt
							<input type="hidden" name="file" value="{{Session::get('pdf')}}">
						</div>	
					@else
					<div class="file-upload-box" id="fileUploadBox">
						<h1 class="ion-plus">
							<small><br>Kies een geldig .pdf bestand</small>
						</h1>
					</div>
					<div class="progress" style="height:2px;">
						<span class="meter" style="height:2px;width:0%;" id="meter"></span>
					</div>
					<div id="m-form"></div>
					{{Form::file('fileInput', ['class' => 'file-input', 'id' => 'fileInput'])}}
					@endif
				</div>
				<hr>
				<div id="stap2" class="stap">
					@if(Session::has('pdf') || Session::has('pdfmulti'))
					<div class="white-overlay" style="display:none;"></div>
					@else
					<div class="white-overlay"></div>
					@endif
					<h3>2. Aanvullende informatie</h3>
					<div class="row">
						<div class="medium-3 columns">
							<label class="left inline">Type advertentie</label>
						</div>
						<div class="medium-9 columns">
							{{Form::select('type', ['0' => 'Kies het type', '1' => 'Coupon', '2' => 'E-ticket', '3' => 'Veiling'])}}
						</div>
					</div>
					<div class="row">
						<div class="medium-3 columns">
							<label class="left inline">Advertentie titel</label>
						</div>
						<div class="medium-9 columns">
							{{Form::text('titel', '')}}
						</div>
					</div>
					<div class="row">
						<div class="medium-3 columns">
							<label class="left inline">Omschrijving</label>
						</div>
						<div class="medium-9 columns">
							{{Form::textarea('omschrijving', '')}}
						</div>
					</div>
					<div class="row">
						<div class="medium-3 columns">
							<label class="left inline">Categorie</label>
						</div>
						<div class="medium-9 columns">
							{{Form::select('categorie', ['0' => 'Kies een categorie', 'vakanties' => 'Vakanties', 'evenementen' => 'Evenementen', 'restaurants' => 'Restaurants', 'hotels' => 'Hotels', 'avondje-uit' => 'Avondje uit', 'dagje-weg' => 'Dagje weg', 'sport' => 'Sport', 'overig' => 'Overig'])}}
						</div>
					</div>
					<div class="row">
						<div class="medium-3 columns">
							<label class="left inline">Geldigheid</label>
						</div>
						<div class="medium-9 columns">
							<div class="left" style="margin-right:25px;"><label>Vanaf (dd-mm-jjjj): {{Form::text('geldig-vanaf')}}</label></div>
							<div class="left"><label>Tot (dd-mm-jjjj): {{Form::text('geldig-tot')}}</label></div>
						</div>
					</div>
					<br>
					<div class="row">
						<div class="medium-3 columns">
							<label>Prijs en waarde</label>
						</div>
						<div class="medium-9 columns">
								<div class="left">
									<label>Verkoopprijs</label>
									<h1><span>&euro;</span> {{ Form::number('prijs', '', ['placeholder' => '0', 'min' => '0', 'id' => 'prijs']) }}</h1>
								</div>
								<div class="left mlt">
									<label>Ter waarde van</label>
									<h1><span>&euro;</span> {{ Form::number('twv', '', ['placeholder' => '0', 'min' => '0', 'id' => 'twv']) }}</h1>
								</div>					
							</div>
					</div>
					<hr>
					<h3>3. Advertentie plaatsen</h3>
<!-- 					@if(Auth::user()->facebookId)
					<label>
						<input type="checkbox" name="postopfacebook"> Plaats een bericht op mijn Facebook met daarin een link naar deze advertentie.
					</label>
					@endif -->
					<p>Bij het plaatsen van een advertentie gaat u akkoord met de <a href="/algemene-voorwaarden">algemene voorwaarden</a>.</p>
					{{Form::submit('Advertentie plaatsen', ['class' => 'button button-submit radius'])}}
				</div>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="medium-10 medium-offset-1">
				<div class="info-box">
					<b>Help!</b>
					<br>
					Voor vragen en/of opmerkingen kunt u altijd mailen naar <b><a href="mailto:info@coupontrade.nl">info@coupontrade.nl</a></b>.
				</div>
			</div>
		</div>
		<div class="spacer"></div>
	</div>
@stop

@section('scripts')
	<script>
		$(document).ready(function()
		{
			$('#fileUploadBox').click(function()
			{
				$('#fileInput').click();
			});

		    $('#fileInput').change(function() {		 
				var ext = $(this).val().split('.').pop().toLowerCase();
				if (ext != 'pdf')
				{		         
					alert('Selecteer een .pdf bestand.');
					$('#fileInput').val("");		         
				}
				else
				{
					$('#fileUploadBox').fadeOut(1000);

			        $.fn.upload = function(remote,data,successFn,progressFn)
			        {
						if(typeof data != "object")
						{
							progressFn = successFn;
							successFn = data;
						}

						return this.each(function()
						{
							if($(this)[0].files[0])
							{
								var formData = new FormData();
								formData.append($(this).attr("name"), $(this)[0].files[0]);

								// if we have post data too
								if(typeof data == "object") {
									for(var i in data) {
										formData.append(i,data[i]);
									}
								}

								// do the ajax request
								$.ajax({
									url: remote,
									type: 'POST',
									xhr: function() {
										myXhr = $.ajaxSettings.xhr();

										if(myXhr.upload && progressFn)
										{
											myXhr.upload.addEventListener('progress',function(prog) {
												var value = ~~((prog.loaded / prog.total) * 100);
												if (progressFn && typeof progressFn == "function")
												{
													progressFn(prog,value);
												}
												else if (progressFn)
												{
													$(progressFn).val(value);
												}
											}, false);
										}
										return myXhr;
									},
									data: formData,
									dataType: "json",
									cache: false,
									contentType: false,
									processData: false,
									complete : function(res)
									{
										var json;
										try {
											json = JSON.parse(res.responseText);
										} catch(e) {
											json = res.responseText;
										}
										if(successFn) successFn(json);
									}
								});
							}
			            });
			        }

					$('#fileInput').upload('/pdfcheck',
						function(success) {
							$('#fileInput').prop('disabled', true);
							setTimeout(function() {
								$('.progress').fadeOut();
								$('#m-form').fadeIn();
							}, 750);

							if (success.length > 1)
							{
								$('#m-form').prepend('<h3>Welke pagina\'s wil je verkopen?</h3>');
								var cnt = 0;
								$.each(success, function(arrayID, group) {
									cnt++;
									var nogiets = group.split('/');
									if (cnt < success.length) {
									$('#m-form').append('<label class="checkbox-inline"><input type="checkbox" name="file[]" id="inlineCheckbox" value="' + group + '"> ' + nogiets[13].split('.')[0] + '</label>');
									} else {
									$('#m-form').append('<hr><label class="checkbox-inline"><input type="checkbox" name="file" id="singleCheckbox" value="' + group + '"> Ik wil het PDF bestand als één geheel verkopen.</label>');
									}
								});
							}
							else
							{
								$('#m-form').append('De pdf die u heeft geselecteerd is succesvol geupload.');
								$('#m-form').append('<input type="hidden" name="file" id="file" value="' + success[0] + '"> ');
								setTimeout(function() {
									$('.white-overlay').fadeOut();
								}, 1000);
							}
						}, 
						function(prog, value) {
							$('#meter').css('width', value + '%');
						}
					);
		   		}
			});

			$('#m-form').on('click', '#singleCheckbox', function() {
				if ($('#singleCheckbox').prop("checked") == true) {
					$('[id=inlineCheckbox]').prop('checked', false);
					$('[id=inlineCheckbox]').prop('disabled', true);
					$('.white-overlay').fadeOut();
				}
				else
				{
					$('[id=inlineCheckbox]').prop('disabled', false);
					$('.white-overlay').fadeIn();
				}
			});
			$('#m-form').on('click', 'input[id*=inlineCheckbox]', function() {
				if ($(this).prop('checked') == true)
				{
					$('#singleCheckbox').prop('checked', false);
					$('#singleCheckbox').prop('disabled', true);
					$('.white-overlay').fadeOut();
				}
				else
				{
					$('#singleCheckbox').prop('disabled', false);
					$('.white-overlay').fadeIn();
				}
			});

		});
	</script>
@stop