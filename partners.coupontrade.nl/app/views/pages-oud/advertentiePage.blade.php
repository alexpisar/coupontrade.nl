@extends('layouts.main')

@section('page')
	<div class="page">
		<div class="hero">
			<div class="small-12 columns text-center">
				<h1>{{$ad->titel}}</h1>
			</div>
		</div>
		<div class="row padding advertentie-page">
			<div class="medium-10 medium-offset-1 columns well">
				@if(Session::has('whoops'))
					<div data-alert class="alert-box alert radius">
						{{Session::get('whoops')}}
					  <a href="#" class="close">&times;</a>
					</div>
				@endif
				<div class="verkoper-foto" style="background:url({{$ad->verkoper->pictureUrl}}) center center / cover;"></div>
				<h5>Aangeboden door <b>{{$ad->verkoper->naam}}</b></h5>
				<hr>
				<div class="row">
					<div class="medium-9 columns">
						<br>
						<span>
							<i class="ion-calendar"></i> Geldig van &nbsp;{{date('j-m-Y', strtotime($ad->geldigVanaf))}}&nbsp; tot &nbsp;{{date('j-m-Y', strtotime($ad->geldigTot))}}
						</span>
						<br><br>
						<h3>Omschrijving</h3>
						<p>{{$ad->omschrijving}}</p>
						<br>
					</div>
					@if($ad->verkocht == '0')
						<div class="medium-3 columns text-center">
							T.w.v. &euro;<b>{{$ad->twv}}</b> voor
							<div class="prijzer"><span>&euro;</span>{{$ad->prijs}}</div>
							{{Form::open(['url' => '/advertentie/' . $ad->advertentieId . '-' . $ad->titelUrl . '/betalen'])}}
							@if($ad->multi == '1')
							<span style="line-height:2.75em;">Nog {{$ad->count}} beschikbaar.</span>
							<select name="aantal">
								<option value="0">Hoeveel wil je er kopen?</option>
								@foreach(range(1, $ad->count) as $opt)
								<option value="{{$opt}}">{{$opt}}</option>
								@endforeach
							</select>
							@endif
							{{Form::submit('Betalen met iDeal', ['class' => 'button button-buys radius'])}}
							{{Form::close()}}
						</div>
					@else
						<div class="medium-3 columns text-center">
							T.w.v. &euro;<b>{{$ad->twv}}</b> voor
							<div class="prijzer"><span>&euro;</span>{{$ad->prijs}}</div>
							<button class="button button-blue radius" disabled>Dit aanbod is uitverkocht</button>
						</div>
					@endif
				</div>
			</div>
		</div>
		@if(count($meer) > 0)
		<div class="row">
			<div class="medium-10 medium-offset-1 no-side-padding">
			<hr>
			<h3>&nbsp;<small>Andere advertenties in de categorie <a href="/categorie/{{$ad->categorie}}">{{ucwords(str_replace('-', ' ', $ad->categorie))}}</a></small></h3>
				@foreach($meer as $bon)
					@if($bon->advertentieId !== $ad->advertentieId)
						<div class="medium-4 col end">
							<div class="well bon">
								<div class="verkoper-details">
									<h4><small>Aangeboden door</small></h4>
									<h3>{{$bon->verkoper->naam}}</h3>
									<p>in <a href="/categorie/{{$bon->categorie}}">{{ucwords(str_replace('-', ' ', $bon->categorie))}}</a></p>
								</div>
								<div class="picture" style="background:url({{$bon->verkoper->pictureUrl}}) center center / cover no-repeat;"></div>
								<div class="content">
									<h4>
										<a class="bonlink" href="/advertentie/{{$bon->advertentieId}}-{{$bon->titelUrl}}">{{ $bon->titel }}</a>
									</h4>
									<p>
										{{{ str_replace('<br />', '', strlen($bon->omschrijving) > 130 ? substr($bon->omschrijving,0,130)."..." : $bon->omschrijving) }}}
									</p>
									<div class="prijs">
										<a href="/advertentie/{{$bon->advertentieId}}-{{$bon->titelUrl}}"><button class="button button-buy radius left">Kopen</button></a>
										<h1 class="right">
											<span>&euro;</span>{{ $bon->prijs }}
										</h1>
									</div>
								</div>
							</div>
						</div>
					@endif
				@endforeach
				<div class="medium-4 columns end">
					<a href="/advertentie/plaatsen" class="bon-plus-link">
						<div class="bon-plus">
							<i class="ion-ios-plus-outline"></i>
							<br>
							<span>Er zijn verder geen advertenties in deze categorie... Plaats de jouwe gratis!</span>
						</div>
					</a>
				</div>
			</div>
		</div>
		@endif
	</div>
@stop