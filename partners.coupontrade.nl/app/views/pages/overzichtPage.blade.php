@extends('layouts.main')

@section('page')
	<div class="page">
		<div class="hero">
			<div class="row">
				<div class="small-12 columns text-center">
					<h1>Overzicht</h1>
				</div>
			</div>
		</div>
		<div class="row padding">
			<div class="medium-3 columns">
				<div class="well">
					<h2><b>{{Auth::user()->referentieId}}</b></h2>
					<h2><small>{{Auth::user()->naam}}</small></h2>
					Partner sinds <b>{{date('d-m-Y', strtotime(Auth::user()->createdAt))}}</b>
					<br>
					{{Auth::user()->straat}}
					<br>
					{{Auth::user()->postcode}}, {{Auth::user()->stad}}
					<br>
					<a href="{{Auth::user()->website}}" target="_blank">{{Auth::user()->website}}</a>
					<br><br>
					<h2><small>Uw account manager</small></h2>
					{{Auth::user()->manager->naam}}
					<br>
					<a href="mailto:{{Auth::user()->manager->email}}">{{Auth::user()->manager->email}}</a>
					<br>
					{{Auth::user()->manager->telefoon}}
				</div>
			</div>
			<div class="medium-9 columns">
				<div class="well">
					<div class="right">
						<input type="hidden" id="partnerId" value="{{Auth::user()->partnerId}}">
						<button class="button button-green radius" id="makeFactuurBtn">
							<div id="iconFactuurBtn">
								<span class="ion-document"></span>Factuur {{date('m-Y')}}
							</div>
							<div id="spinner" class="sk-spinner sk-spinner-wave">
								<div class="sk-rect1"></div>
								<div class="sk-rect2"></div>
								<div class="sk-rect3"></div>
								<div class="sk-rect4"></div>
								<div class="sk-rect5"></div>
							</div>
						</button>
					</div>
					<h3>Uw advertenties</h3>
					<table style="width:100%" border="1">
						<tr>
							<th width="1" class="text-center"><span class="ion-calendar"></span></th>
							<th width="100%">Titel</th>
							<th width="1" class="text-center">&euro;</th>
							<th width="1" class="text-center"><span class="ion-eye"></span></th>
							<th width="1" class="text-center"><span class="ion-ios-cart"></span></th>
							<th width="1" class="text-center">Actief</th>
							<th width="1" class="text-center">Bekijken</th>
						</tr>
						@foreach(Auth::user()->advertenties as $ad)
						<tr>
							<td class="text-center">{{date('d-m-Y', strtotime($ad->createdAt))}}</td>
							<td>{{$ad->titel}}</td>
							<td class="text-center">&euro;{{$ad->prijs}}</td>
							<td class="text-center">{{$ad->weergaven}}</td>
							<td class="text-center">{{Auth::user()->verkopen()->where('advertentieId', '=', $ad->advertentieId)->count()}}</td>
							<td class="text-center table-toggle toggleActiefPartnerAdvertentie" data-active="{{$ad->verkocht}}" data-aid="{{$ad->advertentieId}}">
								{{$ad->verwijderd == 0 ? '<h3 class="toggled"><span class="ion-toggle-filled"></span></h3>' : '<h3><span class="ion-toggle"></span></h3>'}}
							</td>
							<td class="text-center"><a href="http://www.coupontrade.nl/advertentie/{{$ad->advertentieId}}-{{$ad->titelUrl}}">Bekijken</a></td>
						</tr>
						@endforeach
					</table>
				</div>
				<br>
				<div class="well">
					<h3>Uw verkopen</h3>
					@if(count(Auth::user()->waardenummers()->get()) > 0)
					<table style="width:100%" border="1">
						<tr>
							<th width="1" class="text-center"><span class="ion-calendar"></span></th>
							<th width="1" class="text-left">Door</th>
							<th width="100%">Titel</th>
							<th width="1">Waardebonnummer</th>
							<th width="1" class="text-right">&euro;</th>
						</tr>
						@foreach(Auth::user()->waardenummers()->get() as $ad)
						<tr>
							<td class="text-center">{{date('d-m-Y', strtotime($ad->pdf->updatedAt))}}</td>
							<td class="text-left">{{$ad->koper->naam}}</td>
							<td>{{$ad->advertentie->titel}}</td>
							<td class="text-center"><b>{{$ad->waardeNummer}}</b></td>
							<td class="text-right">&euro;{{$ad->advertentie->prijs}}</td>
						</tr>
						@endforeach
					</table>
					@else
						<p>U heeft nog geen verkopen verricht.</p>
					@endif
				</div>
			</div>
		</div>
	</div>
@stop