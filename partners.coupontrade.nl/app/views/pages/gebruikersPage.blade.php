@extends('layouts.main')

@section('page')
	<div class="page">
		<div class="hero">
			<div class="row">
				<div class="small-12 columns text-center">
					<h1>Gebruikers</h1>
				</div>
			</div>
		</div>
		<div class="smallnav">
			<div class="row">
				<div class="medium-12 columns">
					<a href="/">Dashboard</a>
					<a href="/advertenties">advertenties</a>
					<a href="/verkopen">verkopen</a>
					<a href="/gebruikers">gebruikers</a>
					<a href="/partners">partners</a>
					<a href="/todo">te doen / taken</a>
				</div>
			</div>
		</div>
		<div class="row padding">
			<div class="small-12 columns">
				<div class="well">
					<h3>Gebruikers</h3>
					<table style="width:100%" border="1">
					<tr>
						<th width="1">ID</th>
						<th width="1">Naam</th>
						<th width="1">E-mail</th>
						<td width="1">Facebook ID</td>
						<th width="1">Advertenties</th>
						<th width="1">Verkopen</th>
						<th width="1">Aankopen</th>
					</tr>
					@foreach($gebruikers as $g)
					<tr>
						<td>{{$g->userId}}</td>
						<td>
							<div class="table-picture" style="background:url({{substr($g->pictureUrl, 0, 4) === 'http' ? $g->pictureUrl : 'http://www.coupontrade.nl/' . $g->pictureUrl}}) 50% 50% / cover;"></div>
							<div class="tks"><a href="/gebruiker/{{$g->userId}}">{{$g->naam}}</a></div>
						</td> 
						<td><a href="mailto:{{$g->email}}">{{$g->email}}</td>
						<td>{{ $g->facebookId == NULL ? '-' : '<a href="http://facebook.com/' . $g->facebookId . '" target="_blank">' . $g->facebookId . '</a>' }}</td>
						<td>{{$g->advertenties}}</td>
						<td>{{$g->verkopen}}</td>
						<td>{{$g->aankopen}}</td>
					</tr>
					@endforeach
					</table>
				</div>
			</div>
		</div>
	</div>
@stop