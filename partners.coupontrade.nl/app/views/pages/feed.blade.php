@extends('layouts.main')

@section('page')
	<div class="page">
		<div class="hero">
			<div class="row">
				<div class="small-12 columns text-center">
					<h1>Nieuws</h1>
				</div>
			</div>
		</div>
		<div class="smallnav">
			<div class="row">
				<div class="small-12 columns text-center">
					<a href="/">Nieuws</a>
					<a href="/overzicht">Overzicht</a>
				</div>
			</div>
		</div>
		<div class="row padding">
			<div class="medium-3 columns">
				<div class="well">
					<h2><b>{{Auth::user()->referentieId}}</b></h2>
					<h2><small>{{Auth::user()->naam}}</small></h2>
					Partner sinds <b>{{date('d-m-Y', strtotime(Auth::user()->createdAt))}}</b>
					<br>
					{{Auth::user()->straat}}
					<br>
					{{Auth::user()->postcode}}, {{Auth::user()->stad}}
					<br>
					<a href="{{Auth::user()->website}}" target="_blank">{{Auth::user()->website}}</a>
					<br><br>
					<h2><small>Uw account manager</small></h2>
					{{Auth::user()->manager->naam}}
					<br>
					<a href="mailto:{{Auth::user()->manager->email}}">{{Auth::user()->manager->email}}</a>
					<br>
					{{Auth::user()->manager->telefoon}}
				</div>
			</div>
			<div class="medium-9 columns">
				@foreach($nieuws as $n)
				<div class="well" style="margin-bottom: 10px;">
					@if(isset($n->content))
						{{$n->content}}
					@else
						<p><b>{{$n->koper->naam}}</b> heeft <b>'{{$n->advertentie->titel}}'</b> gekocht.</p>
					@endif
				</div>
				@endforeach
			</div>
		</div>
	</div>
@stop