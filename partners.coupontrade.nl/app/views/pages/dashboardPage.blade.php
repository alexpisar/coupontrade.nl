@extends('layouts.main')

@section('page')
	<div class="page">
		<div class="hero">
			<div class="row">
				<div class="medium-12 columns text-center">
					<h1>Dashboard</h1>
				</div>
			</div>
		</div>
		<div class="smallnav">
			<div class="row">
				<div class="medium-12 columns">
					<a href="/">Dashboard</a>
					<a href="/advertenties">advertenties</a>
					<a href="/verkopen">verkopen</a>
					<a href="/gebruikers">gebruikers</a>
					<a href="/partners">partners</a>
					<a href="/todo">te doen / taken</a>
				</div>
			</div>
		</div>		

		@if (Session::has('success'))
		<div class="row padding">
			<div class="small-12 columns">
				<div class="alert-box success">
					{{Session::get('success')}}
				</div>
			</div>
		</div>
		@endif

		<!-- Boxes -->
		@if (Session::has('open-box') && Session::get('open-box') == 'gemisteAdvertentieBox')
		<div class="boxer open" id="gemisteAdvertentieBox">
		@else
		<div class="boxer" id="gemisteAdvertentieBox">
		@endif
			<div class="row padding">
				<div class="medium-10 medium-offset-1 columns">
					<div class="well">
						<h3>Gemiste advertentie toevoegen</h3>
						<hr>
						{{Form::open(['url' => '/toevoegen/advertentie-gemist'])}}
						<div class="row">
							<div class="medium-3 columns">
								<b>Advertentie titel</b>
							</div>
							<div class="medium-9 columns">
								{{Form::text('titel')}}
							</div>
						</div>
						<div class="row">
							<div class="medium-3 columns">
								<b>Korte omschrijving</b>
							</div>
							<div class="medium-9 columns">
								{{Form::textarea('omschrijving')}}
							</div>
						</div>
						<div class="row">
							<div class="medium-3 columns">
								<b>Verloop datum (dd-mm-jjjj)</b>
							</div>
							<div class="medium-9 columns">
								{{Form::text('date')}}
							</div>
						</div>
						<div class="row">
							<div class="medium-3 columns">
								<b>Prijs in hele euro's</b>
							</div>
							<div class="medium-9 columns">
								{{Form::number('prijs')}}
							</div>
						</div>
						<div class="row">
							<div class="medium-3 columns">&nbsp;</div>
							<div class="medium-9 columns">
								{{Form::submit('Plaatsen', ['class' => 'button button-green radius'])}}
								@if (Session::has('whoopsGemist'))
								<p class="text-alert">{{Session::get('whoopsGemist')}}</p>
								@endif
							</div>
						</div>
						{{Form::close()}}
					</div>
				</div>
			</div>
		</div>
		
		@if (Session::has('open-box') && Session::get('open-box') == 'partnerToevoegenBox')
		<div class="boxer open" id="partnerToevoegenBox">
		@else
		<div class="boxer" id="partnerToevoegenBox">
		@endif
			<div class="row padding">
				<div class="medium-10 medium-offset-1 columns">
					<div class="well">
						<h3>Nieuwe partner toevoegen</h3>
						<hr>
						@if (Session::has('whoopsPartner'))
						<p class="text-alert">{{Session::get('whoopsPartner')}}</p>
						@endif
						{{Form::open(['url' => '/toevoegen/partner'])}}
						<div class="row">
							<div class="medium-3 columns">
								<b>Naam (bedrijf)</b>
							</div>
							<div class="medium-9 columns">
								{{Form::text('naam', '', ['placeholder' => 'bijv. "CouponTrade"'])}}
							</div>
						</div>
						<div class="row">
							<div class="medium-3 columns">
								<b>Contactpersoon</b>
							</div>
							<div class="medium-9 columns">
								{{Form::text('contactpersoon', '', ['placeholder' => 'bijv. "Achmed Achternaam"'])}}
							</div>
						</div>
						<div class="row">
							<div class="medium-3 columns">
								<b>E-mail</b>
							</div>
							<div class="medium-9 columns">
								{{Form::text('email')}}
							</div>
						</div>
						<div class="row">
							<div class="medium-3 columns">
								<b>Telefoon</b>
							</div>
							<div class="medium-9 columns">
								{{Form::text('telefoon')}}
							</div>
						</div>
						<div class="row">
							<div class="medium-3 columns">
								<b>Website</b>
							</div>
							<div class="medium-9 columns">
								{{Form::text('website', '', ['placeholder' => 'bijv. "http://www.coupontrade.nl/"'])}}
							</div>
						</div>
						<div class="row">
							<div class="medium-3 columns">
								&nbsp;
							</div>
							<div class="medium-9 columns">
								{{Form::submit('Partner toevoegen', ['class' => 'button button-green radius'])}}
							</div>
						</div>
						{{Form::close()}}
					</div>
				</div>
			</div>
		</div>

		<div class="row padding">
			<div class="medium-4 columns">
				<button class="button button-blue radius" id="gemisteAdvertentieBtn"><span class="ion-plus"> &nbsp; Gemiste advertentie</span></button>
			</div>
			<div class="medium-4 columns">
				<button class="button button-blue radius"><span class="ion-heart-broken"> &nbsp; Uitbetalen</span></button>
			</div>
			<div class="medium-4 columns">
				<button class="button button-blue radius" id="partnerToevoegenBtn"><span class="ion-person-add"> &nbsp; Partner</span></button>
			</div>
		</div>

		<!-- Activity table -->
		<div class="row">
			<div class="small-12 columns">
				<div class="well">
					<b>Recente advertenties</b>
					<table style="width:100%" border="1">
					<tr>
						<th width="1">ID</th>
						<th width="1">Titel</th>
						<th width="1">Verkoper</th>
						<th width="1">Prijs</th>
						<th width="1">Winst</th>
						<th width="1">Verkocht</th>
						<th width="1">Gecontroleerd</th>
						<th width="1">Door</th>
					</tr>
					@foreach($ads as $ad)
					<tr>
						<td>{{$ad->advertentieId}}</td>
						<td><a href="http://www.coupontrade.nl/advertentie/{{$ad->advertentieId}}-{{$ad->titelUrl}}">{{$ad->titel}}</a></td> 
						@if($ad->type == 5)
						<td><a href="/partner/{{$ad->partner->referentieId}}">{{$ad->partner->naam}}</a></td>
						@else
						<td><a href="/gebruiker/{{$ad->verkoper->userId}}">{{$ad->verkoper->naam}}</a></td>
						@endif
						<td>&euro;{{$ad->prijs}}</td>
						<td>&euro;{{$ad->prijs * 0.1}}</td>
						<td>{{$ad->verkocht == 1 ? 'Ja' : 'Nee'}}</td>
						<td>{{$ad->gecontroleerd == 1 ? 'Ja' : 'Nee'}}</td>
						<td>{{$ad->gecontroleerdDoor != NULL ? $ad->gecontroleerdDoor : '-'}}</td>
					</tr>
					@endforeach
					</table>
				</div>
			</div>
		</div>



<!-- 			<div class="row">
				<div class="large-4 medium-6 columns">
					<div class="well-dashed">
						<div class="content">
							<span class="ion-plus"></span>
							<br><br><br>
							Plaats nieuwe gemiste advertentie
						</div>
					</div>
				</div>
			</div> -->
	</div>
@stop