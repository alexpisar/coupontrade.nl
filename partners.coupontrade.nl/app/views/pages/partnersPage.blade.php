@extends('layouts.main')

@section('page')
	<div class="page">
		<div class="hero">
			<div class="row">
				<div class="small-12 columns text-center">
					<h1>Partners</h1>
				</div>
			</div>
		</div>
		<div class="smallnav">
			<div class="row">
				<div class="medium-12 columns">
					<a href="/">Dashboard</a>
					<a href="/advertenties">advertenties</a>
					<a href="/verkopen">verkopen</a>
					<a href="/gebruikers">gebruikers</a>
					<a href="/partners">partners</a>
					<a href="/todo">te doen / taken</a>
				</div>
			</div>
		</div>
		@if (Session::has('success'))
		<div class="row padding">
			<div class="small-12 columns">
				<div class="alert-box success">
					{{Session::get('success')}}
				</div>
			</div>
		</div>
		@endif
		@if (Session::has('open-box') && Session::get('open-box') == 'partnerToevoegenBox')
		<div class="boxer open" id="partnerToevoegenBox">
		@else
		<div class="boxer" id="partnerToevoegenBox">
		@endif
			<div class="row padding">
				<div class="medium-12 columns">
					<div class="well">
						<h3>Nieuwe partner toevoegen</h3>
						<hr>
						@if (Session::has('whoopsPartner'))
						<p class="text-alert">{{Session::get('whoopsPartner')}}</p>
						@endif
						{{Form::open(['url' => '/toevoegen/partner'])}}
						<div class="row">
							<div class="medium-3 columns">
								<b>Naam (bedrijf)</b>
							</div>
							<div class="medium-9 columns">
								{{Form::text('naam', '', ['placeholder' => 'bijv. "CouponTrade"'])}}
							</div>
						</div>
						<div class="row">
							<div class="medium-3 columns">
								<b>Adres</b>
							</div>
							<div class="medium-9 columns">
								{{Form::text('adres', '', ['placeholder' => 'Straatnaam + huisnr'])}}
							</div>
						</div>
						<div class="row">
							<div class="medium-3 columns">
								<b>Postcode</b>
							</div>
							<div class="medium-9 columns">
								{{Form::text('postcode')}}
							</div>
						</div>
						<div class="row">
							<div class="medium-3 columns">
								<b>Stad</b>
							</div>
							<div class="medium-9 columns">
								{{Form::text('stad')}}
							</div>
						</div>
						<div class="row">
							<div class="medium-3 columns">
								<b>BTW-nummer</b>
							</div>
							<div class="medium-9 columns">
								{{Form::text('btwnummer')}}
							</div>
						</div>
						<div class="row">
							<div class="medium-3 columns">
								<b>Contactpersoon</b>
							</div>
							<div class="medium-9 columns">
								{{Form::text('contactpersoon', '', ['placeholder' => 'bijv. "Achmed Achternaam"'])}}
							</div>
						</div>
						<div class="row">
							<div class="medium-3 columns">
								<b>E-mail</b>
							</div>
							<div class="medium-9 columns">
								{{Form::text('email')}}
							</div>
						</div>
						<div class="row">
							<div class="medium-3 columns">
								<b>Telefoon</b>
							</div>
							<div class="medium-9 columns">
								{{Form::text('telefoon')}}
							</div>
						</div>
						<div class="row">
							<div class="medium-3 columns">
								<b>Website</b>
							</div>
							<div class="medium-9 columns">
								{{Form::text('website', '', ['placeholder' => 'bijv. "http://www.coupontrade.nl/"'])}}
							</div>
						</div>
						<div class="row">
							<div class="medium-3 columns">
								&nbsp;
							</div>
							<div class="medium-9 columns">
								{{Form::submit('Partner toevoegen', ['class' => 'button button-green radius'])}}
							</div>
						</div>
						{{Form::close()}}
					</div>
				</div>
			</div>
		</div>
		<div class="row padding">
			<div class="small-12 columns">
				<div class="well">
					<div class="left">
						<h3>Partners</h3>
					</div>
					<div class="right">
						<button class="button button-blue radius" id="partnerToevoegenBtn"><span class="ion-person-add"> &nbsp; Partner toevoegen</span></button>
					</div>
					<table style="width:100%" border="1">
					<tr>
						<th width="1">ID</th>
						<th width="1">Referentie</th>
						<th width="1">Naam</th>
						<th width="1">Contactpersoon</th>
						<th width="1">E-mail</th>
						<th width="1">Telefoon</th>
						<th width="1">Website</th>
						<th width="1">Sinds</th>
						<th width="1">Door</th>
					</tr>
					@foreach($partners as $g)
					<tr>
						<td>{{$g->partnerId}}</td>
						<td><a href="/partner/{{$g->referentieId}}">{{$g->referentieId}}</a></td>
						<td>{{$g->naam}}</td>
						<td>{{$g->contactpersoon}}</td>
						<td><a href="mailto:{{$g->email}}">{{$g->email}}</td>
						<td>{{$g->telefoon}}</td>
						<td><a href="{{$g->website}}" target="_blank">{{$g->website}}</a></td>
						<td>{{date('d-m-Y', strtotime($g->createdAt))}}</td>
						<td>{{$g->manager->username}}</td>
					</tr>
					@endforeach
					</table>
				</div>
			</div>
		</div>
	</div>
	</div>
@stop