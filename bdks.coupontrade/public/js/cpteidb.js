$(document).ready(function() {
	var heroHeight = $('.page .hero').height();
	var docuHeight = $(document).height();
	if (heroHeight)
	{
		$(window).scroll(function()
		{
			var distance = $(window).scrollTop();
			if (distance > heroHeight - 50)
			{
				$('.header').addClass('nono');
			}
			else
			{
				$('.header').removeClass('nono');
			}
		});
	}
	else
	{
		$('.header').addClass('nono');
	}
	$('.footer').css('top', docuHeight);
	$('#toggleSidebar').click(function() {
		$(this).toggleClass('ion-close');
		$('.sidebar').toggleClass('open');
		$('.container').toggleClass('open');
		$('.header').toggleClass('open');
	});
	// $('a').click(function() {
	// 	$('#headerSpinner').show();
	// 	$('.brand').hide();
	// });
	$('#categorieBtn').click(function(e) {
		console.log(e)
	});
});