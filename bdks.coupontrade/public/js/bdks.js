$(document).ready(function() {
	var heroHeight = $('.hero').height();
	var docuHeight = $(document).height();

	if (heroHeight)
	{
		$(window).scroll(function()
		{
			var distance = $(window).scrollTop();
			if (distance > heroHeight - 50)
			{
				$('.header').addClass('nono');
			}
			else
			{
				$('.header').removeClass('nono');
			}
		});
	}
	else
	{
		$('.header').addClass('nono');
	}

	$('#gemisteAdvertentieBtn').click(function() {
		$('.boxer').removeClass('open');
		$('#gemisteAdvertentieBox').toggleClass('open');
	});

	$('#partnerToevoegenBtn').click(function() {
		$('.boxer').removeClass('open');
		$('#partnerToevoegenBox').toggleClass('open');
	});

	$('#nieuwsBerichtBtn').click(function() {
		$('.boxer').removeClass('open');
		$('#nieuwsBerichtBox').toggleClass('open');
	});

	$('#partnerAdBtn').click(function() {
		$('#partnerAdBox').toggle();
	});

	$('.toggleActiefPartnerAdvertentie').click(function() {
		var status 	= $(this).data('active');
		var id 		= $(this).data('aid');

		if (status == 0)
		{
			$(this).html('<h3><span class="ion-toggle"></span></h3>');
			$(this).data('active', 1);
		}
		else
		{
			$(this).html('<h3 class="toggled"><span class="ion-toggle-filled"></span></h3>');
			$(this).data('active', 0);
		}

		$.get('/advertentie/' + id + '/toggle-actief', function(data, status) {
			alert(data);
		});
	});

	$('#makeFactuurBtn').click(function() {
		$('#iconFactuurBtn').hide();
		$('#spinner').show();
		var id = $('#partnerId').val();
		setTimeout(function() {
			window.location.href = '/partner/' + id + '/make/maand-factuur';
		}, 500);
	});

});