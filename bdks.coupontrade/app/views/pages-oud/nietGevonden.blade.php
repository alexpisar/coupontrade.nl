@extends('layouts.main')

@section('page')
	<div class="page">
		<div class="hero">
			<div class="small-12 columns text-center">
				<h1>CouponTrade</h1>
			</div>
		</div>
		<div class="row padding">
			<div class="medium-10 medium-offset-1 text-center">
				<div class="geen-advertenties">
					<h1>
						<i class="ion-sad-outline"></i>
					</h1>
					<h4>
						Oeps...
					</h4>
					<p>
						{{$whoops}}
					</p>
				</div>
			</div>
		</div>
	</div>
@stop