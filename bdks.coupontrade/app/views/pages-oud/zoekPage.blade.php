@extends('layouts.main')

@section('page')
	<div class="page">
		<div class="hero">
			<div class="row">
				<div class="medium-8 medium-offset-2 columns text-center">
					{{Form::open(['url' => '/zoeken'])}}
					<input type="search" placeholder="Zoeken..." class="radius" name="zoekterm">
					<button class="button button-submit radius" style="padding:1em 2.5em;">Zoeken</button>
					{{Form::close()}}
				</div>
			</div>
		</div>
		<div class="row padding">
			<div class="medium-10 medium-offset-1 columns">
				@if(Session::has('whoops'))
				<div data-alert class="alert-box alert radius">
					{{Session::get('whoops')}}
				</div>
				@endif
				@if(!Session::has('ads'))
				<div class="geen-advertenties">
					<h1>
						<i class="ion-search"></i>
					</h1>
					<h4>
						Geen resultaten
					</h4>
					<p>
						Typ een of meer zoektermen in het veld bovenin.
					</p>
				</div>
				@else
					@if(count(Session::get('ads')) > 0)
					@foreach(Session::get('ads') as $bon)
					<div class="medium-6 columns end">
						<div class="well bon">
							<div class="verkoper-details">
								<h4><small>Aangeboden door</small></h4>
								<h3>{{$bon->naam}}</h3>
								<p>in <a href="/categorie/{{$bon->categorie}}">{{ucwords(str_replace('-', ' ', $bon->categorie))}}</a></p>
							</div>
							<div class="picture" style="background:url({{$bon->pictureUrl}}) center center / cover no-repeat;"></div>
							<div class="content">
								<h4>
									<a class="bonlink" href="/advertentie/{{$bon->advertentieId}}-{{$bon->titelUrl}}">{{ $bon->titel }}</a>
								</h4>
								<p>
									{{{ str_replace('<br />', '', strlen($bon->omschrijving) > 130 ? substr($bon->omschrijving,0,130)."..." : $bon->omschrijving) }}}
								</p>
								<div class="prijs">
									<a href="/advertentie/{{$bon->advertentieId}}-{{$bon->titelUrl}}"><button class="button button-buy radius left">Kopen</button></a>
									<h1 class="right">
										<span>&euro;</span>{{ $bon->prijs }}
									</h1>
								</div>
							</div>
						</div>
					</div>
					@endforeach
					<div class="medium-6 columns end">
						<a href="/advertentie/plaatsen" class="bon-plus-link">
							<div class="bon-plus">
								<i class="ion-ios-plus-outline"></i>
								<br>
								<span>Er zijn verder geen advertenties gevonden... Plaats de jouwe gratis!</span>
							</div>
						</a>
					</div>
					@else
					<div class="geen-advertenties">
						<h1>
							<i class="ion-sad-outline"></i>
						</h1>
						<h4>
							Helaas, er zijn geen resultaten gevonden
						</h4>
						<p>
							Wij konden geen advertenties vinden gerelateerd aan de door jouw ingevoerde zoekterm(en).
						</p>
					</div>
					@endif
				@endif
			</div>
		</div>
	</div>
@stop