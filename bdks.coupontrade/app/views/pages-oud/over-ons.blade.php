@extends('layouts.main')

@section('page')
	<div class="page">
		<div class="hero">
			<div class="row">
				<div class="medium-10 medium-offset-1 columns">
					<h1>Over ons</h1>
				</div>
			</div>
		</div>
		<div class="row padding">
			<div class="medium-10 medium-offset-1 columns well">
                <center><img src="/img/logo.png" alt="CouponTrade logo" height="200px"></center>
                <br>
                <h1>Over ons</h1>
                <p>
                    Wat jij wilt, dat hebben wij. Wat jij niet meer wilt, dat kan je bij ons kwijt! Bij Coupontrade kan je 24/7 je e-Tickets, coupons en veilingen kopen of verkopen. Geen onderhandelingen, telefoontjes of langdradige gesprekken, maar gewoon veilig en snel verkopen voor je gewenste prijs. Wij van Coupontrade staan daarbij altijd voor je klaar en per e-mail of telefoon zijn we altijd bereikbaar. Voor vragen of opmerkingen mail je naar info@coupontrade.nl en onze collega’s helpen je zo snel mogelijk verder.
                </p>
                <br>
                <h1>Innovatief en dynamisch bedrijf</h1>
                <p>
                    Sinds de oprichting van Coupontrade heeft innovatie & creativiteit een belangrijke rol gespeeld. Door goed te luisteren naar wat de consumenten nodig hebben en constant in te spelen op de veranderende markt, is er een innovatief concept neergezet. De verkoop van e-tickets, coupons en veilingen zijn tegenwoordig niet meer weg te denken van het internet. Coupontrade speelt hier op in door de kopers en verkopers aan te bieden waar zij naar op zoek zijn zonder enige moeite te doen.
                </p>
                <br>
                <h1>Integriteit</h1>
                <p>
                    Ons werk moet voldoen aan strenge kwaliteitseisen, en waar dit binnen ons vermogen ligt zullen we onze verplichtingen tegenover klanten en personeel altijd nakomen. Respect en vertrouwen zijn kernwaarden binnen ons bedrijf. We werken volgens zeer strenge ethische normen, waarbij alle wet- en regelgeving wordt nageleefd. Je gegevens zijn bij Coupontrade ten alle tijden veilig.
                </p>
			</div>
		</div>
		<div class="row">
			<div class="medium-10 medium-offset-1">
				<div class="info-box">
					<b>Help!</b>
					<br>
					Voor vragen en/of opmerkingen kun je altijd mailen naar <a href="mailto:info@coupontrade.nl">info@coupontrade.nl</a>.
				</div>
			</div>
		</div>
	</div>
@stop