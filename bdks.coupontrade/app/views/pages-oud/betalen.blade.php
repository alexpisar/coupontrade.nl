@extends('layouts.main')

@section('page')
	<div class="page">
		<div class="hero">
			<div class="row">
				<div class="small-12 columns text-center">
					<h1>Betalen met iDeal</h1>
				</div>
			</div>
		</div>
		<div class="row padding">
			<div class="medium-6 medium-offset-3 columns well">
				{{Form::open(['url' => '/advertentie/' . $ad->advertentieId . '-' . $ad->titelUrl . '/betalen/transactie'])}}
					<div class="left">
						<b>Verkoper</b><br>
						<b>Titel</b><br>
						<b>Aantal</b><br>
						<b>Voor de prijs van</b>
					</div>
					<div class="right text-right">
						{{$ad->verkoper->naam}}<br>
						{{$ad->titel}}<br>
						@if(Session::has('aantal'))
						{{Session::get('aantal')}}<br>
						<h1 class="prijzer"><span>&euro;</span>{{$ad->prijs * Session::get('aantal')}}</h1>
						@else
						1<br>
						<h1 class="prijzer"><span>&euro;</span>{{$ad->prijs}}</h1>
						@endif
					</div>
					<hr>
					<p>Kies uw bank:</p>
					<select name="bank">
						<option value="0">Kies je bank...</option>
						@foreach($banken as $bank)
						<option value="{{$bank['Id']}}">{{$bank['Name']}}</option>
						@endforeach
					</select>
					{{Form::submit('Betalen met iDeal', ['class' => 'button button-blue radius'])}}
				{{Form::close()}}
			</div>
		</div>
	</div>
@stop