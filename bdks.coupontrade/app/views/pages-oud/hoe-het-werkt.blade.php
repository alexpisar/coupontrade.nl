@extends('layouts.main')

@section('page')
	<div class="page">
		<div class="hero">
			<div class="row">
				<div class="medium-10 medium-offset-1 columns">
					<h1>Hoe het werkt</h1>
				</div>
			</div>
		</div>
		<div class="row padding">
			<div class="medium-10 medium-offset-1 columns well">
                <h1>Algemene uitleg</h1>
                <p>
                    CouponTrade maakt het mogelijk om op een veilige, makkelijke en snelle wijze digitale coupons, e-tickets en veilingen te kopen en verkopen. Wij doen dit door de kopers snel en veilig te laten betalen met iDEAL. Na de betaling ontvangt de koper meteen zijn coupon, e-ticket of veiling. Daaropvolgend ontvangen de verkopers hun geld binnen 24 tot 48 uur.
                </p>
                <br>
                <h1>Uitleg voor verkopers</h1>
                <p>
                    CouponTrade zorgt ervoor dat de verkoper bij de verkoop altijd zijn geld ontvangt. Het gebruik van het betaalsysteem iDEAL zorgt ervoor dat de koper alleen een coupon, e-ticket of veiling ontvangt wanneer de betaling succesvol is uitgevoerd. CouponTrade maakt het mogelijk om je coupons, e-tickets of veilingen op een veilige, snelle en gemakkelijke manier te verkopen.
                </p>
                <br>
                <h2>Hoe werkt CouponTrade?</h2>
                <p>
                    Om je coupon, veiling of e-ticket te verkopen moet je de onderstaande stappen doorlopen:
                    <ul>
                        <li>Het uploaden van een .pdf bestand</li>
                        <li>Het bepalen van de verkoopprijs en het invullen van verplichte informatie</li>
                        <li>Het kiezen van een promotiemethode</li>
                    </ul>
                    Bij de verkoop van je coupon, e-ticket of veiling ontvang je direct een emailnotificatie waarin wordt vermeld dat je coupon, e-ticket of veiling verkocht is en het bedrag binnen 1-2 werkdagen(afhankelijk van de bank waarbij je bent aangesloten) naar je wordt overgemaakt.
                </p>
                <br>
                <h2>Voordelen</h2>
                <p>
                    Het verkopen van je coupon, e-ticket of veiling via CouponTrade brengt de volgende voordelen met zich mee:
                    <ul>
                        <li>Garantie: je ontvangt altijd je geld</li>
                        <li>Simpel: je zet je advertentie online in 3 simpele stappen</li>
                        <li>Snel: wij bieden ondersteuning bij het promoten van je advertentie</li>
                    </ul>
                </p>
                <br>
                <h2>Geldgarantie</h2>
                <p>
                    CouponTrade weet hoe belangrijk het voor de verkoper is om zijn geld te onvangen. Daarom garandeert CouponTrade dat de verkoper altijd zijn geld ontvangt.
                </p>
                <br>
                <h2>Promotie van je advertentie</h2>
                <p>
                    De verkoper krijgt de optie om te bepalen op welke plaatsen de advertentie geplaatst wordt. Een voorbeeld hiervan is de advertentie automatisch laten plaatsen op je eigen Facebook.
                </p>
                <br>
                <h2>Met weinig inspanning</h2>
                <p>
                    CouponTrade zorgt met de volgende werkzije dat op elk gewenst moment een coupon, e-ticket of veiling gekocht kan worden. De verkoper upload het pdf bestand met de bijbehorende informatie. De opgegeven informatie zorgt ervoor dat de koper alleen maar de betaling hoeft uit te voeren. Na een verkoop ontvangt je als verkoper meteen een email notificatie waarin vermeld word dat de verkoop succesvol uitgevoerd is. CouponTrade maakt het bedrag binnen 1 a 2 werkdagen over naar de verkoper.
                </p>
                <br>
                <h2>Veilig</h2>
                <p>
                    Onze werkwijze maakt het verkopen en kopen van coupons, e-tickets en veilingen op een veilige wijze mogelijk. Wij controleren de tickets met een geautomatiseerde barcodecheck om te voorkomen dat coupons, e-tickets of veilingen dubbel worden verkocht. Daarnaast controleren wij alle bankgegevens en of er meldingen zijn van internetoplichting. Om de kopers zekerheid te bieden krijgen zij inzicht in de onderstaande gegevens van de koper:
                    <ul>
                        <li>naam</li>
                        <li>woonplaats</li>
                        <li>een profiel foto</li>
                    </ul>
                </p>
                <br>
                <h1>Uitleg voor kopers</h1>
                <p>
                    De strenge kwaliteitseisen van CouponTrade zorgen ervoor dat het uitvoeren van een aankoop op CouponTrade veilig is. Wij bieden een platform waarop de koper op een veilige, snelle en gemakkelijk manier een coupon, e-ticket of veiling kan kopen. Deze veiligheid proberen wij te waarborgen door het onderstaande mogelijk te maken:
                    <ul>
                        <li>Bij Coupontrade is het verplicht om als koper en verkoper in te loggen met Facebook of je te registreren met je e-mail adres, met als doel dat je weet wie de verkoper is en wij van CouponTrade weten bij wie jij een aankoop doet.</li>
                        <li>Op CouponTrade ben je verplicht op een profiel foto te plaatsen. Hiermee kan er een inschatting gemaakt worden naar de veiligheid van de potentiele aankoop.</li>
                        <li>De barcodes van coupons, e-tickets of veilingen die op CouponTrade worden geplaatst, worden door een automatische scan gecontroleerd op duplicaten.</li>
                        <li>De bankgegevens van de verkoper wordt telkens gecontroleerd op mijnpolitie.nl</li>
                        <li>CouponTrade neemt altijd direct contact op met beide partijen wanneer een verkoper een negatieve beoordeling heeft gehad. Indien, deze negatieve beoordeling klopt zal de verkoper geblokkeerd worden en verwijderd worden van het platform.</li>
                        <li>CouponTrade beschikt over de gegevens(naam, bankrekening, telefoonnummer, email, etc.) van de koper. Bij het constateren van fraude zullen wij deze informatie aan de betrokken autoriteit verstrekken.</li>
                    </ul>
                </p>
                <br>
                <h2>Simpel</h2>
                <p>
                    Een kaartje kopen op CouponTrade is niet moeilijk! Als koper log je in met je e-mail adres of Facebook account daarna reken je af met iDEAL en ontvang je de coupon, het e-ticket of de veiling meteen per e-mail. Deze werkwijze zorgt ervoor dat het aangeboden coupon, veiling of coupon 24/7 gekocht kan worden zonder contact te maken met de verkoper.
                </p>
                <br>
                <h1>Wat zijn de kosten?</h1>
                <p>
                    Voor het plaatsen van een advertentie betaal je niks. CouponTrade brengt pas kosten in rekening wanneer het gelukt is om je coupon, e-ticket of veiling te kopen of verkopen. Het kost je dan een klein percentage van het verkoopprijs: 5%. Als het gaat om een Coupon van € 10, ontvangt de verkoper € 9,50. Ook de koper betaalt 5%, dus die rekent hier € 10,50 (inclusief 5% bemiddelingskosten) af.
                </p>
			</div>
		</div>
		<div class="row">
			<div class="medium-10 medium-offset-1">
				<div class="info-box">
					<b>Help!</b>
					<br>
					Voor vragen en/of opmerkingen kun je altijd mailen naar <a href="mailto:info@coupontrade.nl">info@coupontrade.nl</a>.
				</div>
			</div>
		</div>
	</div>
@stop