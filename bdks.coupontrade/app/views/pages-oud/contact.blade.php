@extends('layouts.main')

@section('page')
	<div class="page">
		<div class="hero">
			<div class="row">
				<div class="medium-10 medium-offset-1 columns">
					<h1>Contact</h1>
				</div>
			</div>
		</div>
		<div class="row padding faq">
			<div class="medium-10 medium-offset-1 columns well">
                <h2>We helpen je graag</h2>
                <p>
                    Bij ons staan eerlijkheid en gemak hoog in het vaandel. Je hoeft niet bang te zijn om via ingewikkelde omwegen ons te pakken te krijgen. We reageren snel op mails en je kunt ons altijd bellen. We helpen je graag en doen er alles aan om het kopen of verkopen van je kaartje zo soepel mogelijk te laten verlopen.
                    <br><br><br>
                    CouponTrade.nl
                    <br>
                    info@coupontrade.nl
                    <br>
                    06 - 336 454 11
                </p>
			</div>
		</div>
		<div class="row">
			<div class="medium-10 medium-offset-1">
				<div class="info-box">
					<b>Help!</b>
					<br>
					Voor vragen en/of opmerkingen kun je altijd mailen naar <a href="mailto:info@coupontrade.nl">info@coupontrade.nl</a>.
				</div>
			</div>
		</div>
	</div>
@stop