@extends('layouts.main')

@section('page')
	<div class="page">
		<div class="hero">
			<div class="row">
				<div class="small-12 columns text-center">
					<h1>{{ucwords(str_replace('-', ' ', $cat))}}</h1>
				</div>
			</div>
		</div>
		<div class="row smallnav">
			<div class="medium-12 columnss">
				@foreach($cats as $c)
					<a href="/categorie/{{$c}}">{{str_replace('-', ' ', $c)}}</a>
				@endforeach
			</div>
		</div>
		<div class="row padding">
			@if(count($ads) > 0)
				@foreach($ads as $bon)
					<div class="medium-4 columns end">
						<div class="well bon">
							<div class="verkoper-details">
								<h4><small>Aangeboden door</small></h4>
								<h3>{{$bon->verkoper->naam}}</h3>
								<p>in <a href="/categorie/{{$bon->categorie}}">{{ucwords(str_replace('-', ' ', $bon->categorie))}}</a></p>
							</div>
							<div class="picture" style="background:url({{$bon->verkoper->pictureUrl}}) center center / cover no-repeat;"></div>
							<div class="content">
								<h4>
									<a class="bonlink" href="/advertentie/{{$bon->advertentieId}}-{{$bon->titelUrl}}">{{ $bon->titel }}</a>
								</h4>
								<p>
									{{{ str_replace('<br />', '', strlen($bon->omschrijving) > 130 ? substr($bon->omschrijving,0,130)."..." : $bon->omschrijving) }}}
								</p>
								<div class="prijs">
									<a href="/advertentie/{{$bon->advertentieId}}-{{$bon->titelUrl}}"><button class="button button-buy radius left">Kopen</button></a>
									<h1 class="right">
										<span>&euro;</span>{{ $bon->prijs }}
									</h1>
								</div>
							</div>
						</div>
					</div>
				@endforeach
				@if(count($ads) != 12)
					<div class="medium-4 columns end">
						<a href="/advertentie/plaatsen" class="bon-plus-link">
							<div class="bon-plus">
								<i class="ion-ios-plus-outline"></i>
								<br>
								<span>Er zijn verder geen advertenties in deze categorie... Plaats de jouwe gratis!</span>
							</div>
						</a>
					</div>
				@endif
			@else
			<div class="geen-advertenties">
				<h1>
					<i class="ion-sad-outline"></i>
				</h1>
				<h4>
					Helaas... Er zijn geen advertenties meer in deze categorie!
				</h4>
				<p>
					Coupon, e-ticket of een gewonnen veiling te koop? <a href="/advertentie/plaasen">Plaats gratis een advertentie!</a>
				</p>
			</div>
			@endif
		</div>
	</div>
@stop