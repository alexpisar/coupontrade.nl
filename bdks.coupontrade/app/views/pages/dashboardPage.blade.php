@extends('layouts.main')

@section('page')
	<div class="page">
		<div class="hero">
			<div class="row">
				<div class="medium-12 columns text-center">
					<h1>Activiteit</h1>
				</div>
			</div>
		</div>
		<div class="smallnav">
			<div class="row">
				<div class="medium-12 columns">
					<a href="/">Activiteit</a>
					<a href="/statistieken">Statistieken</a>
					<a href="/advertenties">advertenties</a>
					<a href="/verkopen">verkopen</a>
					<a href="/gebruikers">gebruikers</a>
					<a href="/partners">partners</a>
					<a href="/acties">Acties</a>
				</div>
			</div>
		</div>

		@if (Session::has('success'))
		<div class="row padding">
			<div class="small-12 columns">
				<div class="alert-box success">
					{{Session::get('success')}}
				</div>
			</div>
		</div>
		@endif
		
		@if (Session::has('open-box') && Session::get('open-box') == 'gemisteAdvertentieBox')
		<div class="boxer open" id="gemisteAdvertentieBox">
		@else
		<div class="boxer" id="gemisteAdvertentieBox">
		@endif
			<div class="row padding">
				<div class="medium-12 columns">
					<div class="well">
						<h3>Gemiste advertentie toevoegen</h3>
						<hr>
						{{Form::open(['url' => '/toevoegen/advertentie-gemist'])}}
						<div class="row">
							<div class="medium-3 columns">
								<b>Advertentie titel</b>
							</div>
							<div class="medium-9 columns">
								{{Form::text('titel')}}
							</div>
						</div>
						<div class="row">
							<div class="medium-3 columns">
								<b>Korte omschrijving</b>
							</div>
							<div class="medium-9 columns">
								{{Form::textarea('omschrijving')}}
							</div>
						</div>
						<div class="row">
							<div class="medium-3 columns">
								<b>Verloop datum (dd-mm-jjjj)</b>
							</div>
							<div class="medium-9 columns">
								{{Form::text('date')}}
							</div>
						</div>
						<div class="row">
							<div class="medium-3 columns">
								<b>Prijs in hele euro's</b>
							</div>
							<div class="medium-9 columns">
								{{Form::number('prijs')}}
							</div>
						</div>
						<div class="row">
							<div class="medium-3 columns">&nbsp;</div>
							<div class="medium-9 columns">
								{{Form::submit('Plaatsen', ['class' => 'button button-green radius'])}}
								@if (Session::has('whoopsGemist'))
								<p class="text-alert">{{Session::get('whoopsGemist')}}</p>
								@endif
							</div>
						</div>
						{{Form::close()}}
					</div>
				</div>
			</div>
		</div>
		
		@if (Session::has('open-box') && Session::get('open-box') == 'nieuwsberichtBox')
		<div class="boxer open" id="nieuwsBerichtBox">
		@else
		<div class="boxer" id="nieuwsBerichtBox">
		@endif
			<div class="row padding">
				<div class="medium-12 medium-offset columns">
					<div class="well">
						<h3>Nieuwsbericht plaatsen</h3>
						<hr>
						@if (Session::has('whoopsNieuws'))
						<p class="text-alert">{{Session::get('whoopsNieuws')}}</p>
						@endif
						{{Form::open(['url' => '/toevoegen/nieuwsbericht'])}}
						<div class="row">
							<div class="medium-3 columns">
								<b>Bericht</b>
							</div>
							<div class="medium-9 columns">
								{{Form::textarea('contentInput', '', ['id' => 'nieuwsInput', 'style' => 'resize:auto;height:150px;'])}}
								{{Form::textarea('content', '', ['id' => 'nieuwsVal', 'style' => 'display:none;'])}}

								<div class="well" style="padding: 0.65em;" id="nieuwsOutput">
									Een voorbeeld van het ingevoerde bericht verschijnt hier.
								</div>
							</div>
						</div>
						<br>
						<hr>
						<br>
						<div class="row">
							<div class="medium-3 columns">
								<b>Ontvangers</b>
							</div>
							<div class="medium-9 columns nlabels">
								<label><input type="checkbox" name="ontvangers[]" value="iedereen"> Iedereen</label>
								@foreach(Partner::all() as $ontvanger)
									<label><input type="checkbox" name="ontvangers[]" value="{{$ontvanger->referentieId}}"> {{$ontvanger->naam}}</label>
								@endforeach
							</div>
						</div>
						<div class="row">
							<div class="medium-3 columns">
								&nbsp;
							</div>
							<div class="medium-9 columns">
								{{Form::submit('Versturen', ['class' => 'button button-green radius'])}}
							</div>
						</div>
						{{Form::close()}}
					</div>
				</div>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="medium-8 columns">
				<div class="well">
				@foreach ($activiteiten as $a)
					<div class="" style="margin-bottom: 2px;">
						@if (substr(reset($a), 0, -2) == 'advertentie' && $a->type != 5)
						<a href="/gebruiker/{{$a->verkoper->userId}}">{{$a->verkoper->naam}}</a> heeft de advertentie <a href="http://www.coupontrade.nl/advertentie/{{$a->advertentieId}}-{{$a->titelUrl}}">{{$a->titel}}</a> geplaatst.
						@elseif (substr(reset($a), 0, -2) == 'advertentie' && $a->type == 5)
						<a href="/partner/{{$a->partner->partnerId}}">{{$a->partner->naam}}</a> heeft de advertentie <a href="http://www.coupontrade.nl/advertentie/{{$a->advertentieId}}-{{$a->titelUrl}}">{{$a->titel}}</a> geplaatst.
						@elseif (substr(reset($a), 0, -2) == 'user')
						<a href="/gebruiker/{{$a->userId}}">{{$a->naam}}</a> heeft zich geregistreerd.
						@elseif (substr(reset($a), 0, -2) == 'verkoop' && $a->advertentie->type != 5)
						<a href="/gebruiker/{{$a->advertentie->verkoper->userId}}">{{$a->advertentie->verkoper->naam}}</a> heeft een verkoop verricht van de advertentie <a href="http://www.coupontrade.nl/advertentie/{{$a->advertentie->advertentieId}}-{{$a->advertentie->titelUrl}}">{{$a->advertentie->titel}}</a>.
						@endif
						<p style="font-size:0.85em;color:grey;">{{date('d-m-Y h:i:s', strtotime($a->createdAt))}}</p>
						<br>
					</div>
				@endforeach
				</div>
			</div>
			<div class="medium-4 columns text-right">
				<div class="well">
					<button class="button button-blue radius" id="gemisteAdvertentieBtn"><span class="ion-plus"> &nbsp; Gemiste advertentie</span></button>
				<button class="button button-blue radius" id="nieuwsBerichtBtn"><span class="ion-person-add"> &nbsp; Nieuwsbericht</span></button>
				</div>
			</div>
		</div>
	</div>
@stop

@section('scripts')
	<script>
		$(document).ready(function() {
			var converter = new Showdown.converter();
			if ($('#nieuwsInput').val() != '')
			{
				var input = $('#nieuwsInput').val().replace('<script>', '');
				var output = converter.makeHtml(input);
				$('#nieuwsOutput').html(output);
				$('#nieuwsVal').val(output);
			}

			$('#nieuwsInput').keyup(function() {
				var input = $(this).val().replace('<script>', '');
				var output = converter.makeHtml(input);
				$('#nieuwsOutput').html(output);
				$('#nieuwsVal').val(output);
			});
		});
	</script>
@stop