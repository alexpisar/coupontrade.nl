@extends('layouts.main')

@section('page')
	<div class="page">
		<div class="hero">
			<div class="small-12 columns text-center">
				<h1>{{$gebruiker->naam}}</h1>
			</div>
		</div>
		<div class="smallnav">
			<div class="row">
				<div class="medium-12 columns">
					<a href="/">Activiteit</a>
					<a href="/statistieken">Statistieken</a>
					<a href="/advertenties">advertenties</a>
					<a href="/verkopen">verkopen</a>
					<a href="/gebruikers">gebruikers</a>
					<a href="/partners">partners</a>
					<a href="/acties">Acties</a>
				</div>
			</div>
		</div>
		<div class="row padding">
			<div class="well">
				<div class="row">
					<div class="small-12 columns">
						<div class="gebruiker-picture" style="background:url({{substr($gebruiker->pictureUrl, 0, 4) === 'http' ? $gebruiker->pictureUrl : 'http://www.coupontrade.nl/' . $gebruiker->pictureUrl}}) 50% 50% / cover no-repeat;"></div>
						Geregistreerd op <b>{{date('d-m-Y', strtotime($gebruiker->createdAt))}}</b>
						<br>
						Woont in <b>{{$gebruiker->woonplaats}}</b>
						<br>
						<a href="mailto:{{$gebruiker->email}}">{{$gebruiker->email}}</a>
						<br>
						<b>Bankgegevens</b>
						<br>
						Naam bankhouder: {{$gebruiker->bankhouder == NULL ? '-' : $gebruiker->bankhouder}}
						<br>
						IBAN: {{$gebruiker->iban == NULL ? '-' : $gebruiker->iban}}
					</div>
				</div>
				<br><br>
				<div class="row">
					<div class="small-12 columns">
						<h3>Advertenties</h3>
						<table style="width:100%" border="1">
							<tr>
								<th width="1">ID</th>
								<th width="1">Titel</th>
								<th width="1">Prijs</th>
								<th width="1">Winst</th>
								<th width="1">Verkocht</th>
								<th width="1">Gecontroleerd</th>
								<th width="1">Door</th>
							</tr>
							@foreach ($ads as $ad)
							<tr>
								<td>{{$ad->advertentieId}}</td>
								<td><a href="http://www.coupontrade.nl/advertentie/{{$ad->advertentieId}}-{{$ad->titelUrl}}">{{$ad->titel}}</a></td> 
								<td>&euro;{{$ad->prijs}}</td>
								<td>&euro;{{$ad->prijs * 0.1}}</td>
								<td>{{$ad->verkocht == 1 ? 'Ja' : 'Nee'}}</td>
								<td>{{$ad->gecontroleerd == 1 ? 'Ja' : 'Nee'}}</td>
								<td>{{$ad->gecontroleerdDoor != NULL ? $ad->gecontroleerdDoor : '-'}}</td>
							</tr>
							@endforeach
						</table>
					</div>
				</div>
				<br><br>
				<div class="row">
					<div class="small-12 columns">
						<h3>Aankopen</h3>
						<table style="width:100%" border="1">
							<tr>
								<th width="1">ID</th>
								<th width="1">Titel</th>
								<th width="1">Prijs</th>
								<th width="1">Winst</th>
								<th width="1">Verkoper</th>
								<th width="1">Gekocht op</th>
							</tr>
							@foreach ($aankopen as $ad)
							<tr>
								<td>{{$ad->advertentie->advertentieId}}</td>
								<td><a href="http://www.coupontrade.nl/advertentie/{{$ad->advertentie->advertentieId}}-{{$ad->advertentie->titelUrl}}">{{$ad->advertentie->titel}}</a></td> 
								<td>&euro;{{$ad->advertentie->prijs}}</td>
								<td>&euro;{{$ad->advertentie->prijs * 0.1}}</td>
								@if($ad->advertentie->type != 5)
								<td><a href="/gebruiker/{{$ad->advertentie->verkoper->userId}}">{{$ad->advertentie->verkoper->naam}}</a></td>
								@else
								<td><a href="/partner/{{$ad->advertentie->partner->referentieId}}">{{$ad->advertentie->partner->naam}}</a></td>
								@endif
								<td>{{date('d-m-Y', strtotime($ad->updatedAt))}}</td>
							</tr>
							@endforeach
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
@stop