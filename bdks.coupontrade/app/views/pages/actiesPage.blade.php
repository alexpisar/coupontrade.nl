@extends('layouts.main')

@section('page')
	<div class="page">
		<div class="hero">
			<div class="row">
				<div class="small-12 columns text-center">
					<h1>Acties</h1>
				</div>
			</div>
		</div>
		<div class="smallnav">
			<div class="row">
				<div class="medium-12 columns">
					<a href="/">Activiteit</a>
					<a href="/statistieken">Statistieken</a>
					<a href="/advertenties">advertenties</a>
					<a href="/verkopen">verkopen</a>
					<a href="/gebruikers">gebruikers</a>
					<a href="/partners">partners</a>
					<a href="/acties">Acties</a>
				</div>
			</div>
		</div>
		@if (Session::has('success'))
		<div class="row padding">
			<div class="small-12 columns">
				<div class="alert-box success">
					{{Session::get('success')}}
				</div>
			</div>
		</div>
		@endif
		<div class="row padding">
			<div class="small-12 columns">
				<div class="well">
					<div class="left">
						<h3>Acties</h3>
					</div>
					<div class="right">
						<button class="button button-blue radius" id="actieToevoegenBtn"><span class="ion-plus"> &nbsp; Actie toevoegen</span></button>
					</div>
					<table style="width:100%" border="1">
					<tr>
						<th width="1">ID</th>
						<th width="100%">Titel</th>
						<th width="1" class="text-center"><i class="ion-person"></i></th>
						<th width="1" class="text-center">Van</th>
						<th width="1" class="text-center">Tot</th>
						<th width="1" class="text-right">Door</th>
					</tr>
					<tr>
						<td>1</td>
						<td><a href="/actie/1">Van der Valk actie</a></td>
						<td>92</td>
						<td>20-12-2014</td>
						<td>28-02-2014</td>
						<td>chinoe</td>
					</tr>
					</table>
				</div>
			</div>
		</div>
	</div>
	</div>
@stop