@extends('layouts.main')

@section('page')
	<div class="page">
		<div class="hero">
			<div class="row">
				<div class="small-12 columns text-center">
					<h1>Advertenties</h1>
				</div>
			</div>
		</div>
		<div class="smallnav">
			<div class="row">
				<div class="medium-12 columns">
					<a href="/">Activiteit</a>
					<a href="/statistieken">Statistieken</a>
					<a href="/advertenties">advertenties</a>
					<a href="/verkopen">verkopen</a>
					<a href="/gebruikers">gebruikers</a>
					<a href="/partners">partners</a>
					<a href="/acties">Acties</a>
				</div>
			</div>
		</div>
		<div class="row padding">
			<div class="small-12 columns">
				<div class="well">
					<h3>Advertenties</h3>
					<table style="width:100%" border="1">
					<tr>
						<th width="1">ID</th>
						<th width="70%">Titel</th>
						<th width="30%">Verkoper</th>
						<th width="1" class="text-center">Verkocht?</th>
						<th width="1" class="text-center">Verwijderd?</th>
						<th width="1" class="text-right">Geplaatst op</th>
					</tr>
					@foreach($ads as $a)
					<tr>
						<td>{{$a->advertentieId}}</td>
						<td><a href="http://www.coupontrade.nl/advertentie/{{$a->advertentieId}}-{{$a->titelUrl}}">{{$a->titel}}</a></td>
						@if ($a->type != 5)
						<td><a href="/gebruiker/{{$a->verkoper->userId}}">{{$a->verkoper->naam}}</a></td>
						@else
						<td><a href="/gebruiker/{{$a->partner->partnerId}}">{{$a->partner->naam}}</a> (partner)</td>
						@endif
						<td class="text-center">{{$a->verkocht == '0' ? 'Nee' : 'Ja'}}</td>
						@if ($a->type != 5)
						<td class="text-center">{{$a->verwijderd == '0' ? 'Nee' : 'Ja'}}</td>
						@else
						<td class="text-center">{{$a->verwijderd == '0' ? 'Nee (actief)' : 'Ja (inactief)'}}</td>
						@endif
						<td class="text-right">{{date('d-m-Y h:i:s', strtotime($a->createdAt))}}</td>
					</tr>
					@endforeach
					</table>
				</div>
			</div>
		</div>
	</div>
@stop