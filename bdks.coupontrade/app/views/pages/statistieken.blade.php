@extends('layouts.main')

@section('page')
	<div class="page">
		<div class="hero">
			<div class="row">
				<div class="medium-12 columns text-center">
					<h1>Statistieken</h1>
				</div>
			</div>
		</div>
		<div class="smallnav">
			<div class="row">
				<div class="medium-12 columns">
					<a href="/">Activiteit</a>
					<a href="/statistieken">Statistieken</a>
					<a href="/advertenties">advertenties</a>
					<a href="/verkopen">verkopen</a>
					<a href="/gebruikers">gebruikers</a>
					<a href="/partners">partners</a>
					<a href="/acties">Acties</a>
				</div>
			</div>
		</div>
		<br>
		<!-- Dashboard -->
		<div class="row well">
			<div class="medium-3 columns">
				<h3><small>Gebruikers</small></h3>
				<canvas class="chart" id="userChart" data-user-count="{{User::count()}}" data-facebook-count="{{User::where('facebookId', '!=', 'NULL')->count()}}"></canvas>
				<div class="text-right"><a href="/gebruikers">Overzicht</a></div>
			</div>
			<div class="small-12 columns">
				<br><hr><br>
			</div>
			<div class="medium-6 columns">
				<h3><small>Actieve advertenties</small></h3>
				<canvas class="chart" id="adsChart"
					data-vak="{{Advertentie::where('categorie', '=', 'vakanties')->where('verkocht', '=', '0', 'AND')->count()}}"
					data-eve="{{Advertentie::where('categorie', '=', 'evenementen')->where('verkocht', '=', '0', 'AND')->count()}}"
					data-res="{{Advertentie::where('categorie', '=', 'restaurants')->where('verkocht', '=', '0', 'AND')->count()}}"
					data-hot="{{Advertentie::where('categorie', '=', 'hotels')->where('verkocht', '=', '0', 'AND')->count()}}"
					data-avo="{{Advertentie::where('categorie', '=', 'avondje-uit')->where('verkocht', '=', '0', 'AND')->count()}}"
					data-dag="{{Advertentie::where('categorie', '=', 'dagje-weg')->where('verkocht', '=', '0', 'AND')->count()}}"
					data-spo="{{Advertentie::where('categorie', '=', 'sport')->where('verkocht', '=', '0', 'AND')->count()}}"
					data-ove="{{Advertentie::where('categorie', '=', 'overig')->where('verkocht', '=', '0', 'AND')->count()}}"
				></canvas>
				<div class="text-right"><a href="/advertenties">Overzicht</a></div>
			</div>
			<div class="medium-6 columns">
				<h3><small>Verkopen</small></h3>
				<canvas width="400" height="200" style="padding-right:30px;" id="upvChart"
					data-wa="{{Verkoop::where('createdAt', 'LIKE', date('Y-m-d', strtotime('-6 days')) . '%')->count()}}"
					data-wb="{{Verkoop::where('createdAt', 'LIKE', date('Y-m-d', strtotime('-5 days')) . '%')->count()}}"
					data-wc="{{Verkoop::where('createdAt', 'LIKE', date('Y-m-d', strtotime('-4 days')) . '%')->count()}}"
					data-wd="{{Verkoop::where('createdAt', 'LIKE', date('Y-m-d', strtotime('-3 days')) . '%')->count()}}"
					data-we="{{Verkoop::where('createdAt', 'LIKE', date('Y-m-d', strtotime('-2 days')) . '%')->count()}}"
					data-wf="{{Verkoop::where('createdAt', 'LIKE', date('Y-m-d', strtotime('-1 days')) . '%')->count()}}"
					data-wg="{{Verkoop::where('createdAt', 'LIKE', date('Y-m-d', strtotime('now')) . '%')->count()}}"
				></canvas>
				<div class="text-right"><a href="/verkopen">Overzicht</a></div>
			</div>
		</div>
	</div>
@stop

@section('scripts')
	<script>
		$(document).ready(function() {
			Chart.defaults.global.responsive = true;
			var userCtx   = $('#userChart').get(0).getContext("2d");
			var userCount = $('#userChart').data('userCount');
			var facbCount = $('#userChart').data('facebookCount');
			var userData = [
				{
					value: userCount - facbCount,
					color: "rgba(151,187,205,0.35)",
					highlight: "rgba(151,187,205,0.65)",
					label: "E-mail"
				},
				{
					value: facbCount,
					color: "rgba(151,187,205,1)",
					highlight: "#3A539B",
					label: "Facebook"
				}
			];
			var userChart = new Chart(userCtx).Pie(userData);
			
			var adsCtx    = $('#adsChart').get(0).getContext("2d");
			var dta 	  = $('#adsChart');
			var adsData   =	{
				labels: ["Vakanties", "Evenementen", "Restaurants", "Hotels", "Avondje Uit", "Dagje Weg", "Sport", "Overig"],
				datasets: [
			        {
			            label: "Dataset",
			            fillColor: "rgba(151,187,205,0.2)",
			            strokeColor: "rgba(151,187,205,1)",
			            pointColor: "rgba(151,187,205,1)",
			            pointStrokeColor: "#fff",
			            pointHighlightFill: "#fff",
			            pointHighlightStroke: "rgba(151,187,205,1)",
			            data: [dta.data('vak'), dta.data('eve'), dta.data('res'), dta.data('hot'), dta.data('avo'), dta.data('dag'), dta.data('spo'), dta.data('ove')]
			        }
				]
			};
			var adsChart = new Chart(adsCtx).Radar(adsData);

			var curr = new Date;
			var first = curr.getDate() - 7;
			var days = [];
			for(var i = 1;i<8;i++){
			    var next = new Date(curr.getTime());
			    next.setDate(first + i);
			    var day = next.getDate();
			    var mon = next.getMonth() + 1;
			    days.push(day + '-' + mon);
			}

			var vupCtx   = $('#upvChart').get(0).getContext("2d");
			var vdata    = $('#upvChart');
			var vupData  = {
			    labels: days,
			    datasets: [
			        {
			            label: "Verkopen",
			            fillColor: "rgba(151,187,205,0.2)",
			            strokeColor: "rgba(151,187,205,1)",
			            pointColor: "rgba(151,187,205,1)",
			            pointStrokeColor: "#fff",
			            pointHighlightFill: "#fff",
			            pointHighlightStroke: "rgba(151,187,205,1)",
			            data: [vdata.data('wa'), vdata.data('wb'), vdata.data('wc'), vdata.data('wd'), vdata.data('we'), vdata.data('wf'), vdata.data('wg')]
			        }
			    ]
			};
			var vupChart = new Chart(vupCtx).Line(vupData);
		});
	</script>
@stop