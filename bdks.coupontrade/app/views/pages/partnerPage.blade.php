@extends('layouts.main')

@section('page')
	<div class="page">
		<div class="hero">
			<div class="small-12 columns text-center">
				<h1>{{$partner->naam}}</h1>
			</div>
		</div>
		<div class="smallnav">
			<div class="row">
				<div class="medium-12 columns">
					<a href="/">Activiteit</a>
					<a href="/statistieken">Statistieken</a>
					<a href="/advertenties">advertenties</a>
					<a href="/verkopen">verkopen</a>
					<a href="/gebruikers">gebruikers</a>
					<a href="/partners">partners</a>
					<a href="/acties">Acties</a>
				</div>
			</div>
		</div>
		<div class="row padding">
			<div class="well">
				<div class="row">
					<div class="medium-3 columns">
						<div class="left">
							<h2><b>{{$partner->referentieId}}</b></h2>
							<h2><small>{{$partner->naam}}</small></h2>
							Partner sinds <b>{{date('d-m-Y', strtotime($partner->createdAt))}}</b>
							<br>
							{{$partner->straat}}
							<br>
							{{$partner->postcode}}, {{$partner->stad}}
							<br>
							<a href="{{$partner->website}}" target="_blank">{{$partner->website}}</a>
							<br>
							Behandeld door <b>{{$partner->manager->username}}</b>
							<br><br>
							<h2><small>Contactpersoon</small></h2>
							{{$partner->contactpersoon}}
							<br>
							<a href="mailto:{{$partner->email}}">{{$partner->email}}</a>
							<br>
							{{$partner->telefoon}}
						</div>
					</div>
					<div class="medium-9 columns">
						@if(Session::has('fouten'))
						<div class="well partner-ad-box" id="partnerAdBox" style="display:block;">
							<div data-alert class="alert-box alert radius">						
								<?php $fouten = []; ?>
								@foreach(Session::get('fouten') as $error)
								<?php $fouten[] = $error; ?>
								@endforeach
								<?php $foten = array_unique($fouten) ?>
								@foreach($foten as $fout)
									{{$fout}}<br>
								@endforeach
							</div>
						@else
						<div class="well partner-ad-box" id="partnerAdBox">
						@endif
							<h3><small>Advertentie plaatsen voor {{$partner->naam}}</small></h3>
							<hr>
							{{Form::open(['url' => '/partner/' . $partner->referentieId . '/advertentie-plaatsen'])}}
								<b>Categorie</b>
								{{Form::select('categorie', ['0' => 'Kies een categorie', 'vakanties' => 'Vakanties', 'evenementen' => 'Evenementen', 'restaurants' => 'Restaurants', 'hotels' => 'Hotels', 'avondje-uit' => 'Avondje uit', 'dagje-weg' => 'Dagje weg', 'sport' => 'Sport', 'overig' => 'Overig'])}}<br><br>

								<b>Prijs en waarde</b>
								<div class="row" id="stap2">
									<div class="small-12 columns">
										<div class="left">
											<label>Verkoopprijs</label>
											<h1><span>&euro;</span> {{ Form::number('prijs', '', ['placeholder' => '0', 'min' => '0', 'id' => 'prijs']) }}</h1>
										</div>
										<div class="left mlt">
											<label>Ter waarde van</label>
											<h1><span>&euro;</span> {{ Form::number('twv', '', ['placeholder' => '0', 'min' => '0', 'id' => 'twv']) }}</h1>
										</div>					
									</div>
								</div><br>
							
								<b>Titel</b>
								{{Form::text('titel')}}<br>

								<b>Omschrijving arrangement</b><br>
								<i><b>Let op:</b> gebruik <a href="http://markdown.chibi.io/#bold" target="_blank">Markdown syntax</a> / <a href="https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet" target="_blank">(alt)</a> om de omschrijving te formatteren.</i>
								{{Form::textarea('omschrijving', '', ['style' => 'display:none;', 'id' => 'omschrijvingVal'])}}
								{{Form::textarea('omschrijvingMarkdown', '', ['id' => 'omschrijvingInput', 'style' => 'resize:auto;height:250px;'])}}

								<div class="well" style="padding: 0.65em;" id="markdownOutput">
									Een voorbeeld van de ingevoerde omschrijving verschijnt hier.
								</div>
								<br><br>

								<b>Voorwaarden arrangement</b><br>
								<i><b>Let op:</b> gebruik <a href="http://markdown.chibi.io/#bold" target="_blank">Markdown syntax</a> / <a href="https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet" target="_blank">(alt)</a> om de voorwaarden te formatteren.</i>
								{{Form::textarea('voorwaarden', '', ['style' => 'display:none;', 'id' => 'voorwaardenVal'])}}
								{{Form::textarea('voorwaardenMarkdown', '', ['id' => 'voorwaardenInput', 'style' => 'resize:auto;height:250px;'])}}

								<div class="well" style="padding: 0.65em;" id="voorwaardenOutput">
									Een voorbeeld van de ingevoerde voorwaarden verschijnt hier.
								</div>
								<br><br>

								<hr>
								{{Form::submit('Plaatsen', ['class' => 'button button-blue radius', 'style' => 'width:auto;padding:1em 3em;'])}} &nbsp; <span class="text-alert">KIJK DE ADVERTENTIE GOED NA!!!</span>
							{{Form::close()}}
						</div>
						<button class="button button-green radius" id="partnerAdBtn"><span class="ion-plus"></span>Advertentie toevoegen</button>
						<div class="right">
							<input type="hidden" id="partnerId" value="{{$partner->partnerId}}">
							<button class="button button-green radius" id="makeFactuurBtn">
								<div id="iconFactuurBtn">
									<span class="ion-document"></span>Factuur {{date('m-Y')}}
								</div>
								<div id="spinner" class="sk-spinner sk-spinner-wave">
									<div class="sk-rect1"></div>
									<div class="sk-rect2"></div>
									<div class="sk-rect3"></div>
									<div class="sk-rect4"></div>
									<div class="sk-rect5"></div>
								</div>
							</button>
						</div>
						<hr>
						<table style="width:100%;min-width:500px;" border="1">
							<tr>
								<th width="15%">Titel</th>
								<th width="1%" class="text-center">&euro;</th>
								<th width="2%" class="text-center"><span class="ion-cash"></span></th>
								<th width="2%" class="text-center"><span class="ion-eye"></span></th>
								<th width="3%" class="text-center"><span class="ion-calendar"></span></th>
								<th width="1%" class="text-center">Actief</th>
								<th width="3%" class="text-right">Bewerken</th>
							</tr>
							@if (count($ads) > 0)
								@foreach ($ads as $ad)
								<tr>
									<td><a href="http://www.coupontrade.nl/advertentie/{{$ad->advertentieId}}-{{$ad->titelUrl}}">{{$ad->titel}}</a></td> 
									<td class="text-center">&euro;{{$ad->prijs}}</td>
									<td class="text-center">{{count($partner->verkopen()->where('advertentieId', '=', $ad->advertentieId)->get())}}</td>
									<td class="text-center">{{$ad->weergaven}}</td>
									<td class="text-center">{{date('d-m-Y', strtotime($ad->createdAt))}}</td>
									<td class="text-center table-toggle toggleActiefPartnerAdvertentie" data-active="{{$ad->verkocht}}" data-aid="{{$ad->advertentieId}}">
										{{$ad->verwijderd == 0 ? '<h3 class="toggled"><span class="ion-toggle-filled"></span></h3>' : '<h3><span class="ion-toggle"></span></h3>'}}
									</td>
									<td class="text-right"><a href="/advertentie/{{$ad->advertentieId}}/bewerken">Bewerken</a></td>
								</tr>
								@endforeach
							@else
								<tr class="text-center">
									<td>Er zijn geen advertenties gevonden voor deze partner.</td>
								</tr>
							@endif
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
@stop

@section('scripts')
	<script>
		$(document).ready(function() {
			var converter = new Showdown.converter();
			if ($('#omschrijvingInput').val() != '')
			{
				var input = $('#omschrijvingInput').val().replace('<script>', '');
				var output = converter.makeHtml(input);
				$('#markdownOutput').html(output);
				$('#omschrijvingVal').val(output);
			}

			$('#omschrijvingInput').keyup(function() {
				var input = $(this).val().replace('<script>', '');
				var output = converter.makeHtml(input);
				$('#markdownOutput').html(output);
				$('#omschrijvingVal').val(output);
			});

			if ($('#voorwaardenInput').val() != '')
			{
				var input = $('#voorwaardenInput').val().replace('<script>', '');
				var output = converter.makeHtml(input);
				$('#voorwaardenOutput').html(output);
				$('#voorwaardenVal').val(output);
			}

			$('#voorwaardenInput').keyup(function() {
				var input = $(this).val().replace('<script>', '');
				var output = converter.makeHtml(input);
				$('#voorwaardenOutput').html(output);
				$('#voorwaardenVal').val(output);
			});
		});
	</script>
@stop