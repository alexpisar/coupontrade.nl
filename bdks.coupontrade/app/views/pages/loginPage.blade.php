@extends('layouts.main')

@section('page')
<div class="page">
	<div class="hero">
		<div class="content" style="margin-top: 35px;">
			<div class="row">
				<div class="small-12 columns text-center">
					<h1>CouponTrade Admin</h1>
				</div>
			</div>
		</div>
	</div>
	<div class="row padding">
		<div class="medium-4 medium-offset-4 columns well login-box">
			<h3>Login</h3>
			@if(Session::has('whoops'))
			<p class="text-alert">{{Session::get('whoops')}}</p>
			@endif
			{{Form::open(['url' => '/login'])}}
			<b>Gebruikersnaam</b>
			{{Form::text('username', '')}}
			<b>Wachtwoord</b>
			{{Form::password('password', '')}}
			{{Form::submit('Inloggen', ['class' => 'button button-blue radius'])}}
			{{Form::close()}}
		</div>
	</div>
</div>
@stop