<!DOCTYPE html>
<html>
<head>
	<style>
		body {
			font-family: 'Helvetica Neue', 'Helvetica', 'Arial', sans-serif;
			width: 100%;
			font-size: 80%;
		}
		.left {
			width: 50%;
			position: absolute;
			top: 12px;
			left: 0;
		}
		.right {
			width: 50%;
			text-align: right;
			position: absolute;
			top: 0 !important;
			right: 0;
		}
		.row {
			width: 100%;
			height: 125px;
			position: relative;
		}
		.totalen {
			width: 100%;
			position: relative;
		}
		dt {
			font-weight: 700;
		}
		table {
			width: 100%;
			/*border: 1px solid #c5c5c5;*/
		}
		table,th,td {
		    border-collapse: collapse;
		    text-align: left;
		}
		th {
			background: #3498DB;
			color: white;
			padding: 0.25em;
		}
		td {
		  padding: 0.15em 0.25em;  
		}
		.footer {
			border-top: 1px solid #c5c5c5;
			position: absolute;
			bottom: 0;
			width: 100%;
			margin-top: 50px;
		}
		h5 {
			font-weight: normal;
			padding: 0;
			margin-top: 5px;
		}
		hr {
			border-width: 1px;
			border-color: #c5c5c5;
			border-style: solid;
			border-bottom: none;
		}
		.idx {
			background: #e5e5e5;
		}
	</style>
</head>
<body>
	<center><img src="img/logo.png" width="200px"></center>
	<br><br>
	<div class="row">
		<div class="left">
			{{$data['naam']}}<br>
			{{$data['straat']}}<br>
			{{$data['postcode']}}<br>
			Uw referentie: {{$data['ref']}}
		</div>
		<div class="right">
			<dl>
				<dt>Factuurnummer</dt>
				<dd>{{$data['factuurnummer']}}</dd>
				<dt>Factuurdatum</dt>
				<dd>{{$data['datum']}}</dd>
				<dt>BTW-nummer</dt>
				<dd>{{$data['btwnummer']}}</dd>
			</dl>
		</div>
	</div>
	<h4>Betreft: boekingen {{date('m-Y')}}</h4>
	<table>
		<tr>
			<th>Datum</th>
			<th>Boeking #</th>
			<th>Boekingsomschrijving</th>
			<th>Naam koper</th>
			<th style="text-align:right;">Bedrag</th>
		</tr>
		@foreach($boekingen as $b)
		<tr>
			<td>{{date('d-m-Y', strtotime($b->createdAt))}}</td>
			<td>{{$b->verkoopId}}</td>
			<td>{{$b->advertentie->titel}}</td>
			<td>{{$b->koper->naam}}</td>
			<td style="text-align:right;">{{number_format($b->advertentie->prijs, 2)}}</td>
		</tr>
		<tr class="idx">
			<td>{{date('d-m-Y', strtotime($b->createdAt))}}</td>
			<td>{{$b->verkoopId}}</td>
			<td>{{$b->advertentie->titel}}</td>
			<td>{{$b->koper->naam}}</td>
			<td style="text-align:right;">-0.35</td>
		</tr>
		@endforeach
	</table>
	<br><br><br>
	<div class="totalen">
		<div class="left" style="top:0;">
			<table style="width:200px;">
				<tr>
					<th>Factuurinformatie</th>
					<th></th>
				</tr>
				<tr>
					<td>Aantal boekingen</td>
					<td>{{count($boekingen)}}</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
			</table>
		</div>
		<div class="right">
			<table style="width:100%;">
				<tr>
					<th>Totalen:</th>
					<th></th>
				</tr>
				<tr>
					<td>Totaal</td>
					<td style="text-align:right;">{{number_format($data['totaal'], 2)}}</td>
				</tr>
				<tr>
					<td>Transactiekosten</td>
					<td style="text-align:right;">-{{number_format($data['transactiekosten'], 2)}}</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td><hr></td>
				</tr>
				<tr>
					<td><b>Te ontvangen</b></td>
					<td style="text-align:right;"><b>{{number_format($data['totaal'] - $data['transactiekosten'], 2)}}</b></td>
				</tr>
			</table>
		</div>
	</div>
	<br><br><br><br><br><br><br><br><br><br>
	<div class="footer">
		<h5>
			Factuur uitgerekt door CouponTrade - Nienke van Hichtumstraat 21, 1064 MH AMSTERDAM
			<br>
			Telefoon: +31 (0)63 36 45 411 - info@coupontrade.nl - www.coupontrade.nl
			<br>
			info@coupontrade.nl - BTW-nummer: 13813590 - KvK-nummer: 59841958
		</h5>
	</div>
</body>
</html>