<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<style>
		html {
			margin: 0;
			padding: 0;
		}
		body {
			font-family: 'Helvetica', Helvetica, Arial, sans-serif !important;
			width: 100%;
			font-size: 80%;
			color: #212121;
		}
		.header {
			position: relative;
			width: 100%;
			height: 150px;
			background: url(http://www.coupontrade.nl/img/hero-10-12-2014.jpg) center top / cover no-repeat;
			text-align: center;
			line-height: 100px;
			color: white;
			font-weight: 100;
			font-size: 2em;
		}
		.container {
			width: 100%;
			padding: 1.75em;
		}
		.container-b {
			width: 100%;
			height: 50px;
			max-height: 50px;
			padding: 10px;
			background: #f1f1f1;
		}
		.well {
			width: 100%;
			height: auto;
			padding: 1em;
			background: #FFF;
			border: 1px solid #c5c5c5;
		}
		.wn {
			padding: 0;
			margin: 0;
			font-size: 3em;
			margin: 25px 0;
			margin-top: 0;
			color: #4183D7;
			text-align: right;
		}
		.footer {
			border-top: 1px solid #c5c5c5;
			position: absolute;
			bottom: 0;
			width: 100%;
			margin-top: 50px;
		}
		small {
			font-size: 100%;
		}
		hr {
			border-width: 1px;
			border-color: #c5c5c5;
			border-style: solid;
			border-bottom: none;
		}
	</style>
</head>
<body>	
	<div class="container-b">
		<h1 class="wn"><small>Jouw actiecode is</small> {{$data['waardenummer']}}</h1>
	</div>
	<div class="header">
		{{$data['advertentie']->titel}}
	</div>
	<div class="container">
		<div class="well">
			{{$data['advertentie']->omschrijving}}
			<br>
			<h1>Voorwaarden</h1>
			{{$data['advertentie']->voorwaarden}}
		</div>
	</div>
</body>
</html>