<?php

class Advertentie extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $primaryKey = "advertentieId";
	protected $table = 'Advertenties';

	/**
	 * Disable Laravel's Timestamps
	 *
	 */
	public $timestamps = false;

	// haseOne 'User'
	public function verkoper()
	{
		return $this->hasOne('User', 'userId', 'verkoperId');
	}

	// haseOne 'Partner'
	public function partner()
	{
		return $this->hasOne('Partner', 'referentieId', 'verkoperId');
	}

	// Inverse of hasOne from 'Advertentie'
	public function pdf()
	{
		return $this->belongsTo('Pdf', 'advertentieId', 'advertentieId');
	}

	public function user()
	{
		return $this->belongsTo('User', 'userId', 'verkoperId');
	}

	// hasMany 'Verkoop'
	public function verkopen()
	{
		return $this->hasMany('Verkoop', 'verkoperId', 'referentieId');
	}

}