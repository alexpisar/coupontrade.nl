<?php

class Waardenummer extends Eloquent {

	protected $primaryKey = "waardeId";
	protected $table = 'Waardenummers';

	public $timestamps = false;

	public function partner()
	{
		return $this->hasOne('Partner', 'referentieId', 'partnerRef');
	}

}