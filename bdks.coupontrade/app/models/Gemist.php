<?php

class Gemist extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $primaryKey = "gemistId";
	protected $table = 'Gemist';

	/**
	 * Disable Laravel's Timestamps
	 *
	 */
	public $timestamps = false;

}