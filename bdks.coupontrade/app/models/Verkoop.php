<?php

class Verkoop extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $primaryKey = "verkoopId";
	protected $table = 'Verkopen';

	/**
	 * Disable Laravel's Timestamps
	 *
	 */
	public $timestamps = false;

	// haseOne 'Advertentie'
	public function advertentie()
	{
		return $this->hasOne('Advertentie', 'advertentieId', 'advertentieId');
	}

	// haseOne 'User'
	public function koper()
	{
		return $this->hasOne('User', 'userId', 'koperId');
	}

}