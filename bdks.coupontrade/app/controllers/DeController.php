<?php

class DeController extends BaseController {

	/* Views */
	public function loginPage()
	{
		if (Auth::check())
		{
			return Redirect::to('/dashboard');
		}
		else
		{
			return View::make('pages.loginPage');
		}
	}

	public function dashboardPage()
	{
		if (Auth::check())
		{
			$act = [];

			$ads = Advertentie::take(25)->get();
			$usr = User::take(25)->get();
			$vrk = Verkoop::take(25)->get();

			foreach ($ads as $ad)
			{
				$act[] = $ad;
			}

			foreach ($usr as $us)
			{
				$act[] = $us;
			}

			foreach ($vrk as $vr)
			{
				$act[] = $vr;
			}
			usort($act, 'cmp');

			return View::make('pages.dashboardPage')->withActiviteiten($act);
		}
		else
		{
			return Redirect::to('/');
		}
	}

	public function statistiekenPage()
	{
		if (Auth::check())
		{
			return View::make('pages.statistieken');
		}
		else
		{
			return Redirect::to('/');
		}
	}

	public function advertentiesPage()
	{
		if (Auth::check())
		{
			$ads = Advertentie::orderBy('advertentieId', 'DESC')->get();
			return View::make('pages.advertentiesPage')->withAds($ads);
		}
		else
		{
			return Redirect::to('/');
		}
	}

	public function verkopenPage()
	{
		if (Auth::check())
		{
			return View::make('pages.verkopenPage');
		}
		else
		{
			return Redirect::to('/');
		}
	}

	public function gebruikersPage()
	{
		if (Auth::check())
		{
			$g = User::orderBy('userId', 'ASC')->get();
			return View::make('pages.gebruikersPage')->withGebruikers($g);
		}
		else
		{
			return Redirect::to('/');
		}
	}

	public function gebruikerPage($id)
	{
		if (Auth::check())
		{
			$g = User::find($id);
			$a = $g->advertenties()->orderBy('advertentieId', 'DESC')->get();
			$p = $g->aankopen()->orderBy('pdfId', 'DESC')->get();
			return View::make('pages.gebruikerPage')->withGebruiker($g)->withAds($a)->withAankopen($p);
		}
		else
		{
			return Redirect::to('/');
		}
	}

	public function partnersPage()
	{
		if (Auth::check())
		{
			$g = Partner::orderBy('partnerId', 'ASC')->get();
			return View::make('pages.partnersPage')->withPartners($g);
		}
		else
		{
			return Redirect::to('/');
		}
	}

	public function partnerPage($ref)
	{
		if (Auth::check())
		{
			$p = Partner::where('referentieId', '=', $ref)->first();
			$a = $p->advertenties()->get();
			return View::make('pages.partnerPage')->withPartner($p)->withAds($a);
		}
		else
		{
			return Redirect::to('/');
		}
	}

	public function actiesPage()
	{
		if (Auth::check())
		{
			return View::make('pages.actiesPage');
		}
		else
		{
			return Redirect::to('/');
		}
	}

	/* DO's */
	public function doLogin()
	{
		$username = Input::get('username');
		$password = Input::get('password');

		if ($username && $password)
		{
			if (Auth::attempt(['username' => $username, 'password' => $password]))
			{
				return Redirect::to('/dashboard');
			}
			else
			{
				return Redirect::back()->withInput()->with('whoops', 'Gebruikersnaam of wachtwoord is onjuist yo.');
			}
		}
		else
		{
			return Redirect::back()->withInput()->with('whoops', 'Vul beide velden in yo.');
		}
	}

	public function doLogout()
	{
		Auth::logout();
		return Redirect::to('/');
	}

	public function advertentieGemistToevoegen()
	{
		$titel        = Input::get('titel');
		$omschrijving = Input::get('omschrijving');
		$datum        = Input::get('date');
		$prijs        = Input::get('prijs');

		if ($titel && $omschrijving && $datum && $prijs)
		{
			$g               = new Gemist;
			$g->titel        = $titel;
			$g->omschrijving = $omschrijving;
			$g->datum        = $datum;
			$g->prijs        = $prijs;
			if ($g->save())
			{
				return Redirect::to('/')->with('success', 'Gemiste advertentie is geplaatst yo.');
			}
			else
			{
				return Redirect::back()->withInput()->with('open-box', 'gemisteAdvertentieBox')->with('whoopsGemist', 'Kon niet plaatsen yo probeer opnieuw.');
			}
		}
		else
		{
			return Redirect::back()->withInput()->with('open-box', 'gemisteAdvertentieBox')->with('whoopsGemist', 'Vul alle velden in yo.');
		}
	}

	public function partnerToevoegen()
	{
		$naam  = Input::get('naam');
		$adres = Input::get('adres');
		$pcode = Input::get('postcode');
		$stad  = Input::get('stad');
		$btw   = Input::get('btwnummer');
		$cont  = Input::get('contactpersoon');
		$mail  = Input::get('email');
		$tele  = Input::get('telefoon');
		$site  = Input::get('website');
		$wach  = Input::get('wachtwoord');

		if ($naam && $adres && $pcode && $stad && $btw && $cont && $mail && $tele && $site && $wach)
		{
			do {
				$ref = 'cp-' . mt_rand(100000, 999999);
			} while(checkRef($ref) == 1);

			$p                 = new Partner;
			$p->referentieId   = $ref;
			$p->naam           = $naam;
			$p->contactpersoon = $cont;
			$p->email          = $mail;
			$p->telefoon       = $tele;
			$p->website        = $site;
			$p->managerId      = Auth::user()->bdksId;
			$p->straat         = $adres;
			$p->postcode       = $pcode;
			$p->stad           = $stad;
			$p->btwnummer      = $btw;
			$p->password 	   = Hash::make($wach);
			$p->createdAt      = new DateTime;

			if ($p->save())
			{
				return Redirect::to('/partners')->with('success', 'Partner "' . $naam . '" toegevoegd.');
			}
			else
			{
				return Redirect::back()->withInput()->with('open-box', 'gemisteAdvertentieBox')->with('whoopsGemist', 'Kon niet plaatsen yo probeer opnieuw.');
			}
		}
		else
		{
			return Redirect::back()->withInput()->with('open-box', 'partnerToevoegenBox')->with('whoopsPartner', 'Vul alle velden in yo.');
		}
	}

	public function advertentiePlaatsen($ref)
	{
		$titel = preg_replace("/[^A-Za-z0-9 -]/", '', Input::get('titel'));;

		$input = [
			'titel'        => $titel,
			'omschrijving' => Input::get('omschrijving'),
			'voorwaarden'  => Input::get('voorwaarden'),
			'categorie'    => Input::get('categorie'),
			'geldig-vanaf' => Input::get('geldig-vanaf'),
			'geldig-tot'   => Input::get('geldig-tot'),
			'prijs'        => Input::get('prijs'),
			'twv'          => Input::get('twv')
		];

		$rules = [
			'titel'        => 'required|max:55',
			'omschrijving' => 'required',
			'voorwaarden'  => 'required',
			'categorie'    => 'required|not_in:0|in:vakanties,evenementen,restaurants,hotels,avondje-uit,dagje-weg,sport,overig',
			'geldig-vanaf' => 'required|date_format:"d-m-Y"',
			'geldig-tot'   => 'required|date_format:"d-m-Y"',
			'prijs'        => 'required|numeric',
			'twv'          => 'required|numeric'
		];

		$messages = [
			'required'         => 'Alle velden zijn verplicht om in te vullen.',
			'titel.max'        => 'De titel is te lang. Max. 55 karakters.',
			'categorie.not_in' => 'Kies een categorie.',
			'categorie.in'     => 'Je hebt een ongeldige categorie ingevoerd.',
			'date_format'      => 'U heeft een ongeldige datum formaat ingevoerd.',
			'numeric'          => 'De prijs en waarde indicatie mag alleen uit cijfers bestaan.'
		];

		$validator = Validator::make($input, $rules, $messages);

		if ($validator->fails())
		{
			return Redirect::back()->withInput()->with('fouten', $validator->messages()->all());
		}
		else
		{
			$ad               = new Advertentie;
			$ad->verkoperId   = $ref;
			$ad->type         = 5;
			$ad->titel        = $titel;
			$ad->titelUrl     = strtolower(trim(friendlyUrl($titel), '-'));
			$ad->omschrijving = Input::get('omschrijving');
			$ad->voorwaarden  = Input::get('voorwaarden');
			$ad->categorie    = Input::get('categorie');
			$ad->prijs        = Input::get('prijs');
			$ad->twv          = Input::get('twv');
			$ad->geldigVanaf  = date("Y-m-d", strtotime(Input::get('geldig-vanaf')));
			$ad->geldigTot    = date("Y-m-d", strtotime(Input::get('geldig-tot')));
			$ad->createdAt    = new DateTime();
			$ad->updatedAt    = new DateTime();
			
			if($ad->save())
			{
				return Redirect::back()->with('success', 'Je advertentie is succesvol geplaatst!');
			}
			else
			{
				return Redirect::back()->withInput()->with('fouten', ['Je advertentie kon op dit moment niet geplaatst worden, probeer het opnieuw.']);
			}

		}
	}

	public function partnerAdvertentieToggleActief($id)
	{
		$ad = Advertentie::find($id);
		if ($ad->verwijderd == 0)
		{
			if ($ad->increment('verwijderd'))
			{
				return Response::json('Advertentie op inactief gesteld.', 200);
			}
			else
			{
				return Response::json('Kon geen verbinding maken met de server.', 500);
			}
		}
		else
		{
			if ($ad->decrement('verwijderd'))
			{
				return Response::json('Advertentie op actief gesteld.', 200);
			}
			else
			{
				return Response::json('Kon geen verbinding maken met de server.', 500);
			}
		}
	}

	public function maandFactuurPartners($id)
	{
		if (Auth::check())
		{
			$partner = Partner::find($id);
			$verkopen 	= Verkoop::where('verkoperId', '=', $partner->referentieId)->orderBy('verkoopId', 'DESC')->get();
			$fctnr = date('mY') . '-' . mt_rand(100000, 999999);
			$totaal = 0;
			$tk = 0;

			if (count($verkopen) > 0)
			{
				$timestamp	= strtotime("now");
				$date1 = new DateTime();
				$date1->setTimestamp($timestamp);
				foreach ($verkopen as $v)
				{
					$date2 = new DateTime();
					$date2->setTimestamp(strtotime($v->createdAt));

					if ($date1->format('Y-m') === $date2->format('Y-m'))
					{
					    $boekingen[] = $v;
					    $totaal = $totaal + $v->advertentie->prijs;
					    $tk = $tk + 0.35;
					}
				}
			}
			else
			{
				$boekingen = [];
			}

			$data = [
				'factuurnummer'    => $fctnr,
				'datum'            => date('d-m-Y'),
				'btwnummer'        => $partner->btwnummer,
				'naam'             => $partner->naam,
				'straat'           => $partner->straat,
				'postcode'         => $partner->postcode . ' ' . $partner->stad,
				'ref'              => $partner->referentieId,
				'totaal'           => $totaal,
				'transactiekosten' => $tk
			];

		    $html = View::make('pdf.factuurVoorPartners')->withData($data)->withBoekingen($boekingen);
		    return PDFLIB::load($html, 'A4', 'portrait')->show();
		}
		else
		{
			return Redirect::to('/');
		}
	}

	public function nieuwsbericht()
	{
		$cinput     = Input::get('contentInput');
		$content    = Input::get('content');
		$ontvangers = Input::get('ontvangers');

		if ($content && $ontvangers)
		{
			foreach($ontvangers as $o)
			{
				$n = new Nieuws;
				$n->ontvangerId = $o;
				$n->content = $content;
				$n->createdAt = new DateTime();
				$n->save();
			}
			return Redirect::to('/');
		}
		else
		{
			return Redirect::back()->withInput()->with('open-box', 'nieuwsberichtBox')->with('whoopsNieuws', 'Vul alle velden in yo.');
		}
	}

}

function checkRef($ref)
{
	return Partner::where('referentieId', '=', $ref)->count();
}

function friendlyURL($string)
{
    $string = preg_replace("`\[.*\]`U","",$string);
    $string = preg_replace('`&(amp;)?#?[a-z0-9]+;`i','-',$string);
    $string = preg_replace( "`&([a-z])(acute|uml|circ|grave|ring|cedil|slash|tilde|caron|lig|quot|rsquo);`i","\\1", $string );
    $string = preg_replace( array("`[^a-z0-9]`i","`[-]+`") , "-", $string);
 
    return $string;
}

function cmp($a, $b) {
  if ($a['createdAt'] == $b['createdAt']) {
    return 0;
  }

  return ($a['createdAt'] < $b['createdAt']) ? 1 : -1;
}