<?php

/*
|--------------------------------------------------------------------------
| Get Routes
|--------------------------------------------------------------------------
|
|
|
*/

Route::get('/', 'DeController@loginPage');
Route::get('/uitloggen', 'DeController@doLogout');
Route::get('/dashboard', 'DeController@dashboardPage');
Route::get('/statistieken', 'DeController@statistiekenPage');
Route::get('/advertenties', 'DeController@advertentiesPage');
Route::get('/verkopen', 'DeController@verkopenPage');
Route::get('/gebruikers', 'DeController@gebruikersPage');
Route::get('/partners', 'DeController@partnersPage');
Route::get('/acties', 'DeController@actiesPage');

Route::get('/gebruiker/{id}', 'DeController@gebruikerPage');
Route::get('/partner/{id}', 'DeController@partnerPage');

Route::get('/partner/{id}/make/maand-factuur', 'DeController@maandFactuurPartners');
Route::get('/advertentie/{id}/toggle-actief', 'DeController@partnerAdvertentieToggleActief');

Route::get('/pdf', function() {
	$advert = Advertentie::find(1);
	$partner = Partner::find(1);
	do {
		$waardenummer = mt_rand(1000000000, 9999999999);
	} while(checkWaardenummer($waardenummer) == 1);

	$data = [
		'waardenummer' => $waardenummer,
		'datum'        => date('d-m-Y'),
		'bedrijf'      => $partner->naam,
		'advertentie'  => $advert
	];

    $html = View::make('pdf.waardebon')->withData($data);
    return PDFLIB::load($html, 'A4', 'portrait')->show();
});

/*
|--------------------------------------------------------------------------
| Post Routes
|--------------------------------------------------------------------------
|
|
|
*/

Route::post('/login', 'DeController@doLogin');
Route::post('/toevoegen/advertentie-gemist', 'DeController@advertentieGemistToevoegen');
Route::post('/toevoegen/partner', 'DeController@partnerToevoegen');
Route::post('/toevoegen/nieuwsbericht', 'DeController@nieuwsbericht');

Route::post('/partner/{ref}/advertentie-plaatsen', 'DeController@advertentiePlaatsen');


function checkWaardenummer($w)
{
	return Waardenummer::where('waardeNummer', '=', $w)->count();
}