jQuery(document).ready(function($) {
  $('#price_filter').val('0-200');
  $("#price_slider").slider({
    range:true,
    min: 0,
    max: 200,
    values:[0, 200],
    step: 1,
    slide: function(event, ui) {
      $("#price_range_label").html('$' + ui.values[ 0 ] + ' - $' + ui.values[ 1 ] );
      $('#price_filter').val(ui.values[0] + '-' + ui.values[1]).trigger('change');
    }
  });

	
$('#duration_filter').val('1-30');
  $("#duration_slider").slider({
    range:true,
    min: 1,
    max: 30,
    values:[1, 30],
    step: 1,
    slide: function(event, ui) {
      $("#duration_range_label").html( + ui.values[ 0 ] + ' - ' + ui.values[ 1 ] );
      $('#duration_filter').val(ui.values[0] + '-' + ui.values[1]).trigger('change');
    }
  });


  $('#type :checkbox').prop('checked', false);
  $('#rating :checkbox').prop('checked', false);
  $('#facility :checkbox').prop('checked', false);
  $('#arrangement :checkbox').prop('checked', false);
	
	
  FilterJS(services, "#service_list", {
    template: '#template',
    criterias:[
      {field: 'prijs', ele: '#price_filter', type: 'range'},
	  {field: 'duration', ele: '#duration_filter', type: 'range'},
      {field: 'accomodation', ele: '#type', all: 'all'},
	  {field: 'rating', ele: '#rating :checkbox'},
	  {field: 'facilities', ele: '#facility :checkbox'},
	  {field: 'arrangement', ele: '#arrangement :checkbox'},
	  {field: 'country', ele: '#country', all: 'all'}
    ],
    search: { ele: '#search_box' }
  });

});
