jQuery(document).ready(function($) {

  $('#price_filter').val('0-150');
  $("#price_slider").slider({
    range:true,
    min: 0,
    max: 150,
    values:[0, 150],
    step: 1,
    slide: function(event, ui) {
      $("#price_range_label").html('$' + ui.values[ 0 ] + ' - $' + ui.values[ 1 ] );
      $('#price_filter').val(ui.values[0] + '-' + ui.values[1]).trigger('change');
    }
  });

  $('#type :checkbox').prop('checked', true);

  FilterJS(services[0], "#service_list", {
    template: '#template',
    criterias:[
      {field: 'prijs', ele: '#price_filter', type: 'range'},
      {field: 'categorie', ele: '#type :checkbox'}
    ],
    search: { ele: '#search_box' },
	// pagination settings
	pagination: {
	  perPage: 5,
	  range: 3,
	  prevText: 'prev',
	  nextText: 'next',
	  pagination_container: '.pagination',
	  noresults_container: '#noresults'
    }
  });

});
